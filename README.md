# Plotting script for calibration data
 
**Simply clone the repo recursively and** 

Here temporary advice is to compile simple_data_plotter.cpp (not data_plotter.cpp)

```bash
git clone --recursive https://:@gitlab.cern.ch:8443/vplesano/nswcalibrationdataplotter.git
./setup_plotter.sh
```
this will clone the repository and `setup_plotter.sh` will check if the output directories are already created (if no creates them) and compiles the plotter 

## Description
Program is designed for the calibration data plotting using the calibration data root files that are
created by the merged common text files. At the state of 14.06.2021 following features are included:

	* Use the recorded .txt files and merges information into the root trees:
	 baselines, untrimmed thresholds, calibration data, VMM internal pulser DAC calibration data
	* Data choice depends on the selected FEB names that are compared with names from the configuration .json file
	* Sampled baselines
	* Calibrated threshold plots
	* Create plots for the specified FEBs or FEB groups - (HO, IP sides or L1,L2.. layers)
	* Naming the plots includes date-time stamp at the end of the name
	* Archivates created .root files in user defined directory (-a option)
	* Archivates created png/pdf plots in user defined directory (-a option)

### Options

        Common:
	* -a (string):			path to the desired ARCHIVATION directory
	* -n (string):			desired plot/file name suffix
	* -f (string):			desired FEBs to describe with the plots
        * -S (int):			sector nr from which data was obtained
        Threshold calibration:
	* -d (string):			(**Use always**) location of the input files where calibration data is located
	* --sca: 			threshold calibration flag - enables baseline and threshold plotting
	* --bl:				plot baselines
	* --thr:			plot threshold and noise plots
	* --scale:			increases Y axis scale to 1200[mV]
        PDO/TDO calibration:
	* --l1: 			L1 Data calibration flag - enables PDO/TDO calibration
	* -R (string):			(**Use always**) location of the PDO/TDO calibration .root file
	* --pulser:			eanbles pulser DAC calibration (needed for PDO calibration)
        
Useage of the option `-f` is different in the cases of PDO/TDO & threshold calibration data.

In case of baselines/trimmers option `f` takes any `char` string characterizing the front-end names in the .json file that is common for the following format:`MMFE8_L#P#_(HO/IP)L/R`. So for instance `-f L1` would select only data from the Layer 1 of the Micromegas double wedge for being displayed

In case of the PDO/TDO calibration option `-f` needs to be represented in the following format: `L_RR_V` where L - Layer, R - radius (two digit nr (for radiuses < 10 use 0 in front)), V - VMM number. Example:
`-f 3_04_5` would yield the plots for PDO or TDO calibration of the front-end at layer 3, radius 4 and VMM nr 3.

In principle one can install NSWCalibrationDataPlotter in ANY desired directory - option -j points to the original json configuration file that was used to gather the calibration data.

**IMPORTANT**: 

* Root files for all types of calibration data are set up to be updated automatically
* if -f option is not specified, plotting will be done for ALL frontends specified in the .txt data files
* if -a option is not specified, archivation will be done in the `/plotter_output` directory 
* Due to increase in the ammount of data from input files and number of histograms that have to be stoed in memoy **it is advisable to plot baseline plots for one layer at a time**.

### Typical use cases

If user needs to plot threshold calibration data, following examples are applicable:
```bash
./main -n test -f MM -d /afs/path/to/data/here -a /afs/path/to/archive  --sca --bl	#plots baselines for the updated root trees and all FEBs with 'MM' char in their naming
./main -n test -f MM -d /afs/path/to/data/here -a /afs/path/to/archive  --sca --thr     #plots calibrated thresholds for the updated root trees and all FEBs with 'MM' char in their naming
./main -n test -f HO -d /afs/path/to/data/here -a /afs/path/to/archive  --sca --bl	#plots baselines for the already existing root trees for all HO side front-ends 
```
If user needs to calibrate PDO/TDO using the recorded L1A data, following examples are applicable:
```bash
./main -n test -f 0_01_05 -R /afs/path/to/L1A/data/my_swrod_PDOCalib_output.root --l1 --pulser	  #calibrates the PDO with updated VMM internal pulser data, outputs plots for VMM 5 of layer 0 radius 1 FEB
./main -n test -f 0_01_05 -R /afs/path/to/L1A/data/my_swrod_TDOCalib_output.root --l1 -a ~/my/path/to/archive	  #calibrates the TDO, puts the created root trees in the user defined archive, plots data for VMM 5 of layer 0 radius 1 FEB
```

### File arrangement
The output of the baseline and threshold plots are separated into different folders in the dir:
```bash
plotter_output/thresholds/...png
plotter_output/noise/...png
plotter_output/baselines/...svg
```
output plots of the selected FEB VMMs from the PDO/TDO calibration are located in:
```bash
plotter_output/fitpdo/...png
plotter_output/fittdo/...png
``` 
Important note - plotting app uses the same input_data.json as does the calibration script to locate appropriate .txt output directories.

**Do not modify by hand or append any files in the directories common_txt/ and root_files/!**

Directory common_txt holds the combined data from all FEBs:
```bash	 
 common_txt/Baselines.txt - baseline data [line/sample] 
 common_txt/Untrimmed_thresholds.txt - untrimmed thersholds [line/channel]
 common_txt/Calib_data.txt - threshold calibration data [line/channel]
```

Accordinngly these .txt are used to write .root files in the root_files/ directory that are the sources for the plots on the output.

```bash
root_files/pulser_dac_tree.root
root_files/baseline_tree.root
root_files/threshold_tree.root
root_files/data_tree.root
```
The functionality of the plotter depends on the data being intact in those locations.
