#include "NSWCalibDataPlotter/BigThresholds.h"

nsw::BigThresholds::BigThresholds(){}

void nsw::BigThresholds::runFullSector(std::string rootfile,
                                       std::string picname,
                                       std::string FEB,
                                       bool enlarge){

  std::cout<<rootfile<<std::endl;
  TFile *f = new TFile(rootfile.c_str());
  std::cout<< f <<std::endl;
  std::string tree = "par_tree";

  TTreeReader tr(tree.c_str(), f);

  TTreeReaderValue<std::string> ifeb(tr,"board");
  TTreeReaderValue<int> CHAN(tr,"channel_id");
  TTreeReaderValue<int> VMM(tr,"cvmm");
  TTreeReaderValue<float> BLN(tr,"ch_baseline_med");
  TTreeReaderValue<float> vmmBLN(tr,"vmm_median");
  TTreeReaderValue<float> RMS(tr,"ch_baseline_rms");
  TTreeReaderValue<float> effTHR_slope(tr,"ch_eff_th_slope");
  TTreeReaderValue<float> effTHR(tr,"ch_eff_thr_w_best_trim");
  TTreeReaderValue<float> trimTHR(tr,"ch_trimmed_thr");
  TTreeReaderValue<int> trimBest(tr,"ch_best_trim");
  //TTreeReaderValue<int> ch_mask(tr,"ch_mask");

  TH2D* yoba1 = new TH2D("yoba1","yoba1",50,0,5,60,0,120);
  TH2D* yoba2 = new TH2D("yoba2","yoba2",32,0,32,60,0,120);

  TH3D* yoba3 = new TH3D("yoba3","yoba3",50,0,5,60,0,120,50,0,5);
  //TH3D* yoba4 = new TH3D("yoba4","yoba4",50,0,5,60,0,120,300,0,300);

  TGraph* l_slopes = new TGraph();
  TGraph* l_channels = new TGraph();

  std::map<std::string,TH2D*> febs_bl;
  std::map<std::string,TH2D*> febs_et;
  std::map<std::string,TH2D*> febs_tt;
  std::map<std::string,TH2D*> febs_rms;

  std::map<std::string,TGraph*> big_bl;
  std::map<std::string,TGraph*> big_rms;
  std::map<std::string,TGraph*> big_eff;
  std::map<std::string,TGraph*> big_tt;

  TGraph2D *tgr = new TGraph2D();
  TGraph2D *tgr_et = new TGraph2D();

  //TH1D *trim_a = new TH1D("trim_a","trim_a",50,0,5);
  //TH3D *ret  = new TH3D("ret","ret",
  //                      50,0,5,70,0,70,50,0,5);
  //TH3D *reb  = new TH3D("reb","reb",
  //                      50,0,5,70,0,70,150,50,200);

  std::map<std::string,TGraph*> layer_efft;

  std::string this_feb;
  int vmm,chan,btrim;
  float bln,vmm_bln,rms,efft,trimThr,trimSlope;
  int foundMMFE = 0;

  int counter= 0;

  bool stgc = false;

  bool ancient_data = false;
  if(rootfile.find("Freiburg")!=std::string::npos){
     ancient_data = true;
  }

  std::vector<float> L1_efft_vals;

  while(tr.Next()){

    this_feb = *ifeb;
    chan = *CHAN;
    vmm = *VMM;
    bln = *BLN;
    vmm_bln = *vmmBLN;
    rms = *RMS;
    efft = *effTHR;
    trimSlope = *effTHR_slope;
    trimThr = *trimTHR;
    btrim = *trimBest;

    int this_layer, R;
    //int R = getRadius(this_feb,this_layer);
    std::string side, smth, asic, type;
    int sect;
    
    bool new_name_sch = false;
 
    //if(this_feb.find("000")!=std::string::npos){
    //   ancient_data = true;
    //}
 
    //bool stgc = false;
 
    if(this_feb.find("/")!=std::string::npos){
       if(this_feb.find("sTGC")!=std::string::npos ||
          this_feb.find("FEB")!=std::string::npos
         ){
          stgc = true;
       }
       new_name_sch = true;
       std::stringstream ss(this_feb);
       std::string seg;
       std::vector<std::string> seg_list;

       if(this_feb.find('/')!=std::string::npos){
          while(std::getline(ss,seg,'/')){
             seg_list.push_back(seg);
          }
          side=seg_list.at(0);
          smth=seg_list.at(1);
          asic=seg_list.at(2);
          type=seg_list.at(3);
          sscanf(seg_list.at(4).c_str(),"S%i",&sect);
          sscanf(seg_list.at(5).c_str(),"L%i",&this_layer);
          sscanf(seg_list.at(6).c_str(),"R%i",&R);

          std::cout<<"Parsing name:"<<this_feb
                   <<"\nside=["<<side
                   <<"\nsmth=["<<smth
                   <<"\nasic=["<<asic
                   <<"\ntype=["<<type
                   <<"\nsect=["<<sect
                   <<"\nlayer=["<<this_layer
                   <<"\nradius=["<<R
                   <<std::endl;
       }else if(this_feb.find("pFEB")!=std::string::npos){
          type="Pad";
       }else if(this_feb.find("sFEB")!=std::string::npos){
          type="Strip";
       }else{
         std::cout<<"Could not Identify type of the board - "
                  <<this_feb<<"Does not have [p(s)FEB/slashes/sTGC]"<<std::endl;
         exit(0);
       }
       
    }else{
       R = getRadius(this_feb,this_layer);
    }

    //std::cout<<"################################################"
    //         <<"1:"
    //         <<"\nR="<<R
    //         <<"\nL="<<this_layer
    //         <<std::endl; 

    std::string keybase = "-L-"+std::to_string(this_layer);
    std::string blkey = "B"+keybase;
    std::string rkey = "R"+keybase;
    std::string ekey = "E"+keybase;
    std::string tkey = "T"+keybase;
   
    int map_chan;
    if(new_name_sch){
       std::cout<<"NEW naming"<<std::endl;
       if(stgc){
       //if(side.find("sTGC")!=std::string::npos){
          if(type=="Pad"){
             std::cout<<"PAD channel"<<std::endl;      
             map_chan = chan+vmm*64+R*192;
          }else{
             std::cout<<"STRIP channel"<<std::endl;      
             if(R<2){
                map_chan = 576 + chan + vmm*64 + R*384;
             }else{
                map_chan = 1344 + chan +vmm*64;
             }
             /////// rms of the effThr ////////
             if(this_layer==1){
               L1_efft_vals.push_back(efft*1000/4095.0);
             }  
          }
       }else{
          std::cout<<"MMFE naming"<<std::endl;
          map_chan = chan + vmm*64 + R*512;
       }
    }else{
       std::cout<<"OLD naming"<<std::endl;
       map_chan = getSectorChan(this_feb,chan,vmm);
    }
   

    //std::cout<<"2:"
    //         <<"\nCHAN="<<map_chan
    //         <<"\nBLN="<<bln
    //         <<std::endl; 

    float conv;
    if(stgc){
       conv = 1000/4095.0;
    }else{
       conv = 1000*1.5/4095.0;
    }

    yoba1->Fill(rms,efft*conv);
    yoba2->Fill(btrim,efft*conv);

    yoba3->Fill(rms,efft*conv,trimSlope);
    //yoba4->Fill(rms,efft*conv,bln);
    if(this_layer==1){
       l_slopes->SetPoint(l_slopes->GetN(),map_chan,trimSlope);
       l_channels->SetPoint(l_channels->GetN(),map_chan,bln/vmm_bln);
    }

    std::cout<<"SIZE:"<<yoba1->GetEntries()<<std::endl;

    if(big_bl.find(blkey)==big_bl.end()){
        big_bl[blkey] = new TGraph();
        big_rms[rkey] = new TGraph();
        big_eff[ekey] = new TGraph();
        big_tt[tkey] = new TGraph();
    }

    big_bl[blkey]->SetPoint(big_bl[blkey]->GetN(),map_chan,bln);
    big_rms[rkey]->SetPoint(big_rms[rkey]->GetN(),map_chan,rms);
    big_eff[ekey]->SetPoint(big_eff[ekey]->GetN(),map_chan,efft*conv);
    big_tt[tkey]->SetPoint(big_tt[tkey]->GetN(),map_chan,trimThr*conv);

    tgr->SetPoint(tgr->GetN(), map_chan, this_layer, rms);
    tgr->SetPoint(tgr->GetN(), map_chan, this_layer, efft*conv);

    std::string l_key = "L"+std::to_string(this_layer);
    if(layer_efft.find(l_key)==layer_efft.end()){
       layer_efft[l_key] = new TGraph();
    }
    layer_efft[l_key]->SetPoint(layer_efft[l_key]->GetN(),map_chan,efft*conv+100*this_layer);

    if(this_feb.find(FEB)!=std::string::npos){

       std::string fkey1 = this_feb+"_"+std::to_string(1);
       std::string fkey2 = this_feb+"_"+std::to_string(2);
       std::string fkey3 = this_feb+"_"+std::to_string(3);
       std::string fkey4 = this_feb+"_"+std::to_string(4);

       int nbins, start, end;
       if(type=="Pad"){
          nbins = 192;
          start = 0;
          end = 192;
       }else{
          nbins = 512;
          start = 0;
          end = 512;
       }
       int ybins, ystart, yend;       
       if(stgc){
          if(enlarge){
             ybins = 700;
             ystart = 0;
             yend = 700;
          }else{
             ybins = 500;
             ystart = 0;
             yend = 500;
          }
       }else{
          if(enlarge){
             ybins = 600;
             ystart = 0;
             yend = 600;
          }else{
             ybins = 400;
             ystart = 0;
             yend = 400;
          }
       }

       foundMMFE++; 
       std::cout<<"LOOKING:"<<FEB<<" > FOUND:"<<this_feb<<std::endl;
       if(febs_bl.find(fkey1)==febs_bl.end()){
         febs_bl[fkey1] = new TH2D(fkey1.c_str(),fkey1.c_str(),nbins,start,end,ybins,ystart,yend);
         febs_tt[fkey2] = new TH2D(fkey2.c_str(),fkey2.c_str(),nbins,start,end,ybins,ystart,yend);
         febs_et[fkey3] = new TH2D(fkey3.c_str(),fkey3.c_str(),nbins,start,end,ybins,ystart,yend);
         febs_rms[fkey4] = new TH2D(fkey4.c_str(),fkey4.c_str(),nbins,start,end,100,0,10);
         std::cout<<"SOZDAL"<<std::endl;
       }
      std::cout<<"-----------------------------------\nkeys="
               <<fkey1<<", "
               <<fkey2<<", "
               <<fkey3<<"----------------------------------"<<std::endl;
     // std::cout<<"-----------------"<<std::endl;
     // for(auto &k: febs_bl){
     //     std::cout<<k.first<<std::endl;
     // }
     // std::cout<<"-----------------"<<std::endl;
      febs_bl[fkey1]->Fill(chan+vmm*64,bln); 
      febs_tt[fkey2]->Fill(chan+64*vmm,trimThr*conv);  
      febs_et[fkey3]->Fill(chan+64*vmm,efft*conv);
      febs_rms[fkey4]->Fill(chan+64*vmm,rms);

      // ret->Fill(rms,efft,trimSlope); 
      // reb->Fill(rms,efft,bln) ;

      std::cout<<"Params:"
               <<"\n"<<febs_bl.size()
               <<"\n"<<febs_tt.size()
               <<"\n"<<febs_et.size()
               <<"\nnpoints:"
               <<"\n"<<febs_bl[fkey1]->GetEntries()
               <<"\n"<<febs_tt[fkey2]->GetEntries()
               <<"\n"<<febs_et[fkey3]->GetEntries()
               <<"\nvalues:"
               <<"\nCH="<<chan+vmm*64
               <<"\nBL="<<bln
               <<"\nTT="<<trimThr*conv
               <<"\nET="<<efft*conv
               <<"\n"<<std::endl;
       //sleep(0.5);

    }

    counter++;
  }

  ///////////////////////////////////////////////////////////////////
  TCanvas *cnv1 = new TCanvas();
  gStyle->SetOptStat(0);
  gStyle->SetOptTitle(0);

  std::cout<<"Getentries:"<<yoba1->GetEntries()<<std::endl;

  yoba1->GetXaxis()->SetTitle("Channel baseline RMS, [mV]");
  yoba1->GetYaxis()->SetTitle("Channel effective threshold, [mV]");
  yoba1->Draw("COLZ");
  cnv1->SaveAs(Form("ALL-RmsEfft-%s.png",picname.c_str()));
  cnv1->SaveAs(Form("ALL-RmsEfft-%s.pdf",picname.c_str()));
  delete cnv1;

  TCanvas *cnv2 = new TCanvas();
  gStyle->SetOptStat(0);
  gStyle->SetOptTitle(0);
  yoba2->GetXaxis()->SetTitle("Channel best trimmer, [DAC]");
  yoba2->GetYaxis()->SetTitle("Channel effective threshold, [mV]");
  yoba2->Draw("COLZ");
  cnv2->SaveAs(Form("ALL-TrimEfft-%s.png",picname.c_str()));
  cnv2->SaveAs(Form("ALL-TrimEfft-%s.pdf",picname.c_str()));
  delete cnv2;

  TCanvas *cnv3 = new TCanvas();
  //gStyle->SetTitleXOffset(2);
  //gStyle->SetTitleOffset(2,"x");
  //gStyle->SetTitleOffset(2,"y");
  //gStyle->SetTitleOffset(2,"z");
  gStyle->SetOptStat(0);
  gStyle->SetOptTitle(0);
  gPad->SetPhi(45);
  gPad->SetTheta(30);

  yoba3->GetXaxis()->SetTitle("Channel baseline RMS, [mV]");
  yoba3->GetYaxis()->SetTitle("Channel effective threshold, [mV]");
  yoba3->GetZaxis()->SetTitle("Trimmer DAC slope, [DAC/mV]");
  yoba3->Draw("SCAT");
  cnv3->SaveAs(Form("ALL-RmsEfft-vs-trimSlope-%s.png",picname.c_str()));
  cnv3->SaveAs(Form("ALL-RmsEfft-vs-trimSlope-%s.pdf",picname.c_str()));
  delete cnv3;
  
  /////////////////////////////////////////////////////
  //TCanvas *cnv4 = new TCanvas();
  //gStyle->SetOptStat(0);
  //gStyle->SetOptTitle(0);
  //l_slopes->GetXaxis()->SetTitle("Mapped channel, [N]");
  //l_slopes->GetYaxis()->SetTitle("trimmer DAC slope, [DAC/mV]");
  //l_slopes->SetMarkerStyle(7);
  //l_slopes->SetMarkerSize(2);
  //l_slopes->SetMarkerColor(kRed-5);
  //l_slopes->SetMaximum(5);
  //l_slopes->Draw("AP");
  //cnv4->SaveAs(Form("layer1-slopes-%s.png",picname.c_str()));
  //delete cnv4;

  //TCanvas *cnv5 = new TCanvas();
  //gStyle->SetOptStat(0);
  //gStyle->SetOptTitle(0);

  ////--------------------------------------------------------------

  //int nout = 0;

  //for(int i=0; i<l_channels->GetN();i++){
  //    if(l_channels->GetPointY(i)>2){
  //       nout++;
  //    }else{
  //       continue;
  //    }
  //}
  //l_channels->GetXaxis()->SetRangeUser(0,8192);
  //l_channels->SetMarkerStyle(7);
  //l_channels->SetMarkerSize(2);
  //l_channels->SetMarkerColor(kBlue-5);
  //l_channels->SetMaximum(2);

  //l_channels->GetXaxis()->SetTitle("Mapped channel, [N]");
  //l_channels->GetYaxis()->SetTitle("Channel baseline / VMM baseline");
  //l_channels->Draw("AP");
  //TLatex tlx;
  // tlx.SetTextSize(0.03);
  // tlx.DrawLatexNDC(0.7,0.84,
  //  TString::Format("Out of range=%i",nout));


  //cnv5->SaveAs(Form("layer1-channels-%s.png",picname.c_str()));
  //delete cnv5;


  /////////////////////////////////////////////////////////////////
  TCanvas *c6 = new TCanvas();

  auto *mg2 = new TMultiGraph("mg2","mg2");

  l_slopes->SetMarkerColor(209);
  l_slopes->SetMarkerStyle(7);
  l_slopes->SetMarkerSize(2);

  int badSlopes = 0;
  float meanSlope = 0;

  std::vector<float> n_slopes;
  for(int i=0; i<l_slopes->GetN();i++){
     float this_slope = l_slopes->GetPointY(i);
     if(!stgc && this_slope < 1){badSlopes++;}
     if(stgc && this_slope > -1){badSlopes++;}
     n_slopes.push_back(this_slope);
  }
  meanSlope = getMean(n_slopes);
  float slope_rms = getRMS(n_slopes,meanSlope);

  mg2->Add(l_slopes);

  l_channels->SetMarkerStyle(7);
  l_channels->SetMarkerSize(2);
  l_channels->SetMarkerColor(226);

  int badChanBl = 0;
  for(int i=0; i<l_channels->GetN();i++){
     float this_chan = l_channels->GetPointY(i);
     if(this_chan<=0.2){badChanBl++;}else{continue;}
  }

  float meanBlDev = l_channels->GetMean(2);
  float rmsBlDev = l_channels->GetRMS(2);
 
  mg2->Add(l_channels);

  int grmin, grmax;
  int gr_x0 = 0;
  int gr_x1;
  if(stgc){
     grmin = -7;
     grmax = 2;
     gr_x1 = 1860;
  }else{
     grmin = -1;
     grmax = 5;
     gr_x1 = 8193;
  }

  mg2->SetTitle("; Mapped channel; Channel baseline [mV] / VMM baseline [mV]");
  mg2->GetHistogram()->GetXaxis()->SetRangeUser(gr_x0,gr_x1);
  
  mg2->SetMaximum(grmax);
  mg2->SetMinimum(grmin);
  mg2->Draw("AP");

  TF1 *f1=new TF1("f1","x",grmin,grmax);
  TGaxis *ax = new TGaxis(gr_x1,grmin,gr_x1,grmax,"f1",6,"+L");
  ax->SetTitle("Trimmer DAC slope, [DAC/mV]");
  //ax->SetTextFont(12);
  //ax->SetLabelSize(0.03);
  ax->SetTickLength(0.02);
  ax->SetLineColor(209);
  ax->SetTextColor(209);
  ax->Draw();

   float ystart = 0.85;
   if(stgc){
      ystart = 0.65;
   }

   TLatex tl;
   tl.SetTextSize(0.03);
   tl.DrawLatexNDC(0.65,ystart,
    TString::Format("#color[145]{#mu_{trimDAC}=%.2f}",meanSlope));
   tl.DrawLatexNDC(0.65,ystart - 0.04,
    TString::Format("#color[145]{RMS_{trimDAC}=%.2f}",slope_rms));
   tl.DrawLatexNDC(0.65,ystart - 0.08,
    TString::Format("#color[145]{BadTrims=%.2f\%}",((float)badSlopes/8192.0)*100.0));

   tl.DrawLatexNDC(0.2,ystart,
    TString::Format("#color[140]{#mu_{Baseline Dev}=%.2f}",meanBlDev));
   tl.DrawLatexNDC(0.2,ystart - 0.04,
    TString::Format("#color[140]{RMS_{Baseline Dev}=%.2f}",rmsBlDev));
   tl.DrawLatexNDC(0.2,ystart - 0.08,
    TString::Format("#color[140]{BadBaseline=%.2f\%}",((float)badChanBl/8192.0)*100.0));

  c6->SaveAs(Form("layer1-combined-%s.png",picname.c_str()));
  c6->SaveAs(Form("layer1-combined-%s.pdf",picname.c_str()));
  delete c6;
  ///////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////
  for(auto &f: febs_bl){

     TCanvas *c5 = new TCanvas();
 
     c5->SetCanvasSize(1200,800);

     gStyle->SetOptStat(0);
     gStyle->SetOptTitle(0);

     char pref[5];
     char laypcb[4];
     char side[3];
     int type;

     sscanf(f.first.c_str(),
            "%5c_%4c_%3c_%i",
             &pref,
             &laypcb,
             &side,
             &type);

     std::cout<<f.first<<"| "
              <<pref<<" | "
              <<laypcb<<" | "
              <<side<<" | "
              <<type<<std::endl;

     std::cout<<"A "
              <<febs_bl.size()<<" "
              <<febs_tt.size()<<" "
              <<febs_et.size()<<" "
               <<std::endl; 

     //std::string fullname = f.first.substr(0,14);
     std::string fullname = f.first.substr(0,f.first.length()-2);
     std::string key_febs_et = fullname+"_"+std::to_string(3);
     std::string key_febs_tt = fullname+"_"+std::to_string(2);
     std::string key_febs_rms = fullname+"_"+std::to_string(4);
    
     std::cout<<"B "<<fullname<<std::endl;
     std::cout<<f.second->GetEntries()<<std::endl;

     f.second->SetMarkerStyle(7);
     f.second->SetMarkerSize(2);
     f.second->SetMarkerColor(kBlack);
     f.second->GetXaxis()->SetTitle("Mapped channel");
     f.second->GetYaxis()->SetTitle("Voltage,[mV]");
     f.second->Draw();

     std::cout<<"C "<<febs_tt[key_febs_tt]->GetEntries()<<std::endl; 

     febs_tt[key_febs_tt]->SetMarkerStyle(7);
     febs_tt[key_febs_tt]->SetMarkerSize(2);
     febs_tt[key_febs_tt]->SetMarkerColor(kGreen+2);
     febs_tt[key_febs_tt]->Draw("SAME");
     
     std::cout<<"D "<<key_febs_et<<std::endl; 

     febs_et[key_febs_et]->SetMarkerStyle(7);
     febs_et[key_febs_et]->SetMarkerSize(2);
     if( picname.find("P3")!=std::string::npos
         &&
         ((picname.find("P1")!=std::string::npos &&
          picname.find("HOR")!=std::string::npos)
         ||
         (picname.find("p1")!=std::string::npos &&
          picname.find("HOR")!=std::string::npos))
     ){
        febs_et[key_febs_et]->SetMarkerColor(227);
     }else{
        febs_et[key_febs_et]->SetMarkerColor(kOrange+2);
     }
     febs_et[key_febs_et]->Draw("SAME");

     std::cout<<"E"<<std::endl; 

     TMarker *bl_mark = new TMarker(0,0,21);
     bl_mark->SetMarkerColor(kBlack);
     bl_mark->SetMarkerSize(3);
     TMarker *et_mark = new TMarker(0,0,21);
     et_mark->SetMarkerColor(kOrange+2);
     et_mark->SetMarkerSize(3);
     TMarker *tt_mark = new TMarker(0,0,21);
     tt_mark->SetMarkerColor(kGreen+2);
     tt_mark->SetMarkerSize(3);


     TLegend *leg = new TLegend(0.1,0.9,0.35,0.75);
     leg->AddEntry(bl_mark,"Baseline","p");
     leg->AddEntry(tt_mark,"Trim. Threshold","p");
     leg->AddEntry(et_mark,"Eff. Threshold","p");

     //leg->AddEntry(f.second,"Baseline","p");
     //leg->AddEntry(febs_tt[key_febs_tt],"Trim. Threshold","p");
     //leg->AddEntry(febs_et[key_febs_et],"Eff. Threshold","p");
     leg->Draw();

     std::string prefix;
     for(auto &c: f.first){
         if(c=='/'){
           prefix+='_';
         }else{
           prefix+=c;
         }
     }

     c5->SaveAs(Form("THIS-THR-%s-%s.png",prefix.c_str(),picname.c_str()));
     c5->SaveAs(Form("THIS-THR-%s-%s.pdf",prefix.c_str(),picname.c_str()));

     delete c5; 

  }
  for(auto &r: febs_rms){
     TCanvas *t = new TCanvas();
  
     r.second->GetXaxis()->SetTitle("Mapped channel");
     r.second->GetYaxis()->SetTitle("RMS,mV");
     r.second->SetMarkerStyle(7);
     r.second->SetMarkerSize(1);
     r.second->SetMarkerColor(kRed+4);
     r.second->Draw();

     std::string prefix;
     for(auto &c: r.first){
         if(c=='/'){
           prefix+='_';
         }else{
           prefix+=c;
         }
     }

     t->SaveAs(Form("THIS-FEB-RMS-%s-%s.png",prefix.c_str(),picname.c_str()));
     t->SaveAs(Form("THIS-FEB-RMS-%s-%s.pdf",prefix.c_str(),picname.c_str()));
     delete t;
  }

  /////////////////////////////////////////////////////
  std::cout<<"NANAH!  found["<<foundMMFE<<"]"<<std::endl; 
  for(auto &r: big_rms){

     int L2;
     sscanf(r.first.c_str(),"R-L-%i",&L2);

     TCanvas *c2 = new TCanvas();

     c2->SetCanvasSize(1200,800);
   
     int MaxRng;
     if(stgc){
       MaxRng = 1856;
     }else{
       MaxRng = 8192;
     }

     r.second->GetXaxis()->SetTitle("Mapped channel");
     r.second->GetXaxis()->SetRangeUser(-1,MaxRng);
     r.second->GetYaxis()->SetTitle("Baseline RMS,[mV]");
     
     r.second->SetMarkerStyle(7);
     r.second->SetMarkerSize(2);
     r.second->SetMarkerColor(kRed+1);

     r.second->SetMaximum(10);
     //r.second->SetMinimum(1);

     r.second->Draw("AP");

     if(stgc){
        TLine *ln5 = new TLine(576,0,576,10);
        ln5->SetLineColor(kGreen-3);
        ln5->SetLineWidth(2);
        ln5->SetLineStyle(9);
        ln5->Draw("SAME");
       
        TLatex  tlx;
        tlx.SetTextSize(0.03);
        tlx.DrawLatexNDC(0.15,0.65,TString::Format("#color[94]{pFEB  channels}"));
        tlx.DrawLatexNDC(0.55,0.65,TString::Format("#color[54]{sFEB channels}"));

      }else{
        TLine *ln5 = new TLine(5120,0,5120,10);
        ln5->SetLineColor(kGreen-3);
        ln5->SetLineWidth(2);
        ln5->SetLineStyle(9);
        ln5->Draw("SAME");
       
        TLatex  tlx;
        tlx.SetTextSize(0.03);
        tlx.DrawLatexNDC(0.35,0.7,TString::Format("#color[94]{M1}"));
        tlx.DrawLatexNDC(0.75,0.7,TString::Format("#color[54]{M2}"));

      }
    
     c2->SaveAs(Form("BIG-rms-L%i-%s.png",L2,picname.c_str()));
     c2->SaveAs(Form("BIG-rms-L%i-%s.pdf",L2,picname.c_str()));

     delete c2; 
  
   }
  //########## combined ##################################

  for(auto &e: big_eff){

     TCanvas *c4 = new TCanvas();

     c4->SetCanvasSize(1200,800);
 
     gStyle->SetOptTitle(0); 
 
     auto mg = new TMultiGraph("mg","mg");

     int L3;
     sscanf(e.first.c_str(),"E-L-%i",&L3);
     std::string key_bl = "B-L-"+std::to_string(L3);
     std::string key_tt = "T-L-"+std::to_string(L3);
     
     e.second->SetMarkerStyle(7);
     e.second->SetMarkerSize(2);
     e.second->SetMarkerColor(kOrange+2);
     //e.second->SetMarkerColor(kYellow+3);

     big_bl[key_bl]->SetMarkerStyle(7);
     big_bl[key_bl]->SetMarkerSize(2);
     big_bl[key_bl]->SetMarkerColor(kBlack);

     big_tt[key_tt]->SetMarkerStyle(7);
     big_tt[key_tt]->SetMarkerSize(2);
     big_tt[key_tt]->SetMarkerColor(kGreen+2);

     ///// add hists ////////
     mg->Add(e.second);
     mg->Add(big_bl[key_bl]);
     mg->Add(big_tt[key_tt]);
     
     int maxX,maxY;
     if(stgc){
       maxX = 2000;
       maxY = 400;
     }else{
       maxX = 8192;
       maxY = 300;
     }

     mg->SetTitle("; Mapped channel; Voltages,[mV]");
     mg->GetHistogram()->GetXaxis()->SetRangeUser(-1,maxX);
     mg->GetHistogram()->GetYaxis()->SetRangeUser(0,maxY);

     TMarker *bl_mark = new TMarker(0,0,21);
     bl_mark->SetMarkerColor(kBlack);
     bl_mark->SetMarkerSize(3);
     TMarker *et_mark = new TMarker(0,0,21);
     et_mark->SetMarkerColor(kOrange+2);
     et_mark->SetMarkerSize(3);
     TMarker *tt_mark = new TMarker(0,0,21);
     tt_mark->SetMarkerColor(kGreen+2);
     tt_mark->SetMarkerSize(3);

     mg->Draw("AP");

     TLegend *lg = new TLegend(0.1,0.9,0.35,0.75);
     lg->AddEntry(bl_mark,"Baseline","P");
     lg->AddEntry(tt_mark,"Trim. Threshold","P");
     lg->AddEntry(et_mark,"Eff. Threshold","P");
 
     lg->Draw();

     if(stgc){
        if(L3==0){

           TLine *ln = new TLine(384,0,384,300);
           ln->SetLineColor(kRed-5);
           ln->SetLineWidth(2);
           ln->SetLineStyle(9);
           ln->Draw("SAME");
 
           TLine *ln2 = new TLine(576,0,576,300);
           ln2->SetLineColor(kRed+2);
           ln2->SetLineWidth(2);
           ln2->SetLineStyle(9);
           ln2->Draw("SAME");

        }else{
           TLine *ln3 = new TLine(576,0,576,300);
           ln3->SetLineColor(kPink-7);
           ln3->SetLineWidth(2);
           ln3->SetLineStyle(9);
           ln3->Draw("SAME");
        }
        //////////////////////////////////
        TLatex  tlx;
        tlx.SetTextSize(0.03);
        tlx.DrawLatexNDC(0.15,0.65,TString::Format("#color[2]{pFEB  channels}"));
        tlx.DrawLatexNDC(0.55,0.65,TString::Format("#color[4]{sFEB channels}"));

     }
     //for(int i=0; i<8; i++){

     //   float this_point = 64*i +64;

     //   TLine *ln = new TLine(this_point,0,this_point,250);
     //   ln->SetLineColor(kRed-5);
     //   ln->SetLineStyle(9);
     //   ln->Draw("SAME");

     //}

     c4->SaveAs(Form("BIG-COMBINED-L%i-%s.png",L3,picname.c_str()));
     c4->SaveAs(Form("BIG-COMBINED-L%i-%s.pdf",L3,picname.c_str()));

     delete c4; 
  }

  if(!ancient_data){
     /////// SECTOR RMS ///////////
     TCanvas *cn = new TCanvas();
     gStyle->SetTitleOffset(2,"x");
     cn->SetCanvasSize(1400,1050);
   
     tgr->SetTitle(" ; Mapped channel; Layer; RMS, [mV]");
     tgr->SetMaximum(9);
     tgr->Draw("pcol");
   
     gPad->SetPhi(45);
     gPad->SetTheta(60);
    
     cn->SaveAs(Form("SURF-SECTOR-%s.png",picname.c_str()));
     cn->SaveAs(Form("SURF-SECTOR-%s.pdf",picname.c_str()));
     delete cn;
   
     ////// SECTOR EFF THR /////////////
     TCanvas *cn2 = new TCanvas();
     gStyle->SetTitleOffset(2,"x");
     cn2->SetCanvasSize(1400,1050);
   
     tgr->SetTitle(" ; mapped channel; Layer x10; Effective Threshold, [mV]");
     tgr->SetMaximum(70);
     tgr->SetMinimum(5);
     tgr->Draw("pcol");
   
     gPad->SetPhi(45);
     gPad->SetTheta(45);
    
     cn2->SaveAs(Form("SURF-SECTOR-effThr-%s.png",picname.c_str()));
     cn2->SaveAs(Form("SURF-SECTOR-effThr-%s.pdf",picname.c_str()));
     delete cn2;
   
     int icol = 0;
     //std::vector<int> cols = {2,3,4,5,6,7,8,46};
     //std::vector<int> cols = {98,209,4,94,222,225,108,46};
     //std::vector<int> cols = {61,65,71,74,78,81,85,88};
     std::vector<int> cols = {209,211,214,216,222,224,225,227};
   
     TCanvas *tc = new TCanvas();
     tc->SetCanvasSize(1200,800);
     gStyle->SetOptStat(0);
     gStyle->SetOptTitle(0);
   
     gPad->SetGrid();
   
     auto lmg = new TMultiGraph("lmg","lmg");
   
     TLegend *lgnd = new TLegend(0.9,0.7,0.97,0.4);
     lgnd->SetHeader("Layers:","C");
   
     int maxRangeX;
     if(stgc){
        maxRangeX = 1856;
     }else{
        maxRangeX = 8192;
     }
   
     for(auto &l: layer_efft){
   
        l.second->SetMarkerStyle(7);
        l.second->SetMarkerSize(1);
        l.second->SetMarkerColor(cols.at(icol));
         
        lmg->Add(l.second);
        TMarker *mk = new TMarker(0,0,21);
        mk->SetMarkerSize(2);     
        mk->SetMarkerColor(cols.at(icol));
        lgnd->AddEntry(mk, l.first.c_str(),"p");
        icol++;
     }
   
     lmg->SetTitle("; Mapped channel; Effective threshold per layer,[mV]");
     gStyle->SetTitleOffset(1,"x");
     lmg->GetHistogram()->GetXaxis()->SetRangeUser(-1,maxRangeX);
     lmg->SetMaximum(800);
     lmg->SetMinimum(0);
     lmg->Draw("AP");
   
     lgnd->Draw();
   
     for(int i=0; i<8; i++){
        TLine *ln = new TLine(0,i*100,1856,i*100);
        ln->SetLineColor(kBlack);
        ln->Draw("SAME");
     }
   
     if(stgc){
        TLine *ln4 = new TLine(576,0,576,800);
        ln4->SetLineColor(kYellow+2);
        ln4->SetLineStyle(9);
        ln4->SetLineWidth(3);
        ln4->Draw("SAME");
     }
   
     tc->SaveAs(Form("BIG-EFFTHR-COMBINED-%s.png",picname.c_str()));
     tc->SaveAs(Form("BIG-EFFTHR-COMBINED-%s.pdf",picname.c_str()));
   
     delete tc;

 
     //////////////////////////////////////
     float mean = std::accumulate(L1_efft_vals.begin(),
                                  L1_efft_vals.end(),0.0)/L1_efft_vals.size();
     float sumSQ = std::inner_product(L1_efft_vals.begin(),
                                      L1_efft_vals.end(), 
                                      L1_efft_vals.begin(), 0.0);
     float effRMS = std::sqrt(sumSQ / L1_efft_vals.size() - mean * mean);

     std::cout<<"L1 strip effectie thr .stats:"
              <<"\nMean="<<mean
              <<"\nRMS="<<effRMS<<std::endl;
     
     std::cout<<"\n\nDONE PLOTTING"<<std::endl;
  }

   f->Close();

}
/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////
int nsw::BigThresholds::getRadius(std::string febname, int &layer){

   if(febname.find("Q")!=std::string::npos && 
      febname.find("PFEB")!=std::string::npos
      ){
      //  
      int quad;
      std::string tail;
      sscanf(febname.c_str(),"PFEB_L%iQ%i_%s",&layer,&quad,tail.c_str());    
      return quad;

   }else if(
      febname.find("Q")!=std::string::npos && 
      febname.find("SFEB")!=std::string::npos
      ){

      int quad;
      std::string tail;
      sscanf(febname.c_str(),"SFEB_L%iQ%i_%s",&layer,&quad,tail.c_str());    
      return quad;

   }else if(
      febname.find("MMFE")!=std::string::npos && 
      febname.find("8")!=std::string::npos
     ){

     //std::cout<<"FOUND MMFE8:"<<febname<<std::endl; 
     int radius,pcb;
     std::string side;
     std::string end = febname.substr(febname.length()-3,3);
     
     std::string ipho = end.substr(0,2);
     std::string lr = end.substr(end.length()-1,1);

     sscanf(febname.c_str(),"MMFE8_L%iP%i_%s",&layer,&pcb,side.c_str()); 
//     /////////////////////////////////////////////////
     if(pcb==1 && lr=="R"){
       radius=0;
     }else if(pcb==1 && lr=="L"){
       radius=1;
     }else if(pcb==2 && lr=="R"){
       radius=2;
     }else if(pcb==2 && lr=="L"){
       radius=3;
     }else if(pcb==3 && lr=="R"){
       radius=4;
     }else if(pcb==3 && lr=="L"){
       radius=5;
     }else if(pcb==4 && lr=="R"){
       radius=6;
     }else if(pcb==4 && lr=="L"){
       radius=7;
     }else if(pcb==5 && lr=="R"){
       radius=8;
     }else if(pcb==5 && lr=="L"){
       radius=9;
     }else if(pcb==6 && lr=="R"){
       radius=10;
     }else if(pcb==6 && lr=="L"){
       radius=11;
     }else if(pcb==7 && lr=="R"){
       radius=12;
     }else if(pcb==7 && lr=="L"){
       radius=13;
     }else if(pcb==8 && lr=="R"){
       radius=14;
     }else if(pcb==8 && lr=="L"){
       radius=15;
     }else{
       radius=-1;
     }
     ///////////////////////////////////////////////
     if(ipho=="IP"){
        layer=layer-1;
     }else if(ipho=="HO"){
        layer=layer+3;
     }else{
        layer=-1;
     }
     ///////////////////////////////////////////////
     //std::cout<<"Obtained mapped:"
     //         <<"LAYER="<<layer	
     //         <<"RADIUS="<<radius	
     //         <<std::endl;

     return radius;   
   }else{
     return -1;
   }

}

int nsw::BigThresholds::getSectorChan(std::string fename,
                                      int chan,
                                      int vmm){

  int        this_radius, this_vmm, this_chan;
  int        layer = std::atoi(fename.substr(7, 1).c_str());
  int        pcb   = std::atoi(fename.substr(9, 1).c_str());
  const char quad  = fename[11];
  const char side  = fename[13];
 
  if (((layer == 1 || layer == 3) && quad == 'H') ||
      ((layer == 2 || layer == 4) && quad == 'I')) {
    if (side == 'R') {
      this_radius = 2 * (pcb - 1);
      this_vmm    = 7 - vmm;
      this_chan   = 63 - chan;
    } else {
      this_radius = 2 * (pcb - 1) + 1;
      this_vmm    = vmm;
      this_chan   = chan;
    }
  } else {
    if (side == 'L') {
      this_radius = 2 * (pcb - 1);
      this_vmm    = 7 - vmm;
      this_chan   = 63 - chan;

    } else {
      this_radius = 2 * (pcb - 1) + 1;
      this_vmm    = vmm;
      this_chan   = chan;
    }
  }

  return this_radius*512+this_vmm*64+this_chan;

}


void nsw::BigThresholds::baselineFullSector(std::string rootfile,
                                            std::string picname){

  std::cout<<rootfile<<std::endl;
  TFile *f = new TFile(rootfile.c_str());
  std::cout<< f <<std::endl;
  std::string tree = "bl_tree";

  TTree *ftree=(TTree* )f->Get(tree.c_str());

  TTreeReader tr(tree.c_str(), f);

  TTreeReaderValue<std::string> ifeb(tr,"board");
  TTreeReaderValue<int> CHAN(tr,"channel_id");
  TTreeReaderValue<int> VMM(tr,"cvmm");
  TTreeReaderValue<float> SMP(tr,"sample");

  std::map<std::string,TH2D*> BB;
  std::map<std::string,TH1D*> slice;

  std::string this_feb;
  int vmm,chan;
  float samp;

  int totent = ftree->GetEntries();
  int ent = 0;
  while(tr.Next()){

    if(ent==totent){break;}
    this_feb = *ifeb;
    chan = *CHAN;
    vmm = *VMM;
    samp = *SMP;

    int this_layer;
    int R = getRadius(this_feb,this_layer);
    std::string keybase = "-L-"+std::to_string(this_layer);
    std::string blkey = "B"+keybase;

    int map_chan = getSectorChan(this_feb,chan,vmm);

    if(BB.find(blkey)==BB.end()){
       BB[blkey] = new TH2D(blkey.c_str(),blkey.c_str(),8194,-1,8193,350,50,300);
    }
   
    BB[blkey]->Fill(map_chan,samp);    

    if(this_layer==0 && R==14){
       std::string ivmm = std::to_string(vmm);
       if(slice.find(ivmm)==slice.end()){
          slice[ivmm] = new TH1D(ivmm.c_str(),ivmm.c_str(),200,50,250);
       }
       slice[ivmm]->Fill(samp);
    }

    printf("SAMPLE[%i/%i]\r",totent,ent);

    if(ent%1000==0){
      std::cout<<"\n"<<this_layer<<"\n"
               <<map_chan<<"\n"
               <<chan<<"\n"
               <<this_feb<<"\n"
               <<vmm<<"\n"
               <<samp<<"\n"
               <<blkey<<"\n"
               <<BB[blkey]->GetEntries()<<"\n"
               <<BB.size()<<"\n"<<std::endl;
    }
    ent++;

  }
  std::cout<<"PEZDA"<<std::endl;

  for(auto &b:BB){
     int L;
     //std::cout<<"A"<<std::endl;
     sscanf(b.first.c_str(),"B-L-%i",&L);
     //std::cout<<"B "<<L<<std::endl;
     TCanvas *c = new TCanvas();
     gStyle->SetOptTitle(0);
     gStyle->SetOptStat(0);
     //std::cout<<"C"<<std::endl;
     b.second->GetXaxis()->SetTitle("Mapped channel");
     //std::cout<<"D"<<std::endl;
     b.second->GetYaxis()->SetTitle("V_{baseline},[mV]");
     //std::cout<<"E"<<std::endl;
     b.second->Draw("COLZ");
     //std::cout<<"F"<<std::endl;
     c->SaveAs(Form("BIG-BASELINE-L%i-%s.png",L,picname.c_str()));
     c->SaveAs(Form("BIG-BASELINE-L%i-%s.pdf",L,picname.c_str()));
     //std::cout<<"G"<<std::endl;
     delete c;
  }

  for(auto &v: slice){
     TCanvas *c1 = new TCanvas();

     gStyle->SetOptStat(0);
     gStyle->SetOptTitle(0);
     v.second->GetXaxis()->SetTitle("Sample,[mV]");
     v.second->GetYaxis()->SetTitle("N_{samples}");
     v.second->Draw();

     c1->SaveAs(Form("SLICE-R14-Vmm%s-%s.png",v.first.c_str(),picname.c_str()));
     c1->SaveAs(Form("SLICE-R14-Vmm%s-%s.pdf",v.first.c_str(),picname.c_str()));
     delete c1;
  }

}

float nsw::BigThresholds::getMean(std::vector<float> &vals){

   float res = std::accumulate(vals.begin(),vals.end(),0.0)/vals.size();
   return res;

}

float nsw::BigThresholds::getRMS(std::vector<float> &vals, float &mean){

   float sumSq = std::inner_product(vals.begin(), vals.end(), vals.begin(), 0.0);  
   float rms = std::sqrt(sumSq / vals.size() - mean*mean);
   return rms;

}












