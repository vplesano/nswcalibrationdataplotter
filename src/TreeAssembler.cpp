#include "NSWCalibDataPlotter/TreeAssembler.h"
#include "NSWCalibDataPlotter/ThresholdPlotter.h"
#include "NSWRead/NSWChannelID.h"
#include "dirent.h"

nsw::TreeAssembler::TreeAssembler(){}

void nsw::TreeAssembler::fill_pulser_dac(std::string data_path, std::string archive_path){

  std::vector<std::string> pdac_files;

  if(data_path.at(data_path.length()-1)!='/'){data_path+="/";}

  DIR *t_dir=NULL;
  struct dirent *t_ent=NULL;
  t_dir = opendir(data_path.c_str());  
  printf("\nreading VMM pulser data file directory\n");
  std::string s = ".";  
  if(t_dir!=NULL)
  {
    while(t_ent=readdir(t_dir))
    {
      std::string file_n = t_ent->d_name;
      if(file_n.at(0)!=s && file_n.find("TPDAC_samples")!=std::string::npos ||
         file_n.at(0)!=s && file_n.find("tp_sample_data")!=std::string::npos
         ){
         pdac_files.push_back(t_ent->d_name);
         std::cout<<"Found pulser data: "<<file_n<<std::endl;
      }
//      std::cout<<file_n<<std::endl;
    }
  }
  else{std::cout<<"t_dir==NULL"<<std::endl;}  

//======= tonn of checks ========================
  for(int k=0;k<pdac_files.size();k++)
  {
    printf("\n %s",pdac_files[k].c_str());
  }  

  std::sort(pdac_files.begin(),pdac_files.end());
  std::cout<<"\n sorted file vector \n"<<std::endl;  
  for(int k=0;k<pdac_files.size();k++)
  {
    printf("\n %s",pdac_files[k].c_str());
  }  
  std::cout <<"\n"<<std::endl;

  for(int k=0;k<pdac_files.size();k++)
  {
    printf("\n %s",pdac_files[k].c_str());
  }  
//=================================================
  int n_boards = pdac_files.size();
      
  printf("\ndirectory contains %i board output thr_files\n",n_boards);

//============= writing common threshold file =================================
  
  std::ifstream ith_thr;
  std::ofstream new_thr;
  
  std::string tline, nline;
  std::string pulser_file = "common_txt/PulserDac.txt";
  new_thr.open(pulser_file);

  for(int i_board=0; i_board <n_boards; i_board++)
  {
    ith_thr.open(data_path+pdac_files[i_board]);

    std::cout<<"\nwritting "<<data_path+pdac_files[i_board]<<" file to single pulser txt\n"<<std::endl;
    while(std::getline(ith_thr, tline,'\n'))
    {
   //   std::cout<<tline<<std::endl;
      new_thr<<tline<<std::endl;
    }
    
    std::cout<<"\nWritten "<<i_board<<" file to common tree txt\n"<<std::endl;

    ith_thr.close();
  }
  new_thr.close();
//========================================================================================

  FILE *in_chan = fopen(Form("%s",pulser_file.c_str()),"r");

  float sam_mv; //thrmin,thrmax;
  int dac, vmm, sam, lay, rad, sector, side;

  bool oldFormat = false;

  std::string fname_str;
  char fname[20];

  TFile *f = new TFile("root_files/pulser_dac_tree.root","RECREATE");
  TTree *t = new TTree("pdac_tree","Tree of pulser dac samples");

  std::cout<<"\n declared variables trees and root file\n"<<std::endl;

   //---------- channel parameters -----------------------------
   t->Branch("side",&side, "side/I");
   t->Branch("sector",&sector, "sector/I");
   t->Branch("board",&fname_str);
   t->Branch("layer",&lay, "layer/I");
   t->Branch("radius",&rad, "radius/I");
   t->Branch("vmm", &vmm, "vmm/I");
   t->Branch("DAC", &dac, "DAC/I");
   t->Branch("sample", &sam, "sample/I");
   t->Branch("sample_mV", &sam_mv, "sample_mV/F");

   char line_c[400];
   int line_nr=0;
   
   std::cout<<"\ndeclared branches\n"<<std::endl;

   std::cout<<"=============== OUTPUT DEBUG ================="<<std::endl;
   while(fgets(line_c,400,in_chan))
    {
      if(line_nr==0 && line_c[0]=='M'){
         oldFormat=true;
      }

   //   std::cout<<"scanning "<<line_nr<< "line\n"<<std::endl;
   //   std::cout<<line_c<<std::endl;
      if(!oldFormat){
         sscanf(&line_c[0],"%i %i %s %i %i %i %f",
         &side, &sector, &fname, &vmm, &dac, &sam, &sam_mv);
      }else{
         sscanf(&line_c[0],"%s %i %i %i %f",
         &fname, &vmm, &dac, &sam, &sam_mv);
         side = 1;
         sector = 7;
      }
      //====== determining layer/radius, humbly pirated from NSWRead utils ===
     
      fname_str = fname;
      //std::cout<<"FOUND="<<fname_str<<std::endl;

      if(fname_str.length()==10){
         if(fname_str.find("0000")!=std::string::npos){
           fname_str = "MMFE8_L1P1_IPL";
         }else if(fname_str.find("0001")!=std::string::npos){
           fname_str = "MMFE8_L1P2_IPL";
         }else if(fname_str.find("0002")!=std::string::npos){
           fname_str = "MMFE8_L1P3_IPL";
         }else if(fname_str.find("0003")!=std::string::npos){
           fname_str = "MMFE8_L1P4_IPL";
         }else if(fname_str.find("0004")!=std::string::npos){
           fname_str = "MMFE8_L1P5_IPL";
         }else if(fname_str.find("0005")!=std::string::npos){
           fname_str = "MMFE8_L1P6_IPL";
         }else if(fname_str.find("0006")!=std::string::npos){
           fname_str = "MMFE8_L1P7_IPL";
         }else if(fname_str.find("0007")!=std::string::npos){
           fname_str = "MMFE8_L1P8_IPL:";
         }else if(fname_str.find("0008")!=std::string::npos){
           fname_str = "MMFE8_L2P1_IPL";
         }else if(fname_str.find("0009")!=std::string::npos){
           fname_str = "MMFE8_L2P1_IPR";
         }else{
           std::cout<<"What is this board? : "<<fname_str<<std::endl;
           sleep(2);
         }
      }

      std::cout<<"USING="<<fname_str<<std::endl;

      std::string checkName = ""; 

     // int n_slashes;
     // for(auto &c: fname_str){
     //    if(c=='/'){
     //       n_slashes++;
     //    }
     // }

      //std::cout<<"Name has {"<<n_slashes<<"} slashes"<<std::endl;

      if(fname_str.length()==14){
   
         std::cout<<"Parsing test site name scheme"<<std::endl;
         if(side==1){
           checkName +="A";
         }else{
           checkName +="C";
         }
         std::string str_sect = Form("%02i",sector);
         checkName += str_sect+"_M";
         if(fname_str.find("MMFE8")!=std::string::npos){
           checkName += fname_str.substr(6,8);
         }
         checkName += "_A";
      

         std::cout<<"CHECKING="<<checkName<<std::endl;
   
         int buf = checkName.length();
         char fname_char[buf+1];
         strcpy(fname_char, checkName.c_str());

         NSWChannelID id;
         int res = id.ParseBoardID (fname_char);
         if(res == 0){
            char string[40];
            char boardID[20];
            id.GetBoardID (boardID);
            printf ("%d: %s\n", id.GetChannelID(), boardID);
            lay = id.GetLayer();
            rad = id.GetRadius();
            std::cout<<"FOR["<<fname_char
                     <<"\nL="<<lay<<",R="<<rad<<std::endl;
         }else{
            std::cout<<"Parsing went wrong!"<<std::endl;
         }
      }else{

         std::cout<<"Parsing P1 name scheme"<<std::endl;

         std::string info;
         std::vector<std::string> fname_split;
         std::istringstream inputstr(fname_str);
         while(std::getline(inputstr,info,'/')){
            fname_split.push_back(info);
         }

         //int nr = 0;
         //for(auto &i: fname_split){
         //   std::cout<<"["<<nr<<"] - "<<i<<std::endl;
         //   nr++;
         //}

         sscanf(fname_split[5].c_str(),"L%i",&lay);
         sscanf(fname_split[6].c_str(),"R%i",&rad);

         //std::cout<<"found:"
         //         <<"\nNAME:"<<fname_str
         //         <<"\nlay="<<lay
         //         <<"\nrad="<<rad<<std::endl;
         
      }

      //if(fname_str.size() > 10 and fname_str.substr(0,2)=="MM"){

      //  lay = std::atoi(fname_str.substr(7,1).c_str());

      //  if(fname_str.substr(11,1)=="I"){
      //    lay = lay - 1;
      //  }
      //  else{
      //    lay = 8 - lay; 
      //  }
      //  if(lay%2==0){
      //    if(fname_str.substr(13,1)=="L"){
      //      rad = 2*(std::atoi(fname_str.substr(9,1).c_str())-1);
      //    }
      //    else{
      //      rad = 2*(std::atoi(fname_str.substr(9,1).c_str())-1)+1;
      //    }
      //  }
      //  else{
      //    if(fname_str.substr(13,1)=="L"){
      //      rad = 2*(std::atoi(fname_str.substr(9,1).c_str())-1)+1;
      //    }
      //    else{
      //      rad = 2*(std::atoi(fname_str.substr(9,1).c_str())-1);
      //    }  
      //  }
      //std::cout<<"MMFE-"<<fname_str<<" belongs to:"
      //         <<"\nlayer="<<lay
      //         <<"\nradius="<<rad<<std::endl;

      t->Fill();
      line_nr++; 
      //}
      ////=========================================================================
      //else{
      //  continue;
      //}  
    }
  t->Write();
  std::cout<<"\nwritten the branches\n"<<std::endl;

  f->Write();  
  
  printf("\nfile have = %i lines\n",line_nr);

  fclose(in_chan);

  nsw::ThresholdPlotter tplt;

  std::string S;
  if(side==-1){
     S = "C";
  }else{
     S = "A";
  }
  std::string rprefix = S+"_"+std::to_string(sector);

  std::string copy_command = 
              Form("cp root_files/pulser_dac_tree.root %s/%s_pulser_dac_tree.root", 
                   archive_path.c_str(),rprefix.c_str());
  system(copy_command.c_str());

  //gSystem->CopyFile("%s/%s-pulser_dac_tree.root", archive_path.c_str(),rprefix.c_str());

  printf("closed in_chan\n"); 
  std::cout<<"=============================================================\n"<<std::endl;
  std::cout<<"============== PULSER DAC TREE UPDATED ! ====================\n"<<std::endl;
  std::cout<<"=============================================================\n"<<std::endl;

  //exit(1);

}

//============================================================
//          FILLING BASELINE TREE
//============================================================

void nsw::TreeAssembler::fill_bl(std::string data_path, std::string archive_path){

  std::vector<std::string> bl_files;
  
  if(data_path.at(data_path.length()-1)!='/'){data_path+='/';}
 
  DIR *t_dir=NULL;
  struct dirent *t_ent=NULL;

  t_dir = opendir(data_path.c_str());  

  printf("\nreading baseline file directory\n");
  std::string s = ".";  
  if(t_dir!=NULL)
  {
    while(t_ent=readdir(t_dir))
    {
      std::string file_n = t_ent->d_name;
      if(file_n.at(0)!=s && file_n.find("baseline_samples")!=std::string::npos){
        bl_files.push_back(t_ent->d_name);
        std::cout<<file_n<<std::endl;
      }
    }
  }

//======= tonn of checks ========================
  for(int k=0;k<bl_files.size();k++)
  {
    printf("\n %s",bl_files[k].c_str());
  }  

  std::sort(bl_files.begin(),bl_files.end());
  std::cout<<"\n sorted file vector \n"<<std::endl;  
  for(int k=0;k<bl_files.size();k++)
  {
    printf("\n %s",bl_files[k].c_str());
  }  
  std::cout <<"\n"<<std::endl;

  for(int k=0;k<bl_files.size();k++)
  {
    printf("\n %s",bl_files[k].c_str());
  }  
//=================================================
  int n_boards = bl_files.size();
      
  printf("\ndirectory contains %i board output thr_files\n",n_boards);

//============= writing common threshold file =================================
  
   std::ifstream ith_thr;
  
  std::ofstream new_thr;//, arch_thr;
  
  std::string tline, nline;
  std::string bl_file = "common_txt/Baselines.txt";

  new_thr.open(bl_file);

  for(int i_board=0; i_board <n_boards; i_board++)
  {
    ith_thr.open(data_path+bl_files[i_board]);

    std::cout<<"\nwritting boad bl_files to single file\n"<<std::endl;
    while(std::getline(ith_thr, tline,'\n'))
    {
  //    std::cout<<wline<<std::endl;
      new_thr<<tline<<std::endl;
    }
    
    std::cout<<"\nWritten "<<i_board<<" file to common tree txt\n"<<std::endl;

    ith_thr.close();
  }
  new_thr.close();


//========================================================================================

  FILE *in_chan = fopen(Form("%s",bl_file.c_str()),"r");

  float sam, rms;
  int ch, cvmm, side, sector;

  std::string fname_str;
  char fname[10];

  TFile *f = new TFile("root_files/baseline_tree.root","RECREATE");
  TTree *t = new TTree("bl_tree","Tree of baseline samples");

  std::cout<<"\n declared variables trees and root file\n"<<std::endl;

   //---------- channel parameters -----------------------------
   t->Branch("side",&side, "side/I");
   t->Branch("sector",&sector,"sector/I");
   t->Branch("board",&fname_str);
   t->Branch("cvmm", &cvmm, "cvmm/I");
   t->Branch("channel_id", &ch, "channel_id/I");
   t->Branch("sample", &sam, "sample/F");
   t->Branch("rms", &rms, "rms/F");

   char line_c[200];
   int line_nr=0;
   
   std::cout<<"\ndeclared branches\n"<<std::endl;

   while(fgets(line_c,400,in_chan))
    {
    //  cout<<"scanning "<<line_nr<< "line\n"<<endl;
      sscanf(&line_c[0],"%i %i %s %i %i %f %f",
      &side, &sector, &fname, &cvmm, &ch, &sam, &rms);
      fname_str=fname;
      t->Fill();
      line_nr++;
    //  cout<< fname_str;
    }
  t->Write();
  std::cout<<"\nwritten the branches\n"<<std::endl;

  f->Write();  
  
  printf("\nfile have = %i lines\n",line_nr);

  fclose(in_chan);

  std::string S;
  if(side==-1){
     S = "C";
  }else{
     S = "A";
  }
  std::string rprefix = S+"_"+std::to_string(sector);

  std::string copy_command = 
              Form("cp root_files/baseline_tree.root %s/%s_baseline_tree.root", 
                   archive_path.c_str(),rprefix.c_str());
  system(copy_command.c_str());

  //gSystem->CopyFile("%s/%s-baseline_tree.root", archive_path.c_str(),rprefix.c_str());

  printf("closed in_chan\n"); 
  std::cout<<"=========================================================\n"<<std::endl;
  std::cout<<"============== BASELINE TREE UPDATED ====================\n"<<std::endl;
  std::cout<<"=========================================================\n"<<std::endl;

}

//=========================================================
//          FILLING THRESHOLD TREE
//=========================================================
void nsw::TreeAssembler::fill_thre(std::string data_path, std::string archive_path){

  std::vector<std::string> thr_files;
  if(data_path.at(data_path.length()-1)!='/'){data_path+='/';}


  DIR *t_dir=NULL;
  struct dirent *t_ent=NULL;
  t_dir = opendir(data_path.c_str());  
  printf("\nreading calibration file directory\n");
  std::string s = ".";

  bool new_naming = false;

  int cntr=0; 
  if(t_dir!=NULL)
  {
    while(t_ent=readdir(t_dir))
    { 
      std::string file_n = t_ent->d_name;
      std::cout<<file_n<<std::endl;
      if(cntr==0){
        if(file_n.find('/')){
           new_naming=true;
        }
      }
      if(file_n.at(0)!=s && file_n.find("thresholds")!=std::string::npos){
         thr_files.push_back(t_ent->d_name);
      }
      cntr++;
    }
  }
 cntr=0;

  if(new_naming==true){
     std::cout<<"reading NEW NAMING scheme"<<std::endl;
     sleep(1);
  }
//======= tonn of checks ========================
  for(int k=0;k<thr_files.size();k++)
  {
    printf("\n %s",thr_files[k].c_str());
  }  

  std::sort(thr_files.begin(),thr_files.end());
  std::cout<<"\n sorted file vector \n"<<std::endl;  
  for(int k=0;k<thr_files.size();k++)
  {
    printf("\n %s",thr_files[k].c_str());
  }  
  std::cout <<"\n"<<std::endl;

  for(int k=0;k<thr_files.size();k++)
  {
    printf("\n %s",thr_files[k].c_str());
  }  
//=================================================
  int n_boards = thr_files.size();
      
  printf("\ndirectory contains %i board output thr_files\n",n_boards);

//============= writing common threshold file =================================
  
  std::ifstream ith_thr;
  
  std::ofstream new_thr;//, arch_thr;
  
  std::string tline, nline;
  std::string thr_file = "common_txt/Untrimmed_thresholds.txt";

  new_thr.open(thr_file);

  for(int i_board=0; i_board <n_boards; i_board++)
  {
    ith_thr.open(data_path+thr_files[i_board]);

    std::cout<<"\nwritting board thr_files to single file\n"<<std::endl;
    while(std::getline(ith_thr, tline,'\n'))
    {
  //    std::cout<<wline<<std::endl;
      new_thr<<tline<<std::endl;
    }
    
    std::cout<<"\nWritten "<<i_board<<" file to calib tree txt\n"<<std::endl;

    ith_thr.close();
  }
  new_thr.close();
//=============================================================================

  FILE *in_fchan = fopen(Form("%s",thr_file.c_str()),"r");
  std::ifstream in_chan;
  in_chan.open(thr_file);

  float thr, thrmin,thrmax;
  int ch, cvmm, side, sector;

  std::string fname_str;
  char fname[10];

  TFile *f = new TFile("root_files/threshold_tree.root","RECREATE");
  TTree *t = new TTree("thr_tree","Tree of untrimmed channel thresholds");

  std::cout<<"\n declared variables trees and root file\n"<<std::endl;

   //---------- channel parameters -----------------------------
   t->Branch("side",&side, "side/I");
   t->Branch("sector",&sector, "sector/I");
   t->Branch("board",&fname_str);
   t->Branch("cvmm", &cvmm, "cvmm/I");
   t->Branch("channel_id", &ch, "channel_id/I");
   t->Branch("channel_thr", &thr, "channel_thr/F");
   t->Branch("channel_thrmin", &thrmin, "channel_thrmin/I");
   t->Branch("channel_thrmax", &thrmax, "channel_thrmax/I");

   char line_c[400];
   int line_nr=0;
   std::string line;

   std::cout<<"\ndeclared branches\n"<<std::endl;

   std::string head, f_name, rest;
   size_t splitHere = 0;
   size_t nameHere = 0;

   if(new_naming){
      while(std::getline(in_chan,line)){
         std::cout<<"\n\nscanning "<<line_nr<< " line \n"
                  <<line<<std::endl;
         int tabs = 0;
         int nchar = 0;
         for(auto &c: line){
            //std::cout<<c<<std::endl;
            if(c=='\t'){
               tabs++;
               if(tabs==2){
                  nameHere=nchar;              
               } 
               if(tabs==3){
                  splitHere = nchar;
                  //std::cout<<"FOUND 3rd tab:"
                  //         <<"\n char_NR="<<nchar
                  //         <<"\n namehere="<<nameHere<<std::endl;
                  break;
            }
            }
            nchar++;
         }
     
         //std::cout<<"Splitting data"
         //         <<"\nnchar = "<<nchar
         //         <<"\ntabs = "<<tabs
         //         <<"\nhead = "<<line.substr(0,nameHere+1)
         //         <<"\nfeb_name = "<<line.substr(nameHere+1,splitHere-nameHere-1)
         //         <<"\nrest = "<<line.substr(splitHere,line.length())
         //         <<std::endl;
         head = line.substr(0,nameHere);
         rest = line.substr(splitHere+1,line.length());
         f_name = line.substr(nameHere+1,splitHere-nameHere-1);

         if(line_nr==3){sleep(5);}
         sscanf(head.c_str(),"%i %i", &side, &sector);
         sscanf(rest.c_str(),"%i %i %f %f %f ",&cvmm, &ch, &thr, &thrmin, &thrmax);

         //printf("A");
         fname_str=f_name;
         //printf("B");
         t->Fill();
         //printf("C");
         line_nr++;
         std::cout<< fname_str;


      }
   }else{
      while(fgets(line_c,500,in_fchan))
      {
         std::cout<<"scanning "<<line_nr<< " line \n"
                  <<line_c<<std::endl;
         sscanf(&line_c[0],"%i %i %s %i %i %f %f %f",
         &side, &sector, &fname, &cvmm, &ch, &thr, &thrmin, &thrmax);
         printf("A");
         fname_str=fname;
         printf("B");
         t->Fill();
         printf("C");
         line_nr++;
         std::cout<< fname_str;

      }
  }
  t->Write();
  std::cout<<"\nwritten the branches\n"<<std::endl;

  f->Write();  
  
  printf("\nfile have = %i lines\n",line_nr);

  fclose(in_fchan);
  in_chan.close();

  std::string S;
  if(side==-1){
     S = "C";
  }else{
     S = "A";
  }
  std::string rprefix = S+"_"+std::to_string(sector);
 
  std::string copy_command = 
              Form("cp root_files/threshold_tree.root %s/%s_threshold_tree.root", 
                   archive_path.c_str(),rprefix.c_str());
  system(copy_command.c_str());

  //gSystem->CopyFile("%s/%s-threshold_tree.root", archive_path.c_str(),rprefix.c_str());

  printf("closed in_chan\n"); 
  std::cout<<"===================================================\n"<<std::endl;
  std::cout<<"============ THRESHOLD TREE UPDATED ===============\n"<<std::endl;
  std::cout<<"===================================================\n"<<std::endl;


}
//================================================================
//            FILLING MAIN CALIBRATION DATA TREE
//================================================================

void nsw::TreeAssembler::fill_ntuple(std::string data_path, std::string archive_path){

  bool new_naming = false;

  std::vector<std::string> files;

  if(data_path.at(data_path.length()-1)!='/'){data_path+='/';}
  
  DIR *d_dir=NULL;
  struct dirent *ent=NULL;

  d_dir = opendir(data_path.c_str());  

  printf("\nreading calibration file directory\n");
  std::string s = ".";
  int cntr=0;
  if(d_dir!=NULL)
  {
    while(ent=readdir(d_dir))
    {
      std::string file_n = ent->d_name;
      if(cntr==0 && file_n.find('/')){
        new_naming=true;
      }
      if(file_n.at(0)!= s && file_n.find("calibration_data")!=std::string::npos){
         files.push_back(ent->d_name);
         std::cout<<file_n<<std::endl;
      }
    }
  }
//======= tonn of checks ========================
  for(int k=0;k<files.size();k++)
  {
    printf("\n %s",files[k].c_str());
  }  

  std::sort(files.begin(),files.end());
  std::cout<<"\n sorted file vector \n"<<std::endl;  
  for(int k=0;k<files.size();k++)
  {
    printf("\n %s",files[k].c_str());
  }  
  std::cout <<"\n"<<std::endl;
  
  for(int k=0;k<files.size();k++)
  {
    printf("\n %s",files[k].c_str());
  }  
//=================================================
  int n_boards = files.size();
      
  printf("\ndirectory contains %i board output files\n",n_boards);

//============== part that writes single txt for all boards ======================= 
  
   std::ifstream ith_txt;
  
  std::ofstream new_txt;
  
  std::string cline, wline;
  std::string in_file="common_txt/Calib_data.txt";
  new_txt.open(in_file);

  for(int i_board=0; i_board <n_boards; i_board++)
  {
    ith_txt.open(data_path+files[i_board]);

    std::cout<<"\nwritting boad files to single file\n"<<std::endl;
    while(std::getline(ith_txt, wline,'\n'))
    {
  //    std::cout<<wline<<std::endl;
      new_txt<<wline<<std::endl;
    }
    
    std::cout<<"\nWritten "<<i_board<<" file to calib tree txt\n"<<std::endl;

    ith_txt.close();
  }
  new_txt.close();

//=================================================================
//============== FILLING THE GODDAMN TREE =========================
//=================================================================
  FILE *in_chan = fopen(in_file.c_str(),"r");

  std::ifstream inChan;
  inChan.open(in_file);

  float med, rms, eff_th, slope, eff_w_trim, ch_trim_thr;
  int ch, cvmm, trim, ch_mask, side, sector;

  std::string fname_str;
  char fname[10];
  float vmm_med, vmm_rms, vmm_trim_med, vmm_eff_th, vmm_thdac_slope, vmm_thdac_offset;
  int gvmm, vmm_thdac, linkID;   

  TFile *f = new TFile("root_files/calib_data_tree.root","RECREATE");
  TTree *t = new TTree("par_tree","Tree of calibration run output parameters");

  std::cout<<"\n declared variables trees and root file\n"<<std::endl;

   //---------- channel parameters -----------------------------
   t->Branch("side",&side,"side/I");
   t->Branch("sector",&sector,"sector/I");
   t->Branch("linkID",&linkID,"linkID/I");
   t->Branch("board",&fname_str);
   t->Branch("cvmm", &cvmm, "cvmm/I");
   t->Branch("channel_id", &ch, "channel_id/I");
   t->Branch("ch_baseline_med", &med, "ch_baseline_med/F");
   t->Branch("ch_baseline_rms", &rms, "ch_baseline_rms/F");
   t->Branch("ch_eff_threshold", &eff_th, "ch_eff_threshol/F");
   t->Branch("ch_eff_th_slope", &slope, "ch_eff_th_slope/F");
   t->Branch("ch_best_trim", &trim, "ch_best_trim/I");
   t->Branch("ch_eff_thr_w_best_trim", &eff_w_trim, "ch_eff_thr_w_best_trim/F");
   t->Branch("ch_trimmed_thr", &ch_trim_thr, "ch_trimmed_thr/F");
   t->Branch("ch_mask", &ch_mask, "ch_mask/I");
   //---------- chip global parameters --------------------------
   t->Branch("vmm_median", &vmm_med, "vmm_median/F");
   t->Branch("vmm_median_rms", &vmm_rms, "vmm_median_rms/F");
   t->Branch("vmm_median_trim_mid", &vmm_trim_med, "vmm_median_trim_mid/F");
   t->Branch("vmm_eff_thr", &vmm_eff_th, "vmm_eff_thr/F");
   t->Branch("vmm_thdac", &vmm_thdac, "vmm_thdac/I");
   t->Branch("vmm_thdac_slope", &vmm_thdac_slope, "vmm_thdac_slope/F");
   t->Branch("vmm_thdac_offset", &vmm_thdac_offset, "vmm_thdac_offset/F");
   //------------ threshold branch --------------------

   char line_c[400];
   int line_nr=0;
   std::string line, head, f_name, rest;  
   size_t splitHere = 0; 
   size_t nameHere = 0; 

   std::cout<<"\ndeclared branches\n"<<std::endl;

   NSWChannelID id;

   if(new_naming){
      std::cout<<"GOT a NEW naming SCHEME"<<std::endl;
      sleep(1);
      while(std::getline(inChan,line)){
         std::cout<<"\n\nscanning "<<line_nr<< " line \n"
                  <<line<<std::endl;
         int tabs = 0;
         int nchar = 0;
         for(auto &c: line){
            //std::cout<<c<<std::endl;
            if(c=='\t'){
               tabs++;
               if(tabs==2){
                  nameHere=nchar;              
               } 
               if(tabs==3){
                  splitHere = nchar;
                  //std::cout<<"FOUND 3rd tab:"
                  //         <<"\n char_NR="<<nchar
                  //         <<"\n namehere="<<nameHere<<std::endl;
                  break;
            }
            }
            nchar++;
         }
     
        // std::cout<<"Splitting data"
        //          <<"\nnchar = ["<<nchar<<"]"
        //          <<"\ntabs = ["<<tabs<<"]"
        //          <<"\nhead = ["<<line.substr(0,nameHere)<<"]"
        //          <<"\nrest = ["<<line.substr(splitHere+1,line.length())<<"]"
        //          <<"\nfeb_name = ["<<line.substr(nameHere+1,splitHere-nameHere-1)<<"]"
        //          <<std::endl;
         
         head = line.substr(0,nameHere+1);
         rest = line.substr(splitHere,line.length());
         f_name = line.substr(nameHere+1,splitHere-nameHere-1);

         //if(line_nr==200){exit(1);}
         sscanf(head.c_str(),"%i %i", &side, &sector);
         sscanf(rest.c_str(), "%i %i %f %f %f %f %f %f %f %f %i %i %f %f %f %f %i",
         &cvmm, &ch, &med, &rms, &eff_th, &slope,  
         &vmm_med, &vmm_rms, &vmm_trim_med, &vmm_eff_th, &vmm_thdac, &trim, 
         &ch_trim_thr, &eff_w_trim, &vmm_thdac_slope, &vmm_thdac_offset, &ch_mask );
        
         ///////////////////////////////////////////////////////////////////////////////
         fname_str=f_name;
         ///// tryna get the linkID out of the name ///////////
         int buf = f_name.length();
         char fname_char[buf+1];
         strcpy(fname_char, f_name.c_str());
         int res = id.ParseBoardID (fname_char);
         if(line_nr%100==0){
            std::cout<<"HAVE name: "<<fname_str
                     //<<"\nT name: "<<check_fname
                     //<<"\ncheck1="<<SECT
                     //<<"\ncheck2="<<side
                     //<<"\ncheck3="<<SIDE
                     //<<"\ncheck4="<<fname_str.substr(6,fname_str.length()-6)
                     <<std::endl;
         
            if(res == 0){
               char string[40];
               char boardID[20];
               id.GetBoardID (boardID);
               printf ("%d: %s\n", id.GetChannelID(), boardID);
            }else{
               std::cout<<"Parsing went wrong!"<<std::endl;
            }
         }
         //std::cout<<"PArsing success!"<<std::endl;
         linkID = id.GetChannelID();
         //std::cout<<"LINK_ID="<<linkID<<std::endl;
         /////////////////////
         t->Fill();
         //std::cout<<"HEH"<<std::endl;     
         line_nr++;
         //std::cout<< fname_str<<std::endl;
         //std::cout<< line_nr<<std::endl;

     }
  }else{
      while(fgets(line_c,400,in_chan)){

         std::cout<<"scanning "<<line_nr<< "line\n"<<std::endl;
         sscanf(&line_c[0],"%i %i %s %i %i %f %f %f %f %f %f %f %f %i %i %f %f %f %f %i",
         &side, &sector, &fname, &cvmm, &ch, &med, &rms, &eff_th, &slope,  
         &vmm_med, &vmm_rms, &vmm_trim_med, &vmm_eff_th, &vmm_thdac, &trim, 
         &ch_trim_thr, &eff_w_trim, &vmm_thdac_slope, &vmm_thdac_offset, &ch_mask);
         fname_str=fname;
         std::string SECT = std::to_string(sector);
         std::string SIDE;
         if(side==1){
            SIDE = "A";
         }else{
            SIDE = "C";
         }
         //std::cout<<"temp SIDE="<<SIDE<<std::endl;
         // this part should be adjusted for the geographical naming later!
         // AND for sTGC frontends!
         // ideally that should work more straighforward
         // transform fname_str with strcpy and pass to id.ParseBoardID()
         // then pass result to id.GetBoardID()
         // one hopes.....
         bool stgc = fname_str.find("Q") && fname_str.find("FEB");
         std::string endp,layqua,hoip,ftype;
         std::string check_fname;
         //std::cout<<"isSTGC="<<stgc<<std::endl;
         if(!stgc){
          check_fname = SIDE+SECT+"_M"+fname_str.substr(6,fname_str.length()-6);
         }else{
          //std::string chan_group = "A";
          int cnt=0;
          //std::cout<<"temp fname_str="<<fname_str<<std::endl;
          for(int i=0; i<fname_str.length(); i++){
             char this_char = fname_str.at(i);
             if(this_char=='_'){
               cnt++;
               continue;
             }
             if(cnt==0){
               ftype+=this_char;
             }
             if(cnt==1){
               layqua+=this_char;
             }
             if(cnt==2){
               hoip+=this_char;
             }
          }
          //std::cout<<"temp ftype="<<ftype<<std::endl;
          //std::cout<<"temp layqua="<<layqua<<std::endl;
          //std::cout<<"temp hoip="<<hoip<<std::endl;

          if(ftype.at(0)=='S'){
             endp="s";
          }else if(ftype.at(0)=='P'){
             endp="p";
          }else{
             endp="HZ";
          }
          //std::cout<<"temp endp="<<endp<<std::endl;
                
          std::string SECTstr;
          if(std::atoi(SECT.c_str())<10){
            SECTstr = "0"+SECT;
          }else{
            SECTstr = SECT;
          }
          //std::cout<<"temp SECT="<<SECTstr<<std::endl;
         
          check_fname = SIDE+SECTstr+"_"+hoip.substr(0,2)+"_"+layqua+
                        endp+"FEB";
          //std::cout<<"CHECK:"
          //         <<"\nstring: "<<fname_str
          //         <<"\nftype: "<<ftype
          //         <<"\nlayqua: "<<layqua
          //         <<"\nhoip: "<<hoip
          //         <<"\nSECTstr: "<<SECTstr
          //         <<"\nnew fname=: "<<check_fname
          //         <<std::endl;

         }
         /// tryna get the linkID out of the name ///////////
         int buf = check_fname.length();
         char fname_char[buf+1];
         strcpy(fname_char, check_fname.c_str());
         int res = id.ParseBoardID (fname_char);
         if(line_nr%100==0){
            std::cout<<"HAVE name: "<<fname_str
                     <<"\nTRANSFORMED name: "<<check_fname
                     <<"\ncheck1="<<SECT
                     <<"\ncheck2="<<side
                     <<"\ncheck3="<<SIDE
                     <<"\ncheck4="<<fname_str.substr(6,fname_str.length()-6)
                     <<std::endl;
         
            if(res == 0){
               char string[40];
               char boardID[20];
               id.GetBoardID (boardID);
               printf ("%d: %s\n", id.GetChannelID(), boardID);
            }else{
               std::cout<<"Parsing went wrong!"<<std::endl;
            }
         }
         //std::cout<<"PArsing success!"<<std::endl;
         linkID = id.GetChannelID();
         //std::cout<<"LINK_ID="<<linkID<<std::endl;
         /////////////////////
         t->Fill();
         line_nr++;
         //std::cout<< line_nr<<std::endl;

     }
  }
  t->Write();
  std::cout<<"\nwritten the branches\n"<<std::endl;

  f->Write();  
  
  printf("\nfile have = %i lines\n",line_nr);
  fclose(in_chan);
  ///////////////////////////////////////////////////
  std::string S;
  if(side==-1){
     S = "C";
  }else{
     S = "A";
  }
  std::string rprefix = S+"_"+std::to_string(sector);

  std::string copy_command = 
              Form("cp root_files/calib_data_tree.root %s/%s_calib_data_tree.root", 
                   archive_path.c_str(),rprefix.c_str());
  system(copy_command.c_str());

  printf("closed in_chan\n"); 
  std::cout<<"===========================================================\n"<<std::endl;
  std::cout<<"=========== CALIBRATION DATA TREE UPDATED =================\n"<<std::endl;
  std::cout<<"===========================================================\n"<<std::endl;

  //exit(1);
}
