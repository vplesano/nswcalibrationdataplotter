#include "NSWCalibDataPlotter/PulseCalib.h"
#include "cmath"
#include "stdio.h"
#include "iomanip"
#include "iostream"
#include "NSWRead/NSWChannelID.h"

nsw::PulseCalib::PulseCalib() {};

void nsw::PulseCalib::calibrate_pulser_DAC(std::map<std::string,
                                      std::tuple<float,float,float,float>> &vmm_constants,
                                      std::string pic_name,
                                      std::string feb_check){  
  bool check = false;
  int check_l, check_r, check_v;  
  if(feb_check.length()!=0){
    sscanf(feb_check.c_str(), "%i_%i_%i", &check_l, &check_r, &check_v);
    std::cout<<"Check ["<<check<<"] histograms for FEB at:\t["
             <<check_l<<"|"<<check_r<<"|"<<check_v<<"]"<<std::endl;
    check = true;
  }
  else{
    std::cout<<"Histograms will not be saved ["<<check<<"] \n"<<std::endl;
  }
  
    TFile *fvmmdac = new TFile("root_files/pulser_dac_tree.root");
    TTree *vmmdac=(TTree* )fvmmdac->Get("pdac_tree");
    std::cout<< vmmdac<<std::endl;
  
  //------------ tryna read w ttreereader----------
  
  std::cout<<"Loaded pusler dac root file..."<<std::endl;
  TTreeReader Vreader("pdac_tree",fvmmdac);
  TTreeReaderValue<int> vlay(Vreader,"layer");
  TTreeReaderValue<int> vrad(Vreader,"radius");
  TTreeReaderValue<int> Ivmm(Vreader,"vmm");
  TTreeReaderValue<float> mVsamp(Vreader,"sample_mV");
  TTreeReaderValue<int> DAC(Vreader,"DAC");
  
  std::map<std::string,TH1F*> t_vmmdac;
  std::map<std::string,TCanvas*> c_vmmdac; //use if only check-bool is true!
  
  int vmm_totent = vmmdac->GetEntries();
  /////////////////////////////////////////////////////////////////////////////
  int v_lay, v_rad, i_vmm, i_dac;
  float mvsample;
  //------------ filling vmmdac hists -------------------------------
  int safecount=0;
  while(Vreader.Next()){
    if(safecount>vmm_totent){break;}
    if(safecount%100==0){std::cout<<"vmm_ent-"<<safecount<<std::endl;}
    v_lay = *vlay;
    v_rad = *vrad;
    i_vmm = *Ivmm;
    mvsample = *mVsamp;
    i_dac = *DAC;
    std::string vhist  = Form("%i_%02i_%i_%04i",v_lay,v_rad,i_vmm,i_dac);
    if(t_vmmdac.count(vhist)==0){
      t_vmmdac[vhist] = new TH1F(vhist.c_str(),vhist.c_str(),1000,0,400);
    }
    float charge = (300.0*std::pow(10,-15)*(mvsample*std::pow(10,-3)))/(std::pow(10,-15)); //femto-coulombs
    if(v_lay==0 && v_rad==1 && i_vmm==5){std::cout<<"L0R1_vmm0 mvsample"<<mvsample<<" -> q="<<charge<<std::endl; sleep(0.1);}
    t_vmmdac[vhist]->Fill(charge); //Coulombs
    if(safecount<2000){std::cout<<"event ["<<safecount<<"] = |"<<vhist<<"|"<<i_dac<<" DAC - "<<mvsample<<" -> "<<charge<<"C"<<std::endl;}
    safecount++;
  }
  std::map<std::string,float> dactomV;
 
  if(check){ 
    std::cout<<"Histogram map size = "<< t_vmmdac.size()<<std::endl;
  }
  //------------ looking for points dac = n mV ---------------
  for(auto h:t_vmmdac){
    float vmean = t_vmmdac[h.first]->GetMean(1);
    float vrms = t_vmmdac[h.first]->GetRMS(1);
    int highbin = t_vmmdac[h.first]->GetMaximumBin();  
  
    TF1 *vfit = new TF1("vfit","gaus");
  
    if(vrms<20){
      dactomV[h.first] = vmean;
    }   
    else{
      t_vmmdac[h.first]->Fit("vfit","","Q");
      dactomV[h.first] = vfit->GetParameter(1);
    }
    if(check){
      std::cout<<"Fit for "<<h.first<<" - Peak = "<<vmean<<" rms = "<<vrms<<std::endl;  
    }
  
    if(check and h.first.substr(0,6)==feb_check){
      c_vmmdac[h.first] = new TCanvas(h.first.c_str(),h.first.c_str(),700,500);
      gStyle->SetOptStat(0);
      gStyle->SetOptTitle(0);
      c_vmmdac[h.first]->cd();
      h.second->Draw();
  ////////////////////////////////////////////////////////////////////////////
        TLatex lat;
        std::string dtype;
        lat.SetTextSize(0.03);
        lat.DrawLatexNDC(0.15,0.8,TString::Format("|Mean = %f|",vmean));
        //lat.DrawLatexNDC(0.15,0.75,TString::Format("|a=%f +- %f|",gr_slope, gr_slope_err));
        //lat.DrawLatexNDC(0.15,0.7,TString::Format("|b=%f +- %f|",gr_offset, gr_offset_err));
        //lat.DrawLatexNDC(0.15,0.65,TString::Format("|#chi^{2}_{red}=%f|",fit_chisq));
  /////////////////////////////////////////////////////////////////////////////
      c_vmmdac[h.first]->SaveAs(Form("plotter_output/pdacs/%s_%s.png",
                                                 h.first.c_str(),pic_name.c_str()));    
      c_vmmdac[h.first]->SaveAs(Form("plotter_output/pdacs/%s_%s.pdf",
                                                 h.first.c_str(),pic_name.c_str()));    

    }
    delete vfit;
    delete c_vmmdac[h.first];
  }//dac point loop ends
  if(check){
    std::cout<<"DAC to mV map size - "<<dactomV.size()<<std::endl;
  }
  fvmmdac->Close();
  //---------- drawing pulser dac points for vmms ---------------
  
  std::map<std::string, TGraphErrors*> g_dac;
  
  for(auto dp: dactomV){
    std::string vmmname = dp.first.substr(0,6);
    if(g_dac.count(vmmname)==0){
      g_dac[vmmname] = new TGraphErrors();
    }
    float daccharge = dp.second;
    int dacpoint = std::atoi(dp.first.substr(7,4).c_str());
    g_dac[vmmname]->SetPoint(g_dac[vmmname]->GetN(), dacpoint, daccharge);
    
    std::cout<<"graph - "<<vmmname<<"point : "<<dacpoint<<"[DAC] -> ("<<dp.second<<"[fC])/("<<std::setprecision(6)<<(double)(daccharge)<<"[fC]"<<std::endl;
    //sleep(1);
  }//key sorting loop ends
  
  dactomV.clear();//no need fro this map after these points are in the graph
  t_vmmdac.clear();

//  ////////////////////////////////////////////////////////////////////
  for(auto gr: g_dac){
    
    TCanvas *v_can  = new TCanvas;
    gStyle->SetOptStat(0);
    gStyle->SetOptTitle(0);
    std::string gr_pname;
    if(check and gr.first.substr(0,6)==feb_check){
      gr.second->GetXaxis()->SetTitle("Pulse amplitude,[DAC]");
      gr.second->GetYaxis()->SetTitle("Charge,[fC]");  
      gr_pname  = Form("%s_%s",gr.first.c_str(),pic_name.c_str());
      gr.second->SetTitle(gr_pname.c_str());
      gr.second->SetMarkerStyle(3);
      gr.second->SetMarkerSize(1);
      gr.second->SetMarkerColor(kRed);
    }
    int range = gr.second->GetN()-1;
    //std::cout<<"graph name - ["<<gr.first<<"] max_point = "<<gr.second->GetPointX(range)<<"| n_points"<<gr.second->GetN()<<"| first_point = "<<gr.second->GetPointX(0)<<std::endl;
  
    TF1 *vg = new TF1("vg","[0]*x +[1]",gr.second->GetPointX(0),gr.second->GetPointX(range));
    
    gr.second->Fit("vg","R");
    float const_a = vg->GetParameter(0);
    float const_a_err = vg->GetParError(0);
    float const_b = vg->GetParameter(1);
    float const_b_err = vg->GetParError(1);
     vmm_constants[gr.first] = std::make_tuple(const_a, const_a_err, const_b, const_b_err);  
    if(check and gr.first.substr(0,6)==feb_check){
      gr.second->Draw("AP");
      TLatex tl;
      tl.SetTextSize(0.03);
      tl.DrawLatexNDC(0.125,0.85,"Linear fit y=ax+b");
      tl.DrawLatexNDC(0.125,0.82,
         TString::Format("a = %.4f +- %.4f",const_a,const_a_err));
      tl.DrawLatexNDC(0.125,0.79,
         TString::Format("b = %.4f +- %.4f",const_b,const_b_err));

      v_can->SaveAs(Form("plotter_output/pdacs/%s_fullfit.png", gr_pname.c_str()));
      v_can->SaveAs(Form("plotter_output/pdacs/%s_fullfit.pdf", gr_pname.c_str()));
      std::cout<<"graph name - ["<<gr.first<<"]"<<"const_a["<<const_a<<"] const_b["<<const_b<<"]"<<std::endl;  
    }
    delete v_can;

  }

  g_dac.clear();
  std::cout<<"obtained constants:"<<vmm_constants.size()<<std::endl;
  std::cout<<"=============================================================="<<std::endl;
  std::cout<<"================== VMM PULSER DAC FITTED ====================="<<std::endl;
  std::cout<<"=============================================================="<<std::endl;
  sleep(1);

}
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
std::map<std::string,TH1F*> nsw::PulseCalib::read_root_file(std::string this_root_file,
                                                            std::string pic_name,
                                                            std::map<std::string,TH1F*> &t_hpdo,
                                                            std::vector<int> &dac_del,
                                                            std::string feb_check,
                                                            int &totent,
                                                            bool pdo,
                                                            bool tdo){  
////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
  TFile *f = new TFile(this_root_file.c_str());//<--this hsould be input argument!
  TTree *same_data_tree = (TTree*)f->Get("nsw");
  std::cout<<same_data_tree<<std::endl;
  totent = same_data_tree->GetEntries();

  ////////////////////////////////////////////////////////////////////////////////////////////
  // this loop is for filling pdo per dac/delay values
  TTreeReader TRead("nsw",f);
  TTreeReaderArray<unsigned int> layer(TRead,"layer");
  TTreeReaderArray<unsigned int> radius(TRead,"radius");
  TTreeReaderArray<unsigned int> vmmid(TRead,"vmmid");
  TTreeReaderArray<unsigned int> chan(TRead,"channel");
  TTreeReaderArray<unsigned int> linkID(TRead,"linkId");
  TTreeReaderValue<int> trgCalibKey(TRead,"triggerCalibrationKey");
  TTreeReaderArray<unsigned int> PDO(TRead,"pdo");
  TTreeReaderArray<unsigned int> TDO(TRead,"tdo");
  TTreeReaderArray<unsigned int> relbcid(TRead,"relbcid");
  TTreeReaderArray<unsigned int> linkId(TRead,"linkId");
  TTreeReaderArray<unsigned int> L1ID(TRead,"level1Id");
  /////////////////////////////////////////////////////////////////////////////////////////
  int Lay,Rad,Vmm,Chan,Pdo,Tdo,tCK,rbcid,linkid;
  int l1id;

  int hist_max=0;
  if(pdo){hist_max=1030;}
  if(tdo){hist_max=260;}
  unsigned int countEr = 0;

  int nr_keys = 0;
  int n_events = 0;

  std::map<std::string,TH2D*> chan_peak_map;
  TH1D* this_chan = new TH1D("this_chan","this_chan",1024,0,1024);
  TH1D* h_l1id = new TH1D("h_l1id","h_l1id",1000,0,1000);

  while(TRead.Next()){
    countEr++;
    if(countEr>totent){break;}
    if(countEr % 1000 == 0){
       std::cout<<"\033[1;34m"
                <<"\r"<<countEr<<"/"<<totent
                <<"\033[0m"<<std::flush;
      //printf("\r%i event",countEr);
    }
    for(int i_strip=0; i_strip<radius.GetSize(); i_strip++){
      Lay = layer.At(i_strip);
      Rad = radius.At(i_strip);
      Vmm = vmmid.At(i_strip);
      Chan = chan.At(i_strip);
      rbcid = relbcid.At(i_strip);
      //tCK = *trgCalibKey;
      linkid = linkId.At(i_strip);
      std::string chk1 = std::to_string(linkid);
      if(chk1.length()==10){
         tCK = (*trgCalibKey) >> 12;
      }else{
         tCK = *trgCalibKey;
      }
      if(tCK<0){continue;}
      dac_del.push_back(tCK);
      l1id = L1ID.At(i_strip);

      h_l1id->Fill(l1id);

      if(pdo){Pdo = PDO.At(i_strip);}
      if(tdo){Tdo = TDO.At(i_strip);}
///////////////////////////////////////////////////////////////////////////
//      std::string h_peak = Form("%i_%02i_%i_%02i_%i", Lay, Rad, Vmm, Chan, nr_keys);
//
//      if(countEr%1000==0 && i_strip==1){
//         std::cout<<"\n"<<l1id<<std::endl;
//         //sleep(1);
//      }
//      if(l1id==0 && i_strip==1){
//         std::cout<<"Found L1ID reset at << "
//                  <<L1ID.At(i_strip-1)<<" >> , N keys=["<<nr_keys<<"]"<<std::endl;
//         nr_keys++;
//         //sleep(0.5);
//      }
//      if(Lay==0 && Rad==1 && Vmm==0 && Chan==7){
//         if(nr_keys<25){
//            if(chan_peak_map.find(h_peak)==chan_peak_map.end()){
//               chan_peak_map[h_peak] = new TH2D(h_peak.c_str(),h_peak.c_str(),1024,0,1024,70,100,800);
//            }
//            chan_peak_map[h_peak]->Fill(PDO.At(i_strip),tCK);
//         }
//         this_chan->Fill(PDO.At(i_strip));
//      }
//
///////////////////////////////////////////////////////////////////////////
//      if(tCK<0){continue;}
//      if(rbcid!=the_relbcid){continue;}
//      if(rbcid>the_relbcid+1 or rbcid<the_relbcid-1){continue;}
      std::string h_name;
      if(std::to_string(linkid).length()==6){
         h_name = Form("%06i_%i_%02i_%i_%02i_%04i", linkid, Lay, Rad, Vmm, Chan, tCK);
      }else{
         h_name = Form("%09i_%i_%02i_%i_%02i_%04i", linkid, Lay, Rad, Vmm, Chan, tCK);
      }
////////////////////////////////////////////////////////////////////////////////////////////
//      bool found_vmm_relbcid = the_relbcid.find(h_name.substr(0,13))==the_relbcid.end();
//      bool bcid_is_right = rbcid == the_relbcid[h_name.substr(0,13)];
//      if(found_vmm_relbcid and !bcid_is_right){continue;}
////////////////////////////////////////////////////////////////////////////////////////////
      if(t_hpdo.find(h_name)==t_hpdo.end()){
            t_hpdo[h_name] = new TH1F(h_name.c_str(),h_name.c_str(),hist_max,0,hist_max);
      }
      if(pdo){t_hpdo[h_name]->Fill(Pdo);}    
      if(tdo){t_hpdo[h_name]->Fill(Tdo);}    
    }
    n_events++;

  }

//  for(auto &h:chan_peak_map){
//      TCanvas *c = new TCanvas();
//      h.second->Draw();   
//      c->SaveAs(Form("%s-check.png",h.first.c_str()));
//      delete c;
//  }
//
//  TCanvas *c1 = new TCanvas();
//  this_chan->Draw();
//  c1->SaveAs("L0R1V0C10-allPeaks.png");
//  delete c1;
//  TCanvas *c2 = new TCanvas();
//  h_l1id->Draw();
//  c2->SaveAs("L1IDs.png");
//  delete c2;
// 
//  //exit(0); //temporarily

  return t_hpdo;
}

void nsw::PulseCalib::find_peaks(std::map<std::string,TH1F*> &hist,
                    std::map<std::string,TGraphErrors*> &graph,
                    std::string feb_check,
                    std::map<std::string, std::tuple<float,float,float,float>> &vmm_constants,
                    std::string pic_name,
                    float &mean_hits_per_key,
                    bool tdo,
                    bool pdo)
{

bool check = false;
if(feb_check.length()!=0){
  check=true;
  std::cout<<"Check ["<<check<<"] | Histograms for FEB at:\t["<<feb_check<<"]"<<std::endl;
}else{
  std::cout<<"Check ["<<check<<"] | Histograms will not be saved"<<std::endl;
}

for(auto &h:hist){

    bool longID = false;
    bool debug = false;

    std::cout<<"[1]:"<<h.first.substr(7,6)
             <<"\n[2]:"<<h.first.substr(11,6)<<std::endl;
   
    if(h.first.substr(7,6)==feb_check ||
       h.first.substr(11,6)==feb_check){
      debug = true;
    }

    if(h.first.length()>21){
       longID = true;
    }

    std::cout<<"[longID] :"<<longID
             <<"[check]  :"<<check
             <<"[debug]  :"<<debug<<std::endl;

    if(check && debug){
      std::cout<<"################# channel "<<h.first<<" ####################"<<std::endl;
    }

    int calibKey;
    if(longID){
       calibKey = std::atoi(h.first.substr(21,4).c_str());
    }else{
       calibKey = std::atoi(h.first.substr(17,4).c_str());
    }
    if(check && debug){
      std::cout<<"TRGcalibKey: "<<calibKey<<std::endl;
    }
    float h_ent = h.second->GetEntries();
    
    float stat = std::round(h_ent/mean_hits_per_key); //checking if overall nr of eventsis acceprtable

    if(check && debug){
      std::cout<<"|Relative statistics = "<<stat<<"| hist entries "
               <<h_ent<<"| average ent "<<mean_hits_per_key
               <<"| result "<<h_ent/mean_hits_per_key<<"|"<<std::endl;  
    }
    if(stat<=0.1){
      std::cout<<"\n------- Skipping "<<h.first
               <<"-> nr. of entries less than 10% -------"<<std::endl;
      //continue;
    }
    std::string thisFeb;
    if(!longID){
       thisFeb = h.first.substr(0,16);
    }else{
       thisFeb = h.first.substr(0,20);
    }
    if(check){
      std::cout<<"check: febname:"<<thisFeb<<std::endl;
    }
    int firstBin, lastBin, h_max_bin;
    float peak, h_mean, peakerr, h_meanerr;
    h_max_bin = hist[h.first]->GetMaximumBin();
    int maxbin_size = hist[h.first]->GetBinContent(h_max_bin);
    float h_rms = hist[h.first]->GetRMS(1);

    int bin_thresh = maxbin_size/20;
    if(maxbin_size==0 and h_mean!=0){ bin_thresh=50;}

    firstBin = hist[h.first]->FindFirstBinAbove(bin_thresh,1);
    lastBin = hist[h.first]->FindLastBinAbove(bin_thresh,1);
    h_mean = hist[h.first]->GetMean(1);
    float peak_dev = std::fabs(h_mean - h_max_bin);
    float peak_width = std::fabs(lastBin - firstBin);

    TF1 *gf = new TF1("gf","gaus");
    int fitStat = -1;
    bool peakIsMean = false;

    std::string scenario,scenario2;
//////////////// scenarios for pdo hist fitting ////////////////////////////////////
    if(pdo){//pdo fits
      if(stat>=0.2){
        if(std::floor(h_mean)==h_mean and h_rms==0){
          peak = h_mean;
          peakerr = hist[h.first]->GetMeanError();
          //peakerr = hist[h.first]->GetBinError(h_mean);//original
          peakIsMean=true;
          if(check){scenario = "nofit-mean";}
        }
        else if(maxbin_size >= h_ent/2.5 and h_rms <= 5.0){
          fitStat = hist[h.first]->Fit("gf","","Q",h_max_bin-15,h_max_bin+15); //if setting parameters enter "B" option!!!
          if(check){scenario = "fit-maxbin-pm10";}
        }
        else if(maxbin_size < h_ent/2.5 and h_rms > 10.0){
          fitStat = hist[h.first]->Fit("gf","","Q",firstBin-10,lastBin+10);
          if(check){scenario = "fit-first-last-bin";}
        }  
        else{//last resort
          gf->SetParameter(1,h_mean);
          fitStat = hist[h.first]->Fit("gf","B","Q",h_mean-20, h_mean+20);
          if(check){
            std::cout<<"\n[WARNINIG] Couldn`t properly select fit scenario for this histogram ["
                     <<h.first<<"], using full range ================= "<<std::endl;
            scenario = "fit-fullrange";
          }
        }
      }
      else{
        peak = h_mean;
        peakerr = hist[h.first]->GetMeanError();
        //peakerr = hist[h.first]->GetBinError(h_mean);
        peakIsMean=true;
      }
    }//pdo fit flag
//////////////////////// in case we have tdo hists /////////////////////////////
    if(tdo){
      if(stat >= 0.2){//ok stat fits
        if((std::floor(h_mean)==h_mean and h_rms==0) or maxbin_size>=h_ent/2.0 ){
          peak = h_mean;
          peakerr = hist[h.first]->GetMeanError();
          //peakerr = hist[h.first]->GetBinError(h_mean);
          peakIsMean=true;
          if(check){scenario = "nofit-mean";}
        }
        else if(peak_dev<= 1.0 and h_rms<=10){
          gf->SetParameter(1,h_max_bin);
          fitStat = hist[h.first]->Fit("gf","B","Q",h_max_bin-5,h_max_bin+5);
          if(check){scenario = "fit-maxbin";}
        }
        else if(peak_width > 50.0){
          int local_mean = 0;
          float nEntExcluded = 0;
          for(int b=0; b<hist[h.first]->GetNbinsX(); b++){
              if(b<h_max_bin-19 || b>h_max_bin+21){
                 nEntExcluded += hist[h.first]->GetBinContent(b);
              }else{continue;}        
          }

          float local_entries = (float)h_ent - nEntExcluded;

          float num = 0;
          float n_local = 0;
          for(int i=h_max_bin-19; i<h_max_bin+21; i++){ 
              float content = hist[h.first]->GetBinContent(i);
              float weight = content/local_entries;
              num += weight*i; 
              n_local += content;
          }
          local_mean = num/n_local;
          peak = local_mean;
          //peakerr = 
          if(check){scenario = "take-highest";}
        }
//        else if(h_max_bin>h_mean and std::floor(h_max_bin - h_mean)>=1.5*std::floor(lastBin - h_max_bin)){
//          fitStat = hist[h.first]->Fit("gf","","Q",h_mean-5, lastBin+10);
//          if(check){scenario = "fit-hipeak-on-right";}
//        }
//        else if(h_max_bin<h_mean and std::floor(h_mean - h_max_bin)>=1.5*std::floor(h_max_bin - firstBin)){
//          fitStat = hist[h.first]->Fit("gf","","Q",firstBin-10, h_mean+5);
//          if(check){scenario = "fit-hipeak-on-left";}
//        }// if mean and hi peak are same bin (peak_dev = 0) - graph gets to this point
        else{
          gf->SetParameter(1,h_max_bin);
          gf->SetParameter(0,0.5);
          fitStat = hist[h.first]->Fit("gf","B","Q",0, 260);
          if(check){scenario = "fit-fullrng-fixpeak";}
        }
      }//ok fit flag ends
      else{//low stat fits
          peak = h_mean;
          peakerr = hist[h.first]->GetMeanError();
          //peakerr = hist[h.first]->GetBinError(h_mean);
          peakIsMean=true;
      }
    }
    if(!peakIsMean){
      peak = gf->GetParameter(1);
      peakerr = gf->GetParError(1);
    }
    float chi = gf->GetChisquare();
    float ndf = gf->GetNDF();
    float chi_red = chi/ndf;//reduced chi square 
  
    ///////////// if fit is crap take mean /////////////////////////////////
    if(pdo && !peakIsMean){
      if(peak <= 150. ||
        peak >=1030. ||
        chi/ndf>=250.){  
          peak = h_mean;
          peakerr = hist[h.first]->GetMeanError();
          //peakerr = hist[h.first]->GetBinError(h_mean);
          if(check){scenario2 = "fit-failed-take-mean";}
      }
    }
    if(tdo && !peakIsMean && scenario!="take-highest"){
      if(peak <= 0. ||
        peak >=256. ||
        chi/ndf>=100.0){  
          peak = h_mean;
          //peakerr = hist[h.first]->GetBinError(h_mean);
          peakerr = hist[h.first]->GetMeanError();
          if(check){scenario2 = "fit-failed-take-mean";}
      }
    }
    //////////////////////////////////////////////////////////////////

    std::string channelName;
    if(!longID){
       channelName = h.first.substr(0,16);
    }else{
       channelName = h.first.substr(0,20);
    }
    if(graph.count(channelName)==0){ graph[channelName]=new TGraphErrors();}
    std::cout<<"Filling histo:["<<channelName<<"]"<<std::endl;

    float trg_par;
    std::string par_key;
    if(!longID){
       trg_par = std::atoi(h.first.substr(17,4).c_str());
       par_key = channelName.substr(7,6);
    }else{
       trg_par = std::atoi(h.first.substr(21,4).c_str());
       par_key = channelName.substr(11,6);
    }
  
    std::cout<<"Looking for VMM - ["<<par_key<<"]"<<std::endl;
    float conv_trg_par = 0;
    float conv_trg_par_err = 0;
    if(pdo){
      float ca = std::get<0>(vmm_constants[par_key]);
      float caerr = std::get<1>(vmm_constants[par_key]);
      float cb = std::get<2>(vmm_constants[par_key]);
      float cberr = std::get<3>(vmm_constants[par_key]);

     // temporarily commenting out an using one function 
      conv_trg_par = trg_par*ca + cb;    
      conv_trg_par_err = std::sqrt(std::pow(caerr,2) + std::pow(cberr,2));        
      
      //conv_trg_par = trg_par*0.223254 + 7.08677;    
      //conv_trg_par_err = std::sqrt(std::pow(0.000349806,2) + std::pow(0.174611,2));        

      if(check && debug){
        std::cout<<"trgCK("<<trg_par
                 <<") x slope("<<ca<<"+-"<<caerr
                 <<") + offset("<<cb<<"+-"<<cberr
                 <<") = "<<conv_trg_par<<std::endl;
      }
    }
    if(tdo){
        conv_trg_par = 37.5 - trg_par*3.125; //for timing use 25 ns - delay 
                                             //how the fuck to get errors here...?
    }
    if(trg_par<0){continue;}  


    int npoints = graph[channelName]->GetN();
    // after using GetN here tgraph has +1 point
    graph[channelName]->SetPoint(npoints, conv_trg_par, peak);
    // so this guy sets the errors for the empty point, thus GetN()-1
    graph[channelName]->SetPointError(npoints, conv_trg_par_err, peakerr);
    ///////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////
    
    if(check && debug){
      TCanvas *tcan = new TCanvas(h.first.c_str(),h.first.c_str(),700,500);
      gStyle->SetOptStat(0);
      gStyle->SetOptTitle(0);
      tcan->cd();
      h.second->GetXaxis()->SetTitle("Counts,[ADC]"); 
      h.second->GetYaxis()->SetTitle("Entries,[N]"); 
      h.second->Draw();
    //----------------------------------------------------------------------
      std::string data;
      if(pdo){data = "PDO";}
      if(tdo){data = "TDO";}
      std::cout<<"#Points="<<npoints<<std::endl;
      //float xposit = 0.15;
      float xposit = 0.6;
      float yposit = 0.85;
      TLatex tl;
      tl.SetTextSize(0.03);
      tl.DrawLatexNDC(xposit,yposit,TString::Format("|type %.3s|",data.c_str()));
      //tl.DrawLatexNDx(0.15,yposit0.81,
      //   TString::Foxmat("#ypositchi^{2}_{red} = %.2f",chi/ndf));
      tl.DrawLatexNDC(xposit,yposit-0.04,TString::Format("fitStatus = %i",fitStat));
      tl.DrawLatexNDC(xposit,yposit-0.08,TString::Format("entries = %.2f",h_ent));
      tl.DrawLatexNDC(xposit,yposit-0.12,
         TString::Format("peak = %.2f, p-width = %.2f",peak,peak_width));
      tl.DrawLatexNDC(xposit,yposit-0.16,
         TString::Format("p-dev = %.2f, statistics = %.2f",peak_dev,stat));
      tl.DrawLatexNDC(xposit,yposit-0.20,
         TString::Format("maxbin =%i, size = %i",h_max_bin,maxbin_size));
      tl.DrawLatexNDC(xposit,yposit-0.24,
         TString::Format("scen = %s",scenario.c_str()));
//      tl.DrawLatexNDC(xposit,yposit-0.28,
//         TString::Format("scen = %s",scenario2.c_str()));


    //----------------------------------------------------------------------------
      if(pdo){
         tcan->SaveAs(Form("plotter_output/pdo/%s_%s.png",
                                               h.first.c_str(),
                                            pic_name.c_str()));
         tcan->SaveAs(Form("plotter_output/pdo/%s_%s.pdf",
                                               h.first.c_str(),
                                            pic_name.c_str()));

      }
      if(tdo){
         tcan->SaveAs(Form("plotter_output/tdo/%s_%s.png",
                                               h.first.c_str(),
                                            pic_name.c_str()));
         tcan->SaveAs(Form("plotter_output/tdo/%s_%s.pdf",
                                               h.first.c_str(),
                                            pic_name.c_str()));
      }
    }
    delete gf;
  }//global "for" loop ends

  //exit(0);

}//func ends
//#############################################################################

void nsw::PulseCalib::fit_calibKey_graphs(std::map<std::string, TGraphErrors*> &t_gr,
                              std::string feb_check,
                              std::string rootoutfilename,
                              std::string pic_name,
                              bool pdo,
                              bool tdo)
{

bool check = false;

if(feb_check.length()!=0){
  check=true;
  std::cout<<"Check ["<<check<<"] | Histograms for FEB at:\t["<<feb_check<<"]"<<std::endl;
}else{
  std::cout<<"Check ["<<check<<"] | Histograms will not be saved"<<std::endl;
}


  std::string treename;
  if(tdo){treename="TDO_calib";}
  if(pdo){treename="PDO_calib";}

  TFile *out_const = new TFile(Form("%s_constants.root",rootoutfilename.c_str()),"RECREATE");
  TTree *contree = new TTree(treename.c_str(),"Tree of fit constants per channel");
  
  ////////////////////////////////////////////
  int ichannel, ilayer, ivmmid, iradius, chan_sat, chan_ent, ilink, isect, iwheel, itech;//, lost;
  double gr_slope, gr_offset, gr_slope_err, gr_offset_err, fit_chisq, probab;
  
//  ///////////// BB5 branch name scheme////////////////////////////////
  contree->Branch("LINKID", &ilink,"LINKID/I");
  contree->Branch("WHEEL", &iwheel,"WHEEL/I");
  contree->Branch("TECH", &itech,"TECH/I");
  contree->Branch("SEC", &isect,"SEC/I");
  contree->Branch("LAYER", &ilayer,"LAYER/I");
  contree->Branch("RADIUS",&iradius,"RADIUS/I");
  contree->Branch("VMM",&ivmmid,"VMM/I");
  contree->Branch("CH",&ichannel,"CH/I");
  contree->Branch("S",&gr_slope,"S/D");
  contree->Branch("SE",&gr_slope_err,"SE/D");
  contree->Branch("C",&gr_offset,"C/D");
  contree->Branch("CE",&gr_offset_err,"CE/D");
  contree->Branch("CH_SAT",&chan_sat,"CH_SAT/F");
  contree->Branch("CH_ent",&chan_ent,"CH_ent/I");
  contree->Branch("chi2",&fit_chisq,"chi2/D");
  contree->Branch("prob",&probab,"prob/D");
  //contree->Branch("lost_ch",&lost,"lost_ch/I");
  
  //////////////////////////////////////////////
  int grXmax, grYmax;
  std::string xLabel, yLabel;
  
  if(pdo){
    grXmax = 160.0;
    grYmax = 1200.0;
    xLabel = "Charge,[fC]";
    yLabel = "PDO,[ADC]";
  }
  if(tdo){
    grXmax = 30.0;
    grYmax = 260.0; //256 counts for timedetector
    xLabel = "Time,[ns]";
    yLabel = "TDO,[ADC]";
  }

  for(auto gr: t_gr){

   bool longID = false;

   if(gr.first.length()>16){
      longID = true;
   }

   float MIN,MAX;

   bool debug = false;
   if( gr.first.substr(7,6)==feb_check ||
       gr.first.substr(11,6)==feb_check  ){
     debug = true;
   }
   
    int totpoints = gr.second->GetN();
    
    chan_ent = totpoints;
    if(check && debug){
      std::cout<<"graph:"<<gr.first<<" has __"<<totpoints<<"__ points"<<std::endl;
    }
  ////////////////////////////////////  
  // Throwing away channels with less than 
  // two entries in the graph
  ////////////////////////////////////  
  if(totpoints<2){
      if(check && debug){
         std::cout<<"Zannendeshta senpai, "
                  <<"ichi pointo ikiteru kara histogramu printo jaanai!"
                  <<"[SKIPPED "<<gr.first<<" ]"<<std::endl;
      }

      int thislink;
      if(!longID){
         thislink = atoi(gr.first.substr(0,6).c_str());
      }else{
         thislink = atoi(gr.first.substr(0,9).c_str());
      }
      NSWChannelID declink(thislink);
      int dec_sector = declink.GetSector();
      int tech = declink.GetTechnology();
      int this_tech;
      std::string which_tech;
      if(tech==0){
        which_tech = "sTGC";
        this_tech = 0;
      }
      else{
        which_tech = "MM";
        this_tech = 1;
      }
      if(dec_sector>0){
        iwheel = 1;
      }
      else{
        iwheel = -1;
      }
      itech = this_tech;
      isect = std::abs(dec_sector);
      std::cout<<"Link:"<<thislink
               <<" side:"<<iwheel
               <<" sector:"<<isect
               <<" Technology:"<<tech<<std::endl;

      if(!longID){
         ilink  = atoi(gr.first.substr(0,6).c_str());
         ilayer = atoi(gr.first.substr(7,1).c_str());
         iradius = atoi(gr.first.substr(9,2).c_str());
         ivmmid = atoi(gr.first.substr(12,1).c_str());
         ichannel = atoi(gr.first.substr(14,2).c_str());
      }else{
         ilink  = atoi(gr.first.substr(0,10).c_str());
         ilayer = atoi(gr.first.substr(11,1).c_str());
         iradius = atoi(gr.first.substr(13,2).c_str());
         ivmmid = atoi(gr.first.substr(16,1).c_str());
         ichannel = atoi(gr.first.substr(18,2).c_str());
      }
      gr_slope = -9999;
      gr_slope_err = -9999;
      gr_offset = -9999;
      gr_offset_err = -9999;
      chan_sat = -9999;
      chan_ent = -9999;
      fit_chisq = -9999;
      probab = -9999;

      contree->Fill();// fill crap
       
      continue; //skip to next channel
  }
  //////////////////////////////////////
  //////////////////////////////////////
  //////////////////////////////////////
  TString feb_chan = gr.first;
  TString gr_plotname;
  if(check and debug){     
    gr.second->GetYaxis()->SetTitle(yLabel.c_str());  
    gr.second->GetXaxis()->SetTitle(xLabel.c_str());
    gr_plotname  = Form("%s_%s",gr.first.c_str(),pic_name.c_str());
    gr.second->SetTitle(gr_plotname);
    
    gr.second->SetMarkerStyle(1);
    gr.second->SetMarkerSize(3);
    gr.second->SetMarkerColor(kBlue);
    
  }
  int fitmax, fitmin;
  int cnt=0;
  float maxpar_point;
  int npoints = gr.second->GetN(); 

  //chan_ent=0;
  int nmoved = 0;
  if(pdo){fitmin = 0;
    float check_first = gr.second->GetPointY(0);
    if(check_first>=950.00){
      std::cout<<"WARNING - first point starts close to saturation!"<<std::endl;
      continue;
    }
  }
  if(tdo){fitmin = npoints;}
/////// Checking PDO onwards //////////////////////////////////////////////////
  if(pdo){
    int midpoint = std::ceil(npoints/2)+1;
    //int midpoint = std::floor(npoints/2);
    float zero_x = gr.second->GetPointX(0);  
    float zero_y = gr.second->GetPointY(0);
    float mid_y = gr.second->GetPointY(npoints-1);
    float mid_x = gr.second->GetPointX(npoints-1);
    float diff_mid = (mid_y-zero_y)/(mid_x-zero_x);
 //   if(check and gr.first.substr(7,6)==feb_check){
    std::cout<<"\n=====\nnpoints="<<npoints<<"\tmidpoint="<<midpoint<<"\tdy/dx="<<diff_mid<<"\n=====\n"<<std::endl;
 //   }

    int cntr = 0; 
    for(int n=npoints-1; n>=0; n--){
        
       //if(n>=npoints-1){
       //   fitmax = npoints-1;
       //   break;
       //}

       float raw_slope = 
             (gr.second->GetPointY(n) - gr.second->GetPointY(n-1))
             /
             (gr.second->GetPointX(n) - gr.second->GetPointX(n-1));
             
       if(cntr==0 && raw_slope>5 && raw_slope<12){
         fitmax=npoints-1;
         break;
       }
       if(cntr!=0 && raw_slope>5 && raw_slope<12){
          fitmax=n;
          break;
       }
      // ////////////////////////////////////////// 
      // if(cntr==0 && gr.second->GetPointY(n)<=980){
      //   fitmax=npoints-1;
      //   break;
      // }
      // if(cntr!=0 && gr.second->GetPointY(n)<=980){
      //    fitmax=n;
      //    break;
      // }
       cntr++;
    }
    MIN = gr.second->GetPointY(0);
   // for(int i=0; i<gr.second->GetN(); i++){
   //   if(i==npoints-1 && gr.second->GetPointY(i)<=990){
   //     fitmax=i;
   //     break;
   //   }
   //   //chan_ent++;
   //   float this_y = gr.second->GetPointY(i);
   //   float next_y = gr.second->GetPointY(i+1);
   //   float this_x = gr.second->GetPointX(i);
   //   float next_x = gr.second->GetPointX(i+1);
   //   float differ_i = (next_y-this_y)/(next_x-this_x);
   //   float breakpoint = ((float)(cnt+1))/((float)(npoints));
   //   //float differ_zero;
   //   std::cout<<"INFO: dy_"<<i<<"/dx_"<<i<<"="<<differ_i<<std::endl;
   //   if(differ_i<=2.0 && breakpoint>0.5){
   //      fitmax=i;
   //      break;
   //   }
   //   if(differ_i<=2.0 && breakpoint<=0.5){
   //     continue;
   //   }
   //   std::cout<<"Fitmax="<<fitmax<<std::endl;
   //  // if(differ_i=<1.0 && breakpoint>0.5){
   //  //    fitmax=i;
   //  // }
   //  

   //  // if(maxpar_point >= 1000.0){
   //  //   fitmax=i-1;
   //  //   if(gr.second->GetN()==2){fitmax=i;}
   //  //   if(check and gr.first.substr(7,6)==feb_check){
   //  //     std::cout<<"\nfitmin="<<fitmin<<",fitmax="<<fitmax<<",fitmax(PDO)="<<gr.second->GetPointY(fitmax)<<std::endl;
   //  //   }
   //  //   break;
   //  // }
   // }//pdo loop
  }
 
/////// Checking TDO onwards //////////////////////////////////////////////////
  if(tdo){

    for(int i=0; i<gr.second->GetN(); i++){
      float this_point = gr.second->GetPointY(i);
      float next_point = gr.second->GetPointY(i+1);

///////////////////////////////////////////////////////////////////////////////
    //int flip_pos = 0;

    //for(int i=0; i<gr.second->GetN(); i++){

    //  float this_point = gr.second->GetPointY(i);
    //  float next_point = gr.second->GetPointY(i+1);
    //  float x0 = gr.second->GetPointY(i);
    //  float x1 = gr.second->GetPointY(i+1);
 
    //  float temp_slope = (this_point - next_point)/(x0 -x1);  

    //  if(temp_slope < 0){
    //     flip_pos = i+1;
    //     break;
    //  }
    //}
    //fitmin = flip_pos-1;
  
    //for(int p = flip_pos; p<gr.second->GetN(); p++){
    //      float x = gr.second->GetPointX(p)+25;
    //      float sx = gr.second->GetErrorX(p);
    //      float y = gr.second->GetPointY(p);
    //      float sy = gr.second->GetErrorY(p);
    //      gr.second->RemovePoint(p);
    //      gr.second->SetPoint(p,x,y);
    //      gr.second->SetPointError(p,sx,sy);
    //}
    //fitmax = gr.second->GetN();
      

//////// currently working ////////////////////////////////////////////////////

      float diff = this_point - next_point;
      float jumppoint = ((float)(cnt+1))/((float)(npoints));
//      std::cout<<"ADYN - thispoint="<<this_point<<"|nextpoint="<<next_point<<std::endl; 
//      std::cout<<"DUA - DIFF="<<diff<<",["<<(float)(cnt+1)<<":"<<(float)(npoints)<<"] =>> jumppoint="<<jumppoint<<std::endl; 
      if(diff<0 && jumppoint<0.5){
        fitmin=npoints-1;
        fitmax=i+1;
//        std::cout<<"DIFF<0 -> Jump["<<jumppoint<<"] FIT-[min="<<fitmin<<"|max="<<fitmax<<"]"<<std::endl;
//        sleep(1); 
        break;
      }
      if(diff<0 && jumppoint>0.5){
        fitmax=0;
        fitmin=i;
//        std::cout<<"DIFF<0 -> Jump["<<jumppoint<<"] FIT-[min="<<fitmin<<"|max="<<fitmax<<"]"<<std::endl; 
//        sleep(1);
        break;
      }
      if(diff<0 && jumppoint==0.5){
        fitmax=i+1;
        fitmin=npoints-1;
//        std::cout<<"DIFF<0 -> Jump["<<jumppoint<<"] FIT-[min="<<fitmin<<"|max="<<fitmax<<"]"<<std::endl; 
//        sleep(1);
        break;
      }
///////////////////////////////////////////////////////////////////////////
      if(diff<0 && jumppoint==0.5 && npoints!=2){
        fitmax=i+1;
        fitmin=npoints-1;
//        std::cout<<"DIFF<0 -> Jump["<<jumppoint<<"] FIT-[min="<<fitmin<<"|max="<<fitmax<<"]"<<std::endl; 
//        sleep(1);
        break;
      }
      if(diff<0 && jumppoint==0.5 && npoints==2){
        float newXpoint = (gr.second->GetPointX(i)) - 12.5;
        float newXpointErr = gr.second->GetErrorX(i);
        float newYpoint = gr.second->GetPointY(i);
        gr.second->SetPoint(i, newXpoint, newYpoint);
        fitmax=i+1;
        fitmin=npoints-1;
//        std::cout<<"DIFF<0 -> Jump["<<jumppoint<<"] FIT-[min="<<fitmin<<"|max="<<fitmax<<"]"<<std::endl; 
//        sleep(1);
        break;
      }
///////////////////////////////////////////////////////////////////////////
      if(jumppoint==1 && diff>0){
        fitmax=0;
        fitmin=npoints-1;
//        std::cout<<"DIFF<0 -> Jump["<<jumppoint<<"] FIT-[min="<<fitmin<<"|max="<<fitmax<<"]"<<std::endl; 
//        sleep(1);
        break;
      }
      if(diff>0){cnt++;continue;}
    }//fnologr loop ends

////////////////////////////////////////////////////////////////////////      
  }//if tdo ends
//  std::cout<<"HULI? - cnt="<<cnt<<std::endl; 

  if(check and debug){
    std::cout<<"\n##############\nlowlim Y="<<gr.second->GetPointY(fitmin)<<"\thilim Y="<<gr.second->GetPointY(fitmax)<<"\n###############\n"<<std::endl;
    std::cout<<"\n##############\nlowlim X="<<gr.second->GetPointX(fitmin)<<"\thilim X="<<gr.second->GetPointX(fitmax)<<"\n###############\n"<<std::endl;
  }
 
    float lowlim, hilim;
    lowlim = gr.second->GetPointX(fitmin); 
    hilim = gr.second->GetPointX(fitmax);
 
    TF1 *joba = new TF1("joba","[0]*x + [1]", lowlim, hilim);
    joba->SetParLimits(0,0,20);
        
    //gr.second->Fit("joba","R");
    gr.second->Fit("joba","WQRN");
    gr.second->Fit("joba","RM+");
  
    gr_slope = joba->GetParameter(0);
    gr_slope_err = joba->GetParError(0);  
    gr_offset = joba->GetParameter(1);
    gr_offset_err = joba->GetParError(1);
     
    fit_chisq = joba->GetChisquare()/joba->GetNDF();
    probab = joba->GetProb();

    if(gr_slope<2 && fit_chisq >= 100){
      std::cout<<"BAD FIT:"
               <<"\na="<<gr_slope
               <<"\nb="<<gr_offset
               <<std::endl;
      std::cout<<"---------------------------------------------------"<<std::endl;
      for(int p=0; p<gr.second->GetN(); p++){
        std::cout<<"p#-"<<p<<"\t|x="<<gr.second->GetPointX(p)
                 <<"\t|y="<<gr.second->GetPointY(p)<<"|"<<std::endl;
      }
      std::cout<<"fitpoints ="<<fitmin<<":"<<fitmax
               <<", npoints="<<gr.second->GetN()<<std::endl;
      std::cout<<"---------------------------------------------------"<<std::endl;
      //sleep(2); 
      /////////////////////////////////////////////////////////////////
      if(pdo){
         std::cout<<"RE-FIT:"<<std::endl;
         float new_fitmax;
         if(fitmax<3){
            new_fitmax = fitmax;
         }else{
            new_fitmax = fitmax-1;
         }
         //TF1 *joba2 = new TF1("joba2","[0]*x + [1]", lowlim, gr.second->GetPointX(fitmax-1));
         TF1 *joba2 = new TF1("joba2","[0]*x + [1]", lowlim, gr.second->GetPointX(new_fitmax));
         if(fitmax<3){
            joba2->SetParLimits(0,2,12);
         }else{
            joba2->SetParLimits(0,5,12);
         }
         joba2->SetLineColor(kBlue);
         //gr.second->Fit("joba2","R");
         gr.second->Fit("joba2","WQRN");
         gr.second->Fit("joba2","RM+");
 
         joba2->Draw("SAME");

         gr_slope = joba2->GetParameter(0);
         gr_slope_err = joba2->GetParError(0);  
         gr_offset = joba2->GetParameter(1);
         gr_offset_err = joba2->GetParError(1);
          
         fit_chisq = joba2->GetChisquare()/joba2->GetNDF();
         probab = joba2->GetProb();
      }
    }
 
    bool badFit = false;

    if(tdo){
       if(gr_slope>5 || gr_slope < 2){
          badFit=true; 
       }
    }
    if(pdo){
       if(gr_slope>13 || gr_slope < 7){
          badFit=true; 
       }
    }

    if(badFit){ //confining seach of low fit performance to single layer 
      TCanvas *gr_can  = new TCanvas;
      gStyle->SetOptStat(0);
      gStyle->SetOptTitle(0);

      gr.second->GetYaxis()->SetTitle(yLabel.c_str());  
      gr.second->GetXaxis()->SetTitle(xLabel.c_str());
      
      gr.second->SetMarkerStyle(5);
      gr.second->SetMarkerSize(2);

      gr.second->SetMarkerColor(kBlue); 
      gr.second->Draw("ap");
      
      // adding some annotation to plots
      TLatex lat;
      std::string dtype;
      lat.SetTextSize(0.03);
      lat.DrawLatexNDC(0.15,0.8,TString::Format("|BAD FIT, %i points|", gr.second->GetN()));// sprintf(array, "%f", chi));
      lat.DrawLatexNDC(0.15,0.75,TString::Format("|a=%.2f +- %.2f|",gr_slope, gr_slope_err));// sprintf(array, "%f", chi));
      lat.DrawLatexNDC(0.15,0.7,TString::Format("|b=%.2f +- %.2f|",gr_offset, gr_offset_err));// sprintf(array, "%f", chi));
      //lat.DrawLatexNDC(0.15,0.65,TString::Format("|#chi^{2}_{red}=%f|",fit_chisq));// sprintf(array, "%f", chi));
      ///////////////////////////////////////////////////////////////
      gr_can->SaveAs("~/vladwork/low-perf-fits/"+pic_name+"-LOW-PERF-"+feb_chan+".png");
    }

    if(pdo){chan_sat = (1022.0 - gr_offset)/gr_slope;}
    if(check && debug){
      TCanvas *gr_can  = new TCanvas;
      gStyle->SetOptStat(0);
      gStyle->SetOptTitle(0);
      gr.second->Draw("ap");
      // adding some annotation to plots
      TLatex lat;
      std::string dtype;
      if(pdo){dtype="pdo";}
      if(tdo){dtype="tdo";}
      lat.SetTextSize(0.03);
      lat.DrawLatexNDC(0.30,0.8,
          TString::Format("|%s| - %i points",dtype.c_str(),gr.second->GetN()));
      lat.DrawLatexNDC(0.30,0.77,TString::Format("|a=%.4f +- %.4f|",gr_slope, gr_slope_err));
      lat.DrawLatexNDC(0.30,0.74,TString::Format("|b=%.4f +- %.4f|",gr_offset, gr_offset_err));
      //lat.DrawLatexNDC(0.30,0.71,TString::Format("|#chi^{2}_{red}=%.4f|",fit_chisq));
      lat.DrawLatexNDC(0.30,0.71,TString::Format("|p1=%i,p2=%i|",fitmin,fitmax));
      ///////////////////////////////////////////////////////////////
      if(pdo){
         gr_can->SaveAs("plotter_output/fitpdo/"+gr_plotname+".png");
         gr_can->SaveAs("plotter_output/fitpdo/"+gr_plotname+".pdf");
      }
      if(tdo){
         gr_can->SaveAs("plotter_output/fittdo/"+gr_plotname+".png");
         gr_can->SaveAs("plotter_output/fittdo/"+gr_plotname+".pdf");
      }
    }
  ///////////////writing//tree/////////////////////////////////////////////////////////

    int thislink;
    if(!longID){
       thislink = atoi(gr.first.substr(0,6).c_str());
    }else{
       thislink = atoi(gr.first.substr(0,9).c_str());
    }
    NSWChannelID declink(thislink);
    int dec_sector = declink.GetSector();
    int tech = declink.GetTechnology();
    int this_tech;
    std::string which_tech;
    if(tech==0){
      which_tech = "sTGC";
      this_tech = 0;
    }
    else{
      which_tech = "MM";
      this_tech = 1;
    }
    if(dec_sector>0){
      iwheel = 1;
    }
    else{
      iwheel = -1;
    }
    itech = this_tech;
    isect = std::abs(dec_sector);
    std::cout<<"Link:"<<thislink
             <<" side:"<<iwheel
             <<" sector:"<<isect
             <<" Technology:"<<tech<<std::endl;
    if(!longID){
       ilink  = atoi(gr.first.substr(0,6).c_str());
       ilayer = atoi(gr.first.substr(7,1).c_str());
       iradius = atoi(gr.first.substr(9,2).c_str());
       ivmmid = atoi(gr.first.substr(12,1).c_str());
       ichannel = atoi(gr.first.substr(14,2).c_str());
    }else{
       ilink  = atoi(gr.first.substr(0,10).c_str());
       ilayer = atoi(gr.first.substr(11,1).c_str());
       iradius = atoi(gr.first.substr(13,2).c_str());
       ivmmid = atoi(gr.first.substr(16,1).c_str());
       ichannel = atoi(gr.first.substr(18,2).c_str());
    }
    if(check && debug){

      std::cout<<"CHECKS:"
               <<"\nlongID["<<longID<<"]"
               <<"\ncheck["<<check<<"]"
               <<"\ndebug["<<debug<<"]"<<std::endl;
      std::cout<<"\n"<<gr.first<<" : Elink-"<<ilink
                     <<" layer-"  <<ilayer
                     <<" radius-"<<iradius
                     <<" vmm-"<<ivmmid
                     <<" channel-"<<ichannel<<std::endl;
      std::cout<<"\n["<<gr.first<<"]Filling tree:|"<<ilayer
               <<"|"<<iradius<<"|"<<ivmmid<<"|"<<ichannel
               <<"|"<<gr_slope<<"|"<<gr_offset<<"|"<<chan_sat<<"|"<<std::endl;
    }
    ///////////////////////////////////////////////////////////////////////////////
    contree->Fill();  

  }
  contree->Write();
  out_const->Write();

}

//#############################################################################
//                 SUB-MAIN FOR PDO TDO CALIBRATION
//############################################################################
void nsw::PulseCalib::PulseCalib_plot(bool pdo, bool tdo, std::string feb_to_check, std::string pic_name, std::string this_root_file, std::string archive_path){

//// this is kept in memory to later use in pdo vs dac fits //////////
std::map<std::string, std::tuple<float,float,float,float>> vmm_constants; /////// <slope, slope-err, offset, offset-err>
////////////////////////////////////////////////////////////////////

std::cout<<"pdo flag: "<<pdo<<std::endl;
std::cout<<"tdo flag: "<<tdo<<std::endl;
std::cout<<"string to check: "<<feb_to_check<<std::endl;

sleep(1);
/////// pulser calibration ////////
if(pdo){calibrate_pulser_DAC(vmm_constants,pic_name,feb_to_check);}

std::map<std::string,TH1F*> t_hpdo;
std::vector<int> dac_del = {};
int totent = 0;
//////// reading data file ////////////
t_hpdo = read_root_file(this_root_file, pic_name, t_hpdo, dac_del, feb_to_check, totent, pdo, tdo);

///// finding out how many unique delays/DACs are there ///////////////////
int checktrg, currenttrg;// trg_step;

std::vector<int> uniquetrg, uniquetrg_step; //vector of unique dac/delays 
                                            //and vector of dac/delay steps in between

for(int d=2; d<dac_del.size(); d++){
  checktrg = dac_del[d-1];
  currenttrg = dac_del[d];
  if(currenttrg==-1){continue;}
  if(d==2){uniquetrg.push_back(currenttrg);}
  else if(currenttrg>checktrg){
    uniquetrg.push_back(currenttrg);
    uniquetrg_step.push_back(std::fabs(currenttrg - checktrg));
  }
}
dac_del.clear();

//////////////////////////////////////////////////////////////////////////
float avg_ent_per_trgck = std::floor(totent/uniquetrg.size());
///////////////////////////////////////////////////////////////////////////
std::map<std::string, TGraphErrors*> t_gr;

int bin_thresh; //bins i assume... 
//////////trying out peak finding as a separate function///////////////////////
find_peaks(t_hpdo,
           t_gr,
           feb_to_check,
           vmm_constants,
           pic_name,
           avg_ent_per_trgck,
           tdo,
           pdo);
//
//------------------------------------------------------------------------------------------
////////////////////////////////////////////
std::string rootoutfilename;

if(pdo){
  rootoutfilename = "pdofit";
}
if(tdo){
  rootoutfilename = "tdofit";
}
////// fitting calibration key graphs ////////////
fit_calibKey_graphs(t_gr, feb_to_check, rootoutfilename, pic_name, pdo, tdo);
///////////////////

/////////// zdes fit keys should end ////////////////

if(pdo){
   gSystem->CopyFile(Form("pdofit_constants.root",pic_name.c_str()),
                     Form("%s/pdoCalib_backup_%s.root",archive_path.c_str(), 
                                                       pic_name.c_str()));
}

if(tdo){
   gSystem->CopyFile(Form("tdofit_constants.root",pic_name.c_str()),
                     Form("%s/tdoCalib_backup_%s.root",archive_path.c_str(),
                                                       pic_name.c_str()));
}

uniquetrg.clear();
uniquetrg_step.clear();

std::cout<<"⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡿⠿⠿⠿⠛⣉⣥⣤⣤⣶⣶⣶⣤"<<std::endl;
std::cout<<"⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠿⢋⣡⣤⣶⣶⣶⣷⣶⣶⣭⣽⣻⣿⣿⣿⣿"<<std::endl;
std::cout<<"⣿⣿⣿⣿⣿⣿⣿⣿⡿⢋⣡⣾⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿"<<std::endl;
std::cout<<"⠟⠻⢿⣿⣿⣿⣿⠏⣠⡿⠋⣉⢻⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣟"<<std::endl;
std::cout<<"⢸⣿⣦⣝⢿⣿⢏⣴⣿⣄⣀⣠⣾⣿⣿⣿⣿⣿⣿⡿⢛⠻⣿⣿⣿⣿⣿⣿"<<std::endl;
std::cout<<"⢸⣿⣿⣿⡦⢩⣾⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡇⠈⠄⢸⣿⣿⣿⣿⡇"<<std::endl;
std::cout<<"⢸⣿⣿⣿⡇⣿⣿⣿⣿⣷⢙⠿⠿⠿⢿⣿⣿⣿⣿⣿⣶⣴⣾⣿⣿⣿⠟⢠"<<std::endl;
std::cout<<"⠸⣿⣿⣿⣧⠸⣿⣿⣿⣿⡈⠄⢀⣀⣀⣙⣛⣛⡛⣻⣿⣿⣿⣿⣿⣿⢠⣿"<<std::endl;
std::cout<<"⡆⢿⣿⣿⣿⣆⠹⣿⣿⣿⣷⡸⣿⣿⣿⣿⣿⣘⣵⣿⣿⢿⠹⣿⣿⡏⣼⣿"<<std::endl;
std::cout<<"⣧⠸⣿⣿⣿⣿⡷⠈⠙⣿⣿⣿⣮⣭⣭⣷⣿⣿⣿⣿⣃⣼⣾⣿⣿⡇⢻⣿"<<std::endl;
std::cout<<"⣿⣧⠉⠉⠉⠁⠄⠄⢰⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠈⢿"<<std::endl;
std::cout<<"⣿⣿⣷⣄⠄⠄⠄⢠⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣆⠈"<<std::endl;
std::cout<<"⣿⣿⣿⣿⠇⠄⢠⣿▄▄⣿⣿⣿⣿⣿⣿⢟⡻⠿⣿⣿⣿⣿⣿⣿⣿⡿⠄"<<std::endl;
std::cout<<"                            "<<std::endl;
std::cout<<"      Calibration done!     "<<std::endl;

}  
   



