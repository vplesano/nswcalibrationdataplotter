#include "NSWCalibDataPlotter/ThresholdPlotter.h"

nsw::ThresholdPlotter::ThresholdPlotter(){}

using namespace std;

std::string nsw::ThresholdPlotter::add_timestamp(std::string in_string){
  
  time_t rawtime;
  struct tm * timeinfo;
  char buffer[80];
  time(&rawtime);
  timeinfo = localtime(&rawtime);
  strftime(buffer,sizeof(buffer),"%d-%m-%Y_%H-%M-%S",timeinfo);
  std::string str(buffer);
  
  std::cout<<"timestamp test:"<<str<<std::endl;
  std::string out_string = in_string+str;  
  
  return out_string;
}

void nsw::ThresholdPlotter::initH1_pdo(std::map<std::string,TH1D*> &h1,
                                       char* name,
                                       bool scale,
                                       std::string feb){
       h1[name] = new TH1D(name,name,1030,0,1030);
}

void nsw::ThresholdPlotter::initH1_tdo(std::map<std::string,TH1D*> &h1,
                                       char* name,
                                       bool scale,
                                       std::string feb){
       h1[name] = new TH1D(name,name,250,0,250);
}

TH1D* nsw::ThresholdPlotter::getH1_pdo(char* name,
                                       std::map<std::string,TH1D*> &h1 ){
  if (h1.count(name)==0){
    std::cout << "cannot find this histogram: "<< name << std::endl;
    return 0;
  }
  else{
    return h1[name];
  }
}

TH1D* nsw::ThresholdPlotter::getH1_tdo(char* name,
                                       std::map<std::string,TH1D*> &h1 ){
  if (h1.count(name)==0){
    std::cout << "cannot find this histogram: "<< name << std::endl;
    return 0;
  }
  else{
    return h1[name];
  }
}

void nsw::ThresholdPlotter::initH2_DetOut_trgCalibKey(std::map<std::string,TH2D*> &h2,
                                                      char* name,
                                                      std::string feb,
                                                      double n_events){
      h2[name] = new TH2D(name,name,1200,0,1200,1250,0,1250); 
}

TH2D* nsw::ThresholdPlotter::getH2_DetOut_trgCalibKey(char* name,
                                                      std::map<std::string,TH2D*> &h2 ){
  if (h2.count(name)==0){
    std::cout << "cannot find histogram with name: "<< name << std::endl;
    return 0;
  }
  else{
    return h2[name];
  }
}

void nsw::ThresholdPlotter::initH2(std::map<std::string,TH2D*> &h2,
                                   char* name,
                                   bool scale,
                                   std::string feb){
//  std::string feb = name;
   bool mmfe = feb.find("MMFE")!=std::string::npos;
   bool pfeb = feb.find("PFEB")!=std::string::npos;
   bool sfeb = feb.find("SFEB")!=std::string::npos;
   bool strips = feb.find("Strip")!=std::string::npos;
   bool pads = feb.find("Pad")!=std::string::npos;

   if(mmfe){
       if(scale==true){h2[name] = new TH2D(name,name,512,0,512,1200,0,1200);}
      else{h2[name] = new TH2D(name,name,512,0,512,600,0,600);} 
      std::cout<<"Found MMFE"<<std::endl;
   }
   else if(pfeb){
       std::cout<<"FOUND pFEB"<<std::endl;
       if(scale==true){h2[name] = new TH2D(name,name,192,0,192,1200,0,1200);}
      else{h2[name] = new TH2D(name,name,192,0,192,600,0,600);} 
   }
   else if(sfeb){
       std::cout<<"FOUND sFEB"<<std::endl;
       if(scale==true){h2[name] = new TH2D(name,name,512,0,512,1200,0,1200);}
      else{h2[name] = new TH2D(name,name,512,0,512,600,0,600);} 
   }
   else if(strips){
       std::cout<<"FOUND STRIPS"<<std::endl;
       if(scale==true){h2[name] = new TH2D(name,name,512,0,512,1200,0,1200);}
      else{h2[name] = new TH2D(name,name,512,0,512,600,0,600);} 
   }
   else if(pads){
       std::cout<<"FOUND PADS"<<std::endl;
       if(scale==true){h2[name] = new TH2D(name,name,192,0,192,1200,0,1200);}
      else{h2[name] = new TH2D(name,name,192,0,192,600,0,600);} 
   }
   else{
     std::cout<<"[initH2]Do we actually have this kind of boards << "<<feb<<" >> ?"<<std::endl;
   }
}

TH2D* nsw::ThresholdPlotter::getH2(char* name,
                                   std::map<std::string,TH2D*> &h2 ){
  if (h2.count(name)==0){
    std::cout << "cannot find h2: "<< name << std::endl;
    return 0;
  }
  else{
    return h2[name];
  }
}

void nsw::ThresholdPlotter::initH2_noise(std::map<std::string,TH2D*> &h2,
                                         char* name,
                                         bool scale,
                                         std::string feb){

   bool mmfe = feb.find("MMFE")!=std::string::npos;
   bool pfeb = feb.find("PFEB")!=std::string::npos;
   bool sfeb = feb.find("SFEB")!=std::string::npos;
   bool strips = feb.find("Strip")!=std::string::npos;
   bool pads = feb.find("Pad")!=std::string::npos;

   int nbins = 500;
   int nbins_full = 1000;
   int hmin = 0;//mV
   int hmax = 5;//mV
   int hmax_full = 15;

   if(mmfe==true){
       if(scale==false){h2[name] = new TH2D(name,name,512,0,512,nbins,0,hmax);}
      else{h2[name] = new TH2D(name,name,512,0,512,nbins_full,0,hmax_full);} 
   }
   else if(pfeb==true){
       if(scale==false){h2[name] = new TH2D(name,name,192,0,192,nbins,0,hmax);}
      else{h2[name] = new TH2D(name,name,192,0,192,nbins_full,0,hmax_full);} 
   }
   else if(sfeb==true){
       if(scale==false){h2[name] = new TH2D(name,name,512,0,512,nbins,0,hmax);}
      else{h2[name] = new TH2D(name,name,512,0,512,nbins_full,0,hmax_full);} 
   }
   else if(strips){
       std::cout<<"FOUND STRIPS"<<std::endl;
       if(scale==true){h2[name] = new TH2D(name,name,512,0,512,nbins,0,hmax);}
      else{h2[name] = new TH2D(name,name,512,0,512,nbins,0,hmax);} 
   }
   else if(pads){
       std::cout<<"FOUND PADS"<<std::endl;
       if(scale==true){h2[name] = new TH2D(name,name,192,0,192,nbins,0,hmax);}
      else{h2[name] = new TH2D(name,name,192,0,192,nbins,0,hmax);} 
   }
   else{
     std::cout<<"[inintH2_noise] Do we actually have this kind of boards << "<<feb<<" >> ?"<<std::endl;
   }
}

void nsw::ThresholdPlotter::initH2_noise_over(std::map<std::string,TH2D*> &h2,
                                              char* name,
                                              bool scale,
                                              std::string feb){

   bool mmfe = feb.find("MMFE")!=std::string::npos;
   bool pfeb = feb.find("PFEB")!=std::string::npos;
   bool sfeb = feb.find("SFEB")!=std::string::npos;
   bool strips = feb.find("Strip")!=std::string::npos;
   bool pads = feb.find("Pad")!=std::string::npos;

   if(mmfe==true){
       if(scale==false){h2[name] = new TH2D(name,name,512,0,512,8100,0,8100);}
      else{h2[name] = new TH2D(name,name,512,0,512,30100,0,30100);} 
   }
   else if(pfeb==true){
       if(scale==false){h2[name] = new TH2D(name,name,192,0,192,8100,0,8100);}
      else{h2[name] = new TH2D(name,name,192,0,192,30100,0,30100);} 
   }
   else if(sfeb==true){
       if(scale==false){h2[name] = new TH2D(name,name,512,0,512,8100,0,8100);}
      else{h2[name] = new TH2D(name,name,512,0,512,30100,0,30100);} 
   }
   else if(strips){
       std::cout<<"FOUND STRIPS"<<std::endl;
       if(scale==true){h2[name] = new TH2D(name,name,512,0,512,8100,0,8100);}
      else{h2[name] = new TH2D(name,name,512,0,512,600,0,600);} 
   }
   else if(pads){
       std::cout<<"FOUND PADS"<<std::endl;
       if(scale==true){h2[name] = new TH2D(name,name,192,0,192,8100,0,8100);}
      else{h2[name] = new TH2D(name,name,192,0,192,600,0,600);} 
   }
   else{
     std::cout<<"[initH2_noise_over] Do we actually have this kind of boards << "<<feb<<" >> ?"<<std::endl;
   }
}
//////////////// for layer histogram init ////////////////////////////////////////////
void nsw::ThresholdPlotter::initH2_layer(std::map<std::string,TH2D*> &h2,
                                         char* name,
                                         bool scale){
   h2[name] = new TH2D(name,name,8192,0,8192,10000,0,10000);
}
////////////////////////////////////////////////////////////////////////////////////
//===============for sampled baselines======================
void nsw::ThresholdPlotter::initH2F(std::map<std::string,TH2F*> &h2f,
                                    char* name,
                                    bool scale,
                                    std::string feb){

   bool mmfe = feb.find("MMFE")!=std::string::npos;
   bool pfeb = feb.find("PFEB")!=std::string::npos;
   bool sfeb = feb.find("SFEB")!=std::string::npos;
   bool strips = feb.find("Strip")!=std::string::npos;
   bool pads = feb.find("Pad")!=std::string::npos;

   if(mmfe==true){
     std::cout<<"Found - MM ["<<feb<<"]"<<std::endl;
       if(scale==false){h2f[name] = new TH2F(name,name,512,0,512,300,50,350);}
      else{h2f[name] = new TH2F(name,name,512,0,512,1200,0,1200);} 
   }
   else if(pfeb==true){
     std::cout<<"Found - pFEB ["<<feb<<"]"<<std::endl;
       if(scale==false){h2f[name] = new TH2F(name,name,192,0,192,300,50,350);}
      else{h2f[name] = new TH2F(name,name,192,0,192,1200,0,1200);} 
   }
   else if(sfeb==true){
     std::cout<<"Found - sFEB ["<<feb<<"]"<<std::endl;
       if(scale==false){h2f[name] = new TH2F(name,name,512,0,512,300,50,350);}
      else{h2f[name] = new TH2F(name,name,512,0,512,1200,0,1200);} 
   }
   else if(strips){
       std::cout<<"FOUND STRIPS"<<std::endl;
       if(scale==true){h2f[name] = new TH2F(name,name,512,0,512,1200,0,1200);}
      else{h2f[name] = new TH2F(name,name,512,0,512,600,0,600);} 
   }
   else if(pads){
       std::cout<<"FOUND PADS"<<std::endl;
       if(scale==true){h2f[name] = new TH2F(name,name,192,0,192,1200,0,1200);}
      else{h2f[name] = new TH2F(name,name,192,0,192,600,0,600);} 
   }
   else{
     std::cout<<"[initH2F] Do we actually have this kind of boards << "<<feb<<" >> ?"<<std::endl;
   }
}

void nsw::ThresholdPlotter::initH1F_dac(std::map<std::string,TH1F*> &h1f, char* name){
       h1f[name] = new TH1F(name,name,2300,0,2300);
}

TH1F* nsw::ThresholdPlotter::getH1F(char* name, std::map<std::string,TH1F*> &h1f ){
  if (h1f.count(name)==0){
    std::cout << "cannot find h1f: "<< name << std::endl;
    return 0;
  }
  else{
    return h1f[name];
  }
}

TH2F* nsw::ThresholdPlotter::getH2F(char* name, std::map<std::string,TH2F*> &h2f ){
  if (h2f.count(name)==0){
    std::cout << "cannot find h2f: "<< name << std::endl;
    return 0;
  }
  else{
    return h2f[name];
  }
}
///////////////////////////////////////////////////////////
//////////// MAIN THRESHOLD PLOTTER //////////////////////
/////////////////////////////////////////////////////////
void nsw::ThresholdPlotter::ThrCalib_plot(std::string data_path,
                                          std::string archive_path,
                                          std::string json_config,
                                          std::string pic_name,
                                          std::string this_str,
                                          bool bl_on,
                                          bool thr_on,
                                          bool scale,
                                          bool notitle){


 std::vector<std::string> bl_frontends;
 std::vector<std::string> thr_frontends;

 if(bl_on){
   bl_frontends = getAvailableFebs("root_files/baseline_tree.root", bl_on);
   std::cout<<"GOT: "<<bl_frontends.size()<<std::endl;
   sleep(2);
 }

 if(thr_on){
   thr_frontends = getAvailableFebs("root_files/calib_data_tree.root", bl_on);
   std::cout<<"GOT: "<<thr_frontends.size()<<std::endl;

   for(auto &f: thr_frontends){
     std::cout<<f<<std::endl;
   }
   sleep(2);
 }
 
//--------- opening generated root files -----------------------

  TFile *f = new TFile("root_files/calib_data_tree.root");
  std::cout<< f <<std::endl;
  TTree *par_tree=(TTree* )f->Get("par_tree");
  std::cout<< par_tree<<std::endl;

//-----------------------------------------------
  TFile *f1 = new TFile("root_files/threshold_tree.root");
  TTree *thr_tree=(TTree* )f1->Get("thr_tree");
  std::cout<< thr_tree<<std::endl;
//-------------------------------------------------

  TFile *f2 = new TFile("root_files/baseline_tree.root");
  TTree *bl_tree = (TTree*)f2->Get("bl_tree");
  std::cout<<bl_tree<<std::endl;

//------------------------------------------------------
//define maps to hold the pointers to the histos
std::map<std::string,TH2D*> h1; //baseline hists
std::map<std::string,TH2D*> h3; // channel trimmed threshold hists
std::map<std::string,TH2D*> h4; //untrimed thresholds from dif root file
std::map<std::string,TH2D*> h5; //channel noise rms histograms
std::map<std::string,TH2D*> htno; //channel noise rms histograms
std::map<std::string,TH2D*> hn; //channel noise rms histograms from direct bl measurements
std::map<std::string,TH2D*> hno; //channel noise rms overflow histograms from direct bl measurements
std::map<std::string,TH2D*> h6; //channel effective threshold
std::map<std::string,TH2D*> h7; // median sample RMS over ALL VMM channels hist
std::map<std::string,TH2F*> bl; // baseline hist
std::map<std::string,TH2D*> hmask; //masked channels

//define map to hold canvas per mmfe8
std::map<std::string,TCanvas*> c;
std::map<std::string,TCanvas*> n;
//std::map<std::string,TCanvas*> c_blo;
std::map<std::string,TCanvas*> c_bl;
std::map<std::string,TCanvas*> c_no;

//=========================================================================================
std::vector<std::string> FEBs;
if(bl_frontends.size()==0 && thr_frontends.size()!=0){
   for(auto &t: thr_frontends){
      if(t.find(this_str)!=std::string::npos){
         FEBs.push_back(t);
      }
   }
}else if(bl_frontends.size()!=0 && thr_frontends.size()==0){
   for(auto &b: bl_frontends){
      if(b.find(this_str)!=std::string::npos){
         FEBs.push_back(b);
      }
   }
}

long int n_mmfe = FEBs.size();

std::sort(FEBs.begin(),FEBs.end());

for(auto &fb: FEBs){
   std::cout<<fb<<std::endl;
}

std::cout<<"we have: "<<FEBs.size()<<" FEBs to describe"<<std::endl;

sleep(2);
//=========================  define histos for each MMFE8 ====================================
std::cout<<"initializing histograms"<<std::endl;
for(long int i_mmfe=0; i_mmfe<n_mmfe; i_mmfe++){

  if(thr_on==true){
    initH2(h1,Form("ch_baseline_med_vs_channel_%s",FEBs[i_mmfe].c_str()), scale, FEBs[i_mmfe]);
    initH2(h3,Form("ch_trimmed_thr_vs_channel_%s",FEBs[i_mmfe].c_str()),scale, FEBs[i_mmfe]);
    initH2(h4,Form("ch_threshold_ut_vs_channel_%s",FEBs[i_mmfe].c_str()),scale, FEBs[i_mmfe]);
//---------effective threshold ------------------------------------------
    initH2(h6,
           Form("ch_eff_thr_w_best_trim_vs_channel_%s",
                FEBs[i_mmfe].c_str()),
                scale,
                FEBs[i_mmfe]);
    initH2(hmask,
           Form("mask_vs_channel_%s",FEBs[i_mmfe].c_str()),scale, FEBs[i_mmfe]);
//------------noise histo -------------------------------------------
    initH2_noise(h5,
                 Form("ch_baseline_rms_vs_channel_%s",FEBs[i_mmfe].c_str()),scale, FEBs[i_mmfe]);
    initH2_noise(h7,
                 Form("vmm_median_rms_vs_channel_%s",FEBs[i_mmfe].c_str()),scale, FEBs[i_mmfe]);
    //initH2_noise(htno,
    //             Form("ch_baseline_rms_overflow_vs_channel_%s",
    //                  FEBs[i_mmfe].c_str()),
    //                  scale, 
    //                  FEBs[i_mmfe]);
  }
  if(bl_on==true){
    initH2_noise(hn,
                 Form("baseline_rms_vs_channel_%s",FEBs[i_mmfe].c_str()),scale, FEBs[i_mmfe]);
    //initH2_noise_over(hno,
    //                  Form("noise_overflow_vs_channel_%s",
    //                       FEBs[i_mmfe].c_str()),
     //                      scale,
      //                     FEBs[i_mmfe]);
//------------baselines-------------------------------------------------
     initH2F(bl,Form("samples_vs_channel_%s",FEBs[i_mmfe].c_str()),scale, FEBs[i_mmfe]);
    }
}

//======================== set style =========================================================
  std::cout<<"Formating histograms"<<std::endl;
for(int i_mmfe=0; i_mmfe<n_mmfe; i_mmfe++){
//------------------ baseline and thresholds ----------------------------
  if(thr_on==true){
    //----------------------- channel baseline median ----------------------
    getH2(
       Form("ch_baseline_med_vs_channel_%s",FEBs[i_mmfe].c_str()),h1)->SetStats(0);
    getH2(
       Form("ch_baseline_med_vs_channel_%s",
            FEBs[i_mmfe].c_str()),
            h1)->SetTitle(Form("%s Baseline and Thresholds",FEBs[i_mmfe].c_str()));
    getH2(
       Form("ch_baseline_med_vs_channel_%s",
            FEBs[i_mmfe].c_str()),
            h1)->SetMarkerStyle(7);
    getH2(
       Form("ch_baseline_med_vs_channel_%s",
            FEBs[i_mmfe].c_str()),
            h1)->SetMarkerColor(kBlack);
    getH2(
       Form("ch_baseline_med_vs_channel_%s",
            FEBs[i_mmfe].c_str()),
            h1)->GetYaxis()->SetTitle("Voltages,[mV]");
    getH2(
       Form("ch_baseline_med_vs_channel_%s",
            FEBs[i_mmfe].c_str()),
            h1)->GetXaxis()->SetTitle("Mapped channel");
    //----------------------- channel trimmed threshold -------------------------
    getH2(Form("ch_trimmed_thr_vs_channel_%s",FEBs[i_mmfe].c_str()),h3)->SetMarkerStyle(7);
    getH2(Form("ch_trimmed_thr_vs_channel_%s",FEBs[i_mmfe].c_str()),h3)->SetMarkerColor(kGreen);
    getH2(
       Form("ch_trimmed_thr_vs_channel_%s",FEBs[i_mmfe].c_str()),h3)->GetYaxis()->SetTitle("mV");
    getH2(
       Form("ch_trimmed_thr_vs_channel_%s",
            FEBs[i_mmfe].c_str()),
            h3)->GetXaxis()->SetTitle("Mapped channel");
    //----------------------- channel untrimmed threshold -----------------------
    getH2(Form("ch_threshold_ut_vs_channel_%s",FEBs[i_mmfe].c_str()),h4)->SetMarkerStyle(7);
    getH2(Form("ch_threshold_ut_vs_channel_%s",FEBs[i_mmfe].c_str()),h4)->SetMarkerColor(kBlue);
    getH2(
       Form("ch_threshold_ut_vs_channel_%s",FEBs[i_mmfe].c_str()),h4)->GetYaxis()->SetTitle("mV");
    getH2(
       Form("ch_threshold_ut_vs_channel_%s",
            FEBs[i_mmfe].c_str()),
            h4)->GetXaxis()->SetTitle("Mapped channel");
    //----------------------- effective threshold ---------------------------------
    getH2(
       Form("ch_eff_thr_w_best_trim_vs_channel_%s",
            FEBs[i_mmfe].c_str()),
            h6)->SetMarkerStyle(7);
    getH2(
       Form("ch_eff_thr_w_best_trim_vs_channel_%s",
            FEBs[i_mmfe].c_str()),
            h6)->SetMarkerColor(kYellow+3);
    getH2(Form("ch_eff_thr_w_best_trim_vs_channel_%s",
          FEBs[i_mmfe].c_str()),
          h6)->GetYaxis()->SetTitle("Voltages,[mV]");
    getH2(
       Form("ch_eff_thr_w_best_trim_vs_channel_%s",
            FEBs[i_mmfe].c_str()),
            h6)->GetXaxis()->SetTitle("Mapped channel");    
    //---------------------- noise plots ----------------------------------------
     getH2(
       Form("ch_baseline_rms_vs_channel_%s",FEBs[i_mmfe].c_str()),h5)->SetStats(0);
    getH2(
       Form("ch_baseline_rms_vs_channel_%s",
            FEBs[i_mmfe].c_str()),
            h5)->SetTitle(Form("%s Channel Noise", FEBs[i_mmfe].c_str()));
    getH2(
       Form("ch_baseline_rms_vs_channel_%s",
            FEBs[i_mmfe].c_str()),
            h5)->SetMarkerStyle(7);
    getH2(Form("ch_baseline_rms_vs_channel_%s",
               FEBs[i_mmfe].c_str()),
               h5)->SetMarkerColor(kMagenta+3);
    getH2(Form("ch_baseline_rms_vs_channel_%s",
               FEBs[i_mmfe].c_str()),
               h5)->GetYaxis()->SetTitle("Baseline RMS, [mV]");
    getH2(Form("ch_baseline_rms_vs_channel_%s",
               FEBs[i_mmfe].c_str()),
               h5)->GetXaxis()->SetTitle("Mapped channel");
    // //-----------for overflow to see readings out of scope of the histogram -------------
    // getH2(Form("ch_baseline_rms_overflow_vs_channel_%s",
    //            FEBs[i_mmfe].c_str()),htno)->SetStats(0);
    //getH2(Form("ch_baseline_rms_overflow_vs_channel_%s",
    //           FEBs[i_mmfe].c_str()),htno)->SetMarkerStyle(22);
    //getH2(Form("ch_baseline_rms_overflow_vs_channel_%s",
    //           FEBs[i_mmfe].c_str()),htno)->SetMarkerColor(kBlack);
    //getH2(Form("ch_baseline_rms_overflow_vs_channel_%s",
    //           FEBs[i_mmfe].c_str()),htno)->GetYaxis()->SetTitle("ENC");
    //getH2(Form("ch_baseline_rms_overflow_vs_channel_%s",
    //            FEBs[i_mmfe].c_str()),htno)->GetXaxis()->SetTitle("Channel");
    ////--------------- median RMS of samples over ALL VMM channels------------------
     getH2(Form("vmm_median_rms_vs_channel_%s",FEBs[i_mmfe].c_str()),h7)->SetStats(0);
     getH2(Form("vmm_median_rms_vs_channel_%s",FEBs[i_mmfe].c_str()),h7)->SetMarkerStyle(7);
     getH2(Form("vmm_median_rms_vs_channel_%s",FEBs[i_mmfe].c_str()),h7)->SetMarkerColor(kGreen+2);
     getH2(Form("vmm_median_rms_vs_channel_%s",
                FEBs[i_mmfe].c_str()),h7)->GetYaxis()->SetTitle("Median noise, [mV]");
     getH2(Form("vmm_median_rms_vs_channel_%s",
                FEBs[i_mmfe].c_str()),h7)->GetXaxis()->SetTitle("Mapped channel");
    //-------------- masked channels --------------------------------
     getH2(Form("mask_vs_channel_%s",FEBs[i_mmfe].c_str()),hmask)->SetStats(0);
     getH2(Form("mask_vs_channel_%s",FEBs[i_mmfe].c_str()),hmask)->SetMarkerStyle(21);
     getH2(Form("mask_vs_channel_%s",FEBs[i_mmfe].c_str()),hmask)->SetMarkerColor(kBlue);
  }
  ///////////////////
  if(bl_on==true){
  //--------------------- noise plot from direct bl measurement ------------------
      getH2(Form("baseline_rms_vs_channel_%s",FEBs[i_mmfe].c_str()),hn)->SetStats(0);
      getH2(Form("baseline_rms_vs_channel_%s",
            FEBs[i_mmfe].c_str()),hn)->SetTitle(Form("%s Channel Noise", FEBs[i_mmfe].c_str()));
      getH2(Form("baseline_rms_vs_channel_%s",FEBs[i_mmfe].c_str()),hn)->SetMarkerStyle(7);
      getH2(Form("baseline_rms_vs_channel_%s",FEBs[i_mmfe].c_str()),hn)->SetMarkerColor(kRed+2);
      getH2(Form("baseline_rms_vs_channel_%s",
            FEBs[i_mmfe].c_str()),hn)->GetYaxis()->SetTitle("Baseline RMS, [mV]");
      getH2(Form("baseline_rms_vs_channel_%s",
            FEBs[i_mmfe].c_str()),hn)->GetXaxis()->SetTitle("Mapped channel");

      //getH2(Form("noise_overflow_vs_channel_%s",FEBs[i_mmfe].c_str()),hno)->SetMarkerStyle(22);
      //getH2(Form("noise_overflow_vs_channel_%s",FEBs[i_mmfe].c_str()),hno)->SetMarkerSize(1);
      //getH2(Form("noise_overflow_vs_channel_%s",FEBs[i_mmfe].c_str()),hno)->SetMarkerColor(kBlack);
      //getH2(Form("noise_overflow_vs_channel_%s",
      //           FEBs[i_mmfe].c_str()),hno)->GetYaxis()->SetTitle("ENC");
      //getH2(Form("noise_overflow_vs_channel_%s",
      //           FEBs[i_mmfe].c_str()),hno)->GetXaxis()->SetTitle("Channel");  
    //----------------------- baseline samples -------------------------------
      getH2F(Form("samples_vs_channel_%s",FEBs[i_mmfe].c_str()),bl)->SetStats(0);
      getH2F(Form("samples_vs_channel_%s",
         FEBs[i_mmfe].c_str()),bl)->SetTitle(Form("%s Baseline",FEBs[i_mmfe].c_str()));
      getH2F(Form("samples_vs_channel_%s",FEBs[i_mmfe].c_str()),bl)->GetYaxis()->SetTitle("Baseline, [mV]");
      getH2F(Form("samples_vs_channel_%s",
         FEBs[i_mmfe].c_str()),bl)->GetXaxis()->SetTitle("Mapped channel");
    }

}

//========================== fill histos ======================

//int e_to_mV = 8000*9/6242;
int e_to_mV;
std::map<std::string, int> nr_masked_chan;
float scaConvConst = 0; 

if(thr_on){
  //-------- filling the threshold vs baseline plots -----------------
  for(int i_mmfe=0; i_mmfe<n_mmfe; i_mmfe++){
        if(FEBs.at(i_mmfe).find("MMFE")!=std::string::npos){
          scaConvConst = 1000*1.5/4095.0;
        }
        else{
          scaConvConst = 1000/4095.0;
        }           
 
        std::cout<<"Assembling plots for FEB - "<<FEBs[i_mmfe]<<std::endl;
        std::cout<<Form("ch_baseline_med:channel_id+cvmm*64>>ch_baseline_med_vs_channel_%s",
                   FEBs[i_mmfe].c_str())
        <<Form("board==\"%s\"",FEBs[i_mmfe].c_str()) 
        ///////////////////////////
        <<std::endl;
        std::cout<<Form("ch_trimmed_thr*%f:channel_id+cvmm*64>>ch_trimmed_thr_vs_channel_%s",
                         scaConvConst, 
                         FEBs[i_mmfe].c_str())
        <<Form("board==\"%s\"", FEBs[i_mmfe].c_str()) 
        <<std::endl;
        ///////////////////////////
        std::cout<<Form("channel_thr:channel_id+cvmm*64>>ch_threshold_ut_vs_channel_%s",
                        FEBs[i_mmfe].c_str())
        <<Form("board==\"%s\"", FEBs[i_mmfe].c_str()) 
        <<std::endl;
        ///////////////////////////
        std::cout<<Form("ch_eff_thr_w_best_trim:channel_id+cvmm*64>>ch_eff_thr_w_best_trim_vs_channel_%s",FEBs[i_mmfe].c_str())
        <<Form("board==\"%s\"", FEBs[i_mmfe].c_str()) 
        <<std::endl;
        ///////////////////////////
  
        std::cout<<"Drawing threshold histos for - "<<FEBs[i_mmfe]<<std::endl;
        std::cout<<par_tree->Draw(
                   Form("ch_baseline_med:channel_id+cvmm*64>>ch_baseline_med_vs_channel_%s",
                        FEBs[i_mmfe].c_str()),
                        Form("board==\"%s\"",FEBs[i_mmfe].c_str()),"goff")<<std::endl;
        std::cout<<par_tree->Draw(
                   Form("ch_trimmed_thr*%f:channel_id+cvmm*64>>ch_trimmed_thr_vs_channel_%s",
                        scaConvConst,
                        FEBs[i_mmfe].c_str()),
                        Form("board==\"%s\"",FEBs[i_mmfe].c_str()),"goff")<<std::endl;
        std::cout<<par_tree->Draw(
                   Form("ch_eff_thr_w_best_trim*%f:channel_id+cvmm*64>>ch_eff_thr_w_best_trim_vs_channel_%s",
                   scaConvConst,
                   FEBs[i_mmfe].c_str()),
                   Form("board==\"%s\"",FEBs[i_mmfe].c_str()),"goff")<<std::endl;
        std::cout<<par_tree->Draw(
                   Form("ch_mask:channel_id+cvmm*64>>mask_vs_channel_%s",
                   FEBs[i_mmfe].c_str()),
                   Form("board==\"%s\" && ch_mask!=0",FEBs[i_mmfe].c_str()),"goff")<<std::endl;

        int mask = getH2(Form("mask_vs_channel_%s",FEBs[i_mmfe].c_str()),hmask)->GetEntries();
        std::cout<<mask<<std::endl;
    
        std::cout <<thr_tree->Draw(
                    Form("channel_thr:channel_id+cvmm*64>>ch_threshold_ut_vs_channel_%s",
                    FEBs[i_mmfe].c_str()),
                    Form("board==\"%s\"",FEBs[i_mmfe].c_str()),"goff")<<std::endl;
       
        nr_masked_chan[FEBs[i_mmfe]] = 
                  getH2(Form("mask_vs_channel_%s",FEBs[i_mmfe].c_str()),hmask)->GetEntries();
    
  }

  for(int i_mmfe=0; i_mmfe<n_mmfe; i_mmfe++)
  {

    if(FEBs.at(i_mmfe).find("MMFE")!=std::string::npos){
      e_to_mV = 8000*9/6242;
    }
    else{
      e_to_mV = 8000*3/6242;
    }
    std::cout<<"Drawing noise histogramm for "<<FEBs[i_mmfe]<<endl;
    std::cout<<Form("ch_baseline_rms:channel_id+cvmm*64>>ch_baseline_rms_vs_channel_mmfe8-%04i",i_mmfe)
             <<Form("board==\"%s\"",FEBs[i_mmfe].c_str())<<endl;
    std::cout<<Form("vmm_median_rms:channel_id+cvmm*64>>vmm_median_rms_vs_channel_mmfe8-%04i",i_mmfe)
             <<Form("board==\"%s\"",FEBs[i_mmfe].c_str())<<endl;
    std::cout<<par_tree->Draw(
               Form("ch_baseline_rms:channel_id+cvmm*64>>ch_baseline_rms_vs_channel_%s",
                    FEBs[i_mmfe].c_str()),
                    Form("board==\"%s\"",FEBs[i_mmfe].c_str()),"goff")<<std::endl;
    //std::cout<<par_tree->Draw(
    //           Form("7900:channel_id+cvmm*64>>ch_baseline_rms_overflow_vs_channel_%s",
    //                FEBs[i_mmfe].c_str()),
    //                Form("board==\"%s\" && ch_baseline_rms>%i",FEBs[i_mmfe].c_str(), e_to_mV),"goff")<<std::endl;
    std::cout<<par_tree->Draw(
               Form("vmm_median_rms:channel_id+cvmm*64>>vmm_median_rms_vs_channel_%s",
                    FEBs[i_mmfe].c_str()),
                    Form("board==\"%s\"",FEBs[i_mmfe].c_str()),"goff")<<std::endl;

  }

}//thr_on bool if expresion ends
//-------- filling pure baseline plots ---------------------------------------------------------------------------------------------------

if(bl_on){

  for(int i_mmfe=0; i_mmfe<n_mmfe; i_mmfe++){

    if(FEBs.at(i_mmfe).find("MMFE")!=std::string::npos){
      e_to_mV = 8000*9/6242;
    }
    else{
      e_to_mV = 8000*3/6242;
    }
    std::cout<<"Drawing baseline histogramm for "<<FEBs[i_mmfe]<<std::endl;
    std::cout<<Form("sample:channel_id+cvmm*64>>samples_vs_channel_%s",
                    FEBs[i_mmfe].c_str())
             <<Form("board==\"%s\"",FEBs[i_mmfe].c_str(),"goff") 
             <<std::endl;
    std::cout<<"Noise plot for"<<FEBs[i_mmfe]<<std::endl;
    std::cout<<Form("rms:channel_id+cvmm*64>>baseline_rms_vs_channel_%s",
                    FEBs[i_mmfe].c_str())
             <<Form("board==\"%s\"",FEBs[i_mmfe].c_str(),"goff") <<std::endl;
    std::cout<<bl_tree->Draw(
                  Form("sample:channel_id+cvmm*64>>samples_vs_channel_%s",
                       FEBs[i_mmfe].c_str()),
                       Form("board==\"%s\"",FEBs[i_mmfe].c_str()),"goff")<<std::endl;
    std::cout<<bl_tree->Draw(
                  Form("rms:channel_id+cvmm*64>>baseline_rms_vs_channel_%s",
                       FEBs[i_mmfe].c_str()),
                       Form("board==\"%s\"",FEBs[i_mmfe].c_str()),"goff")<<std::endl;
    //std::cout<<bl_tree->Draw(
    //              Form("7900:channel_id+cvmm*64>>noise_overflow_vs_channel_%s",
    //                   FEBs[i_mmfe].c_str()),
    //                   Form("board==\"%s\" && rms>%i",FEBs[i_mmfe].c_str(),e_to_mV),"goff")<<std::endl;
  }
}

//
// later here a loop should be added to count overflow entries...
//
//Draw one canvas per MMMFE8
std::string ymd, hms;
TDatime *dt = new TDatime;
const char* datetime = dt->AsString();
//std::cout<<"date: "<<datetime<<" !"<<std::endl;
std::string datetime_str = datetime;
std::replace(datetime_str.begin(),datetime_str.end(),' ','_'); 
std::replace(datetime_str.begin(),datetime_str.end(),':','-'); 

std::string thr_folder_name="";
std::string bl_folder_name="";
if(thr_on==true){
  thr_folder_name = add_timestamp("threshold_calibration_plots_");
  std::string make_thr_folder = "mkdir -p "+archive_path+thr_folder_name; 
  system(make_thr_folder.c_str());
}
if(bl_on==true){
  bl_folder_name = add_timestamp("baseline_plots_");
  std::string make_bl_folder = "mkdir -p "+archive_path+bl_folder_name; 
  system(make_bl_folder.c_str());
}

//##################################################################

if(thr_on){

  for(int i_mmfe=0; i_mmfe<n_mmfe; i_mmfe++){
    c[Form("c_%s",FEBs[i_mmfe].c_str())] = 
           new TCanvas(Form("c_%s",FEBs[i_mmfe].c_str()),
                       Form("c_%s",FEBs[i_mmfe].c_str()),700,500);
    c[Form("c_%s",FEBs[i_mmfe].c_str())]->cd();

    if(notitle){gStyle->SetOptTitle(0);}

    getH2(Form("ch_baseline_med_vs_channel_%s",FEBs[i_mmfe].c_str()),h1)->Draw();
    getH2(Form("ch_trimmed_thr_vs_channel_%s",FEBs[i_mmfe].c_str()),h3)->Draw("SAME");
    getH2(Form("ch_threshold_ut_vs_channel_%s",FEBs[i_mmfe].c_str()),h4)->Draw("SAME");
    getH2(Form("ch_eff_thr_w_best_trim_vs_channel_%s",FEBs[i_mmfe].c_str()),h6)->Draw("SAME");
   
    TLegend *leg=new TLegend(0.1, 0.9, 0.35, 0.75);
    leg->AddEntry(
         getH2(Form("ch_baseline_med_vs_channel_%s",
                    FEBs[i_mmfe].c_str()),h1),"Baseline","P");
    leg->AddEntry(
         getH2(Form("ch_trimmed_thr_vs_channel_%s",
                    FEBs[i_mmfe].c_str()),h3),"Trimmed threshold","P");
    leg->AddEntry(
         getH2(Form("ch_threshold_ut_vs_channel_%s",
                    FEBs[i_mmfe].c_str()),h4),"Untrimmed threshold","P");
    leg->AddEntry(
         getH2(Form("ch_eff_thr_w_best_trim_vs_channel_%s",
                    FEBs[i_mmfe].c_str()),h6),"Effective threshold","P");

    std::string mask_comment = "Tot CH masked: "+std::to_string(nr_masked_chan[FEBs[i_mmfe]]);

    leg->AddEntry((TObject*)0,mask_comment.c_str(),"");
   
    leg->Draw();
   //-----saving pic ---------------
    std::string suffix = changeFeName(FEBs[i_mmfe]);

    c[Form("c_%s",FEBs[i_mmfe].c_str())]->SaveAs(
      Form("plotter_output/thresholds/%s_%s_%s.png",
           suffix.c_str(),pic_name.c_str(), datetime_str.c_str())
      );
    c[Form("c_%s",FEBs[i_mmfe].c_str())]->SaveAs(
      Form("plotter_output/thresholds/%s_%s_%s.pdf",
           suffix.c_str(),pic_name.c_str(), datetime_str.c_str())
      );
    /////////// saving to archive ///////////////
    c[Form("c_%s",FEBs[i_mmfe].c_str())]->SaveAs(
      Form("%s%s/%s_%s_thr_%s.pdf",
           archive_path.c_str(),
           thr_folder_name.c_str(),
           suffix.c_str(),
           pic_name.c_str(), 
           datetime_str.c_str())
      ); 
    c[Form("c_%s",FEBs[i_mmfe].c_str())]->SaveAs(
      Form("%s%s/%s_%s_thr_%s.png",
           archive_path.c_str(), 
           thr_folder_name.c_str(), 
           suffix.c_str(),
           pic_name.c_str(), 
           datetime_str.c_str())
      ); 

    delete c[Form("c_%s",FEBs[i_mmfe].c_str())];
    delete getH2(Form("ch_baseline_med_vs_channel_%s",FEBs[i_mmfe].c_str()),h1);    
    delete getH2(Form("ch_trimmed_thr_vs_channel_%s",FEBs[i_mmfe].c_str()),h3);
    delete getH2(Form("ch_threshold_ut_vs_channel_%s",FEBs[i_mmfe].c_str()),h4);
    delete getH2(Form("ch_eff_thr_w_best_trim_vs_channel_%s",FEBs[i_mmfe].c_str()),h6);
    delete getH2(Form("mask_vs_channel_%s",FEBs[i_mmfe].c_str()),hmask);
  
  }
//############################### threshold noise plots##########################################
  for(int i=0; i<n_mmfe; i++)
  {

    n[Form("n_%s",FEBs[i].c_str())] = 
            new TCanvas(Form("n_%s",FEBs[i].c_str()),Form("n_%s",FEBs[i].c_str()),700,500);
    n[Form("n_%s",FEBs[i].c_str())]->cd();
 
    if(notitle){gStyle->SetOptTitle(0);}
 
    getH2(Form("ch_baseline_rms_vs_channel_%s",FEBs[i].c_str()),h5)->Draw();
    //getH2(Form("ch_baseline_rms_overflow_vs_channel_%s",FEBs[i].c_str()),htno)->Draw("SAME");
    getH2(Form("vmm_median_rms_vs_channel_%s",FEBs[i].c_str()),h7)->Draw("SAME");

    TLegend *leg=new TLegend(0.1, 0.9, 0.3, 0.8);
    leg->AddEntry(getH2(Form("ch_baseline_rms_vs_channel_%s",
                             FEBs[i].c_str()),h5),"Channel noise RMS","P");
    leg->AddEntry(getH2(Form("vmm_median_rms_vs_channel_%s",
                             FEBs[i].c_str()),h7),"VMM meadian noise RMS","P");
    leg->Draw();
//-------- saving --------------
    std::string suffix = changeFeName(FEBs[i]);

    n[Form("n_%s",
           FEBs[i].c_str())]->SaveAs(Form("plotter_output/noise/%s_%s_%s.pdf",
                                          suffix.c_str(),
                                          pic_name.c_str(), 
                                          datetime_str.c_str())
      );
    n[Form("n_%s",
           FEBs[i].c_str())]->SaveAs(Form("plotter_output/noise/%s_%s_%s.png",
                                          suffix.c_str(),
                                          pic_name.c_str(), 
                                          datetime_str.c_str())
      );
    /////////// saving to archive ///////////////
    n[Form("n_%s",FEBs[i].c_str())]->SaveAs(
                  Form("%s%s/%s_%s_noise_%s.pdf",
                       archive_path.c_str(), 
                       thr_folder_name.c_str(), 
                       suffix.c_str(),
                       pic_name.c_str(), 
                       datetime_str.c_str())
      );
    n[Form("n_%s",FEBs[i].c_str())]->SaveAs(
                  Form("%s%s/%s_%s_noise_%s.png",
                       archive_path.c_str(), 
                       thr_folder_name.c_str(), 
                       suffix.c_str(),
                       pic_name.c_str(), 
                       datetime_str.c_str())
      );


    delete n[Form("n_%s",FEBs[i].c_str())];
   
    delete getH2(Form("ch_baseline_rms_vs_channel_%s",FEBs[i].c_str()),h5);
    //delete getH2(Form("ch_baseline_rms_overflow_vs_channel_%s",FEBs[i].c_str()),htno);
    delete getH2(Form("vmm_median_rms_vs_channel_%s",FEBs[i].c_str()),h7);

  }

}
//#################################### baseline plots + noise ######################################################
if(bl_on){

  for(int i_mmfe=0; i_mmfe<n_mmfe; i_mmfe++){

    if(notitle){gStyle->SetOptTitle(0);}
////////////////// plotting baselines ////////////////////////
    c_bl[Form("b_%s",FEBs[i_mmfe].c_str())] = 
              new TCanvas(Form("b_%s",FEBs[i_mmfe].c_str()),Form("c_%s",FEBs[i_mmfe].c_str()),700,500);
    c_bl[Form("b_%s",FEBs[i_mmfe].c_str())]->cd();
    getH2F(Form("samples_vs_channel_%s",FEBs[i_mmfe].c_str()),bl)->Draw("COLZ");

    c_bl[Form("b_%s",FEBs[i_mmfe].c_str())]->SaveAs(
         Form("plotter_output/baselines/%s_baseline_%s_%s.pdf",
              changeFeName(FEBs[i_mmfe]).c_str(),pic_name.c_str(),datetime_str.c_str())
         );
    c_bl[Form("b_%s",FEBs[i_mmfe].c_str())]->SaveAs(
         Form("plotter_output/baselines/%s_baseline_%s_%s.png",
              changeFeName(FEBs[i_mmfe]).c_str(),pic_name.c_str(),datetime_str.c_str())
         );
    /////////// saving to archive_path ///////////////
    c_bl[Form("b_%s",FEBs[i_mmfe].c_str())]->SaveAs(
         Form("%s%s/%s_%s_%s.png",
              archive_path.c_str(), 
              bl_folder_name.c_str(), 
              changeFeName(FEBs[i_mmfe]).c_str(),
              pic_name.c_str(), 
              datetime_str.c_str())
         );
    c_bl[Form("b_%s",FEBs[i_mmfe].c_str())]->SaveAs(
         Form("%s%s/%s_%s_%s.pdf",
              archive_path.c_str(), 
              bl_folder_name.c_str(), 
              changeFeName(FEBs[i_mmfe]).c_str(),
              pic_name.c_str(), 
              datetime_str.c_str()));

/////////////// plotting noise //////////////////////////////
    c_no[Form("no_%s",FEBs[i_mmfe].c_str())] = 
         new TCanvas(Form("no_%s",FEBs[i_mmfe].c_str()),Form("no_%s",FEBs[i_mmfe].c_str()),700,500);  
    c_no[Form("no_%s",FEBs[i_mmfe].c_str())] ->cd();

    getH2(Form("baseline_rms_vs_channel_%s",FEBs[i_mmfe].c_str()),hn)->Draw();
    //getH2(Form("noise_overflow_vs_channel_%s",FEBs[i_mmfe].c_str()),hno)->Draw("SAME");
    std::string suffix = changeFeName(FEBs[i_mmfe]);
    c_no[Form("no_%s",FEBs[i_mmfe].c_str())]->SaveAs(
         Form("plotter_output/noise/%s_noise_%s_%s.png",
              suffix.c_str(),pic_name.c_str(),datetime_str.c_str())
         );
    c_no[Form("no_%s",FEBs[i_mmfe].c_str())]->SaveAs(
         Form("plotter_output/noise/%s_noise_%s_%s.pdf",
              suffix.c_str(),pic_name.c_str(),datetime_str.c_str())
         );
    /////////// saving to archive_path ///////////////
    c_no[Form("no_%s",FEBs[i_mmfe].c_str())]->SaveAs(
              Form("%s%s/%s_noise_%s_%s.png",
                   archive_path.c_str(), 
                   bl_folder_name.c_str(), 
                   suffix.c_str(),
                   pic_name.c_str(), 
                   datetime_str.c_str())
        );
    c_no[Form("no_%s",FEBs[i_mmfe].c_str())]->SaveAs(
              Form("%s%s/%s_noise_%s_%s.pdf",
                   archive_path.c_str(), 
                   bl_folder_name.c_str(), 
                   suffix.c_str(),
                   pic_name.c_str(), 
                   datetime_str.c_str())
        );
    
    delete c_no[Form("no_%s",FEBs[i_mmfe].c_str())];
    delete getH2(Form("baseline_rms_vs_channel_%s",FEBs[i_mmfe].c_str()),hn);
    //delete getH2(Form("noise_overflow_vs_channel_%s",FEBs[i_mmfe].c_str()),hno);

   }

  }//baseline save IF ends

}//// function ends!

std::vector<std::string> nsw::ThresholdPlotter::getAvailableFebs(std::string rootfile,
                                                                 bool baseline){

//--------- opening generated root files -----------------------

  std::cout<<rootfile<<std::endl;
  TFile *f = new TFile(rootfile.c_str());
  std::cout<< f <<std::endl;
  std::string tree;
  if(baseline){
    tree = "bl_tree";
  }else{
    tree = "par_tree";
  } 

//--------- checking available febs-----------------------
  //std::cout<<"1 "<<tree<<", baseline="<<baseline<<std::endl;
  TTreeReader tr(tree.c_str(), f);
  
 // std::cout<<"2"<<std::endl;
  TTreeReaderValue<std::string> ifeb(tr,"board");
  
  //std::cout<<"3"<<std::endl;
  std::string this_feb;
  
  std::vector<std::string> febs;
  
  //std::cout<<"4"<<std::endl;
  while(tr.Next()){
    this_feb = *ifeb;
    if(std::find(febs.begin(),febs.end(),this_feb) == febs.end()){
       febs.push_back(this_feb);
       std::cout<<"PUT:"<<this_feb<<std::endl;      
    }
  }
  //std::cout<<"5"<<std::endl;
  f->Close();

  return febs;

}
 
void nsw::ThresholdPlotter::PlotOldBaseline(std::string file, 
                                            std::string pname,
                                            int exp_samples){

   TH2D *bl = new TH2D("bl","bl",512,0,512,200,50,250);
   TH2D *bl_full = new TH2D("bl_full","bl_full",512,0,512,412,0,1030);

   TH2D *rms = new TH2D("rms","rms",512,0,512,80,0,4);

   std::map<std::string, TH1D*> vmms;

   for(int i=0; i<8; i++){
      std::string key = "vmm"+std::to_string(i);
      vmms[key] = new TH1D(key.c_str(),key.c_str(),120,100,220);
   }
 
   std::cout<<"[1] "<<file<<std::endl;
   if(file.length()==0){
     std::cout<<"USE option -d, idiot"<<std::endl;
     exit(0);
   } 
   std::ifstream input;
   input.open(file);
   std::cout<<"[2] "<<input<<std::endl;  
   std::string line;
   int cntr = 0;
   std::string this_fe;
   int tab_cnt = 0;
   std::vector<float> ch_samp;
   int prev_chan =-1;
   while(std::getline(input, line,'\n')){
        std::cout<<line<<std::endl;
  
      if(cntr==0){
         for(auto &c: line){
            if(c=='\t'){
               tab_cnt++;
            }  
         }
         std::cout<<"Have ["<<tab_cnt<<"] TABS"<<std::endl;
      } 
        
      char fe[20];
      int chan, vmm, sect, side;
      float BL,RMS;
      if(tab_cnt==3){
          
         sscanf(line.c_str(),"%s %i %i %f",&fe,&vmm,&chan,&BL);
      }else if(tab_cnt==4){
         sscanf(line.c_str(),"%s %i %i %f %f",&fe,&vmm,&chan,&BL,&RMS);
      }else if(tab_cnt==6){
         sscanf(line.c_str(),"%i %i %s %i %i %f",&side,&sect,&fe,&vmm,&chan,&BL,&RMS);
      }else{
         std::cout<<"I have no expected pattern for this data format:"<<tab_cnt<<std::endl;
      }
     
      if(tab_cnt==3 || tab_cnt==6){
         ch_samp.push_back(BL);
         if(cntr%exp_samples==0){
            float mean = std::accumulate(ch_samp.begin(),
                                         ch_samp.end(),0.0)/ch_samp.size();
            float sq_sum = std::inner_product(ch_samp.begin(), 
                                              ch_samp.end(), ch_samp.begin(), 0.0);
            float stdev = std::sqrt(sq_sum / ch_samp.size() - mean * mean);
            rms->Fill(chan+vmm*64,stdev);
            ch_samp.clear();
         }
      }      

      bl->Fill(chan+64*vmm,BL);
      bl_full->Fill(chan+64*vmm,BL);
      std::string thisKey = "vmm"+std::to_string(vmm);
      //std::string thisKeyRms = "rms"+std::to_string(vmm);
      vmms[thisKey]->Fill(BL);
      //vmm[thisKeyRms]->Fill(BL);
      if(cntr==0){this_fe=fe;}
      int prev_chan = chan;
      cntr++;
   }

   int nsamples = cntr/512;

   std::cout<<"READ HAVE ["<<cntr<<"] LINES"<<std::endl;

   input.close();   
   /////////////////////////////////////////////////////////////////

   TCanvas *cnv = new TCanvas();
   cnv->SetCanvasSize(800,600);
   gStyle->SetOptStat(0); 
   gStyle->SetOptTitle(0); 
   bl->GetXaxis()->SetTitle("Mapped channel");
   bl->GetYaxis()->SetTitle("V_{baseline}, [mV]");
   bl->Draw("COLZ");

    TLatex tlx;
    tlx.SetTextSize(0.03);
    tlx.DrawLatexNDC(0.6,0.84,
        TString::Format("N_{samples} = %i [chan^{-1}]",nsamples));

   cnv->SaveAs(Form("OLD-baseline-%s-%s.png",this_fe.c_str(),pname.c_str()));
   cnv->SaveAs(Form("OLD-baseline-%s-%s.pdf",this_fe.c_str(),pname.c_str()));
   delete cnv;
   //////////// FULL RANGE baseline  //////////////////
   TCanvas *cnv2 = new TCanvas();
   cnv2->SetCanvasSize(800,600);
   gStyle->SetOptStat(0); 
   gStyle->SetOptTitle(0); 
   bl_full->GetXaxis()->SetTitle("Mapped channel");
   bl_full->GetYaxis()->SetTitle("V_{baseline}, [mV]");
   bl_full->Draw("COLZ");

    TLatex tlx2;
    tlx2.SetTextSize(0.03);
    tlx2.DrawLatexNDC(0.6,0.84,
        TString::Format("N_{samples} = %i [chan^{-1}]",nsamples));

   cnv2->SaveAs(Form("OLD-FULL-baseline-%s-%s.png",this_fe.c_str(),pname.c_str()));
   cnv2->SaveAs(Form("OLD-FULL-baseline-%s-%s.pdf",this_fe.c_str(),pname.c_str()));
   delete cnv2;
   ///////////////////////////////////////////////////////
  
   for(auto &h: vmms){
     TCanvas *c1 = new TCanvas();
     h.second->GetXaxis()->SetTitle("V_{sample},[mV]");
     h.second->GetYaxis()->SetTitle("# samples");
     h.second->Draw();
     c1->SaveAs(Form("sampleDist-%s-%s-%s.png",this_fe.c_str(),h.first.c_str(), pname.c_str()));
     delete c1;
   }

   TCanvas *r = new TCanvas();
   rms->GetXaxis()->SetTitle("Mapped channel");
   rms->GetYaxis()->SetTitle("RMS(V_{baseline}),[mV]");
   rms->SetMarkerStyle(6);
   rms->SetMarkerColor(kBlue+3);
   rms->SetMarkerSize(2);
   rms->Draw();
   r->SaveAs(Form("OLD-RMS-%s-%s.png",this_fe.c_str(),pname.c_str()));
   r->SaveAs(Form("OLD-RMS-%s-%s.pdf",this_fe.c_str(),pname.c_str()));
   delete r;
 
}

std::string nsw::ThresholdPlotter::changeFeName(std::string inputstr){

   std::string outstr;
   for(auto &c: inputstr){
      if(c=='/'){
         outstr+="_";
      }else{
         outstr+=c;
      }
   }
   return outstr;

}


