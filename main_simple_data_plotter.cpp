//---------- root + std + boost libs-------------------
#include <fstream>
#include <sstream>
#include <iostream>
#include <TString.h>
#include <string>
#include <vector>
#include <chrono>
#include <ctime>
#include "NSWCalibDataPlotter/ThresholdPlotter.h"
#include "NSWCalibDataPlotter/TreeAssembler.h"
#include "NSWCalibDataPlotter/PulseCalib.h"
#include "NSWCalibDataPlotter/BigThresholds.h"

using namespace std;

//===============================================================================
//#############################  M A I N #######################################
//===============================================================================

#ifndef __CINT__
int main(int ac, const char* av[]){

//  auto start = std::chrono::high_resolution_clock::now();
//-------- terminal value input comparison ------------------------------

  bool scale = false;
  bool vs = false;
  bool bb5 = false;
  bool baseline = false;
  bool threshold = false;  
  bool upd_tree = true;
  bool pulser = false;
  bool sca = false;
  bool l1 = false;
  bool pdo = false;
  bool tdo = false;
  bool BIG = false;
  bool OLD = false;
  bool notit = false;

  int samples = -1;
  int side = -1;
  int sect = -1;

  std::string name;
  std::string this_str;
  std::string data_path;
  std::string archive_path;
  std::string root_file;
  std::string json_config;

  printf("WELCOME COMRADE, LETS PLOT THINGS:\n");
  for(int i_arg=1;i_arg<ac;i_arg++){
        if(strcmp(av[i_arg], "-n")==0){
            name = std::string(av[i_arg+1]);
            i_arg++;
            printf("Plot names/root files will have will have suffix- [%s]\n",name.c_str());
        }
        if(strcmp(av[i_arg], "-a")==0){
            archive_path = std::string(av[i_arg+1]);
            i_arg++;
            printf("ARCHIVATION will be done here: [%s]\n",archive_path.c_str());
        }
        if(strcmp(av[i_arg], "-f")==0){
           this_str = std::string(av[i_arg+1]);
           i_arg++;
           printf("Looking for board names containing-> - [%s]\n",this_str.c_str());
        }
        if(strcmp(av[i_arg], "-d")==0){
           data_path = std::string(av[i_arg+1]);
           i_arg++;
           printf("Directory containing the data -> - [%s]\n",data_path.c_str());
        }
        if(strcmp(av[i_arg], "-j")==0){
           json_config = std::string(av[i_arg+1]);
           i_arg++;
           printf("Configuration file-> - [%s]\n",json_config.c_str());
        }
        if(strcmp(av[i_arg], "-R")==0){
           root_file = std::string(av[i_arg+1]);
           i_arg++;
           printf("Selected PDO/TDO calib data root file -> - [%s]\n",root_file.c_str());
        }
        if(strcmp(av[i_arg], "-S")==0){
           sect = std::atoi(av[i_arg+1]);
           i_arg++;
           printf("Plotting for sector -> - [%i]\n",sect);
        }
        if(strcmp(av[i_arg], "-W")==0){
           side = std::atoi(av[i_arg+1]);
           i_arg++;
           printf("Plotting for side -> - [%i]\n",side);
        }
        //------------- GENERAL FLAGS  --------------------
        if(strcmp(av[i_arg], "--l1")==0){
           l1 = true;
           printf("Drawing L1 data calibration results -> [DA]\n");
        }
        if(strcmp(av[i_arg], "--pdo")==0){
           pdo = true;
           printf("Calibrating PDO -> [DA]\n");
        }
        if(strcmp(av[i_arg], "--tdo")==0){
           tdo = true;
           printf("Calibrating TDO -> [DA]\n");
        }
        if(strcmp(av[i_arg], "--sca")==0){
           sca = true;
           printf("Drawing SCA calibration results -> [DA]\n");
        }
        //--------------------------------------------------------------------------
        if(strcmp(av[i_arg], "--big")==0){
           BIG = true;
           printf("Drawing layer wide baselines and thresholds -> [DA]\n");
        }
        //---------------------------------------------------------------------
        if(strcmp(av[i_arg], "--old")==0){
           OLD = true;
           printf("Drawing OLD BASELINES -> [DA]\n");
        }
        if(strcmp(av[i_arg], "-s")==0){
           samples = std::atoi(av[i_arg+1]);
           i_arg++;
           printf("Plotting for #samples -> - [%i]\n",samples);
        }
        //--------------------------------------------------------------------------------
        if(strcmp(av[i_arg], "--bl")==0){
           baseline = true;
           printf("Drawing baselines -> [DA]\n");
        }
        if(strcmp(av[i_arg], "--thr")==0){
           threshold = true;
           printf("Drawing thresholds? -> [DA]\n");
        }
        if(strcmp(av[i_arg], "--pulser")==0){
           pulser = true;
           printf("Drawing Pulser DAC -> [DA]\n");
        }
        if(strcmp(av[i_arg], "--scale")==0){
           scale = true;
           printf("Drawing on the full mV scale -> [DA]\n");
        }
        if(strcmp(av[i_arg], "--notitle")==0){
           notit = true;
           printf("Ommitting plot titles, printing in better resolution -> [DA]\n");
        }
        if(strcmp(av[i_arg], "-h")==0){
          std::cout<<"HOW TO:"
                   <<"\nCommon params: -n (pic/file prefix)"
                   <<"\n               -a (dir to put archive) can ignore"
                   <<"\n               -f ((front-ends to check) for SCA data \"MMFE8\"_L#P#_"
                   <<"\n                                         for L1  L_RR_V (layer,radius,vmm)"
                   <<"\nBaselines/thresholds need: "
                   <<"\n               --sca, --bl, --thr, (optional --scale, --notitle)"
                   <<"\n               -d (path to the calibration input file directory"
                   <<"\nPDO/TDO need:"
                   <<"\n               --l1, --pdo, --tdo (these are also deduced by the script)"
                   <<"\n               -R (calibration root file)"
                   <<"\n               --pulser for the PDO calib + [-d] option for the pulser files"
                   <<"\nOld baselines:"
                   <<"\n               --old and -d, -n, -s (samples)"
                   <<"\nBIG baselines/thresholds:"
                   <<"\n               --big and -R, -n"
                   << std::endl; 
          exit(0);
        }
      
  }//input argument for loop end!

//--------------------------------------------------------------------
auto start = std::chrono::high_resolution_clock::now();

std::cout<<"A"<<std::endl;
if(l1 && root_file.length()==0){
   std::cout<<"ROOT file was not specified!"<<std::endl;
   exit(1);
}

if(archive_path.length()==0){
   archive_path="plotter_output/";
}

std::cout<<"A"<<std::endl;
//////////////////////////
nsw::ThresholdPlotter tplt;
nsw::TreeAssembler tass;
nsw::PulseCalib pc;
/////////////////////////////
if(OLD){
   tplt.PlotOldBaseline(data_path, name, samples);
   exit(1);
}

std::cout<<"B"<<std::endl;
if(sca==true){
  if(upd_tree == true)
  {
    if(baseline == true){tass.fill_bl(data_path,archive_path);}    
    tass.fill_thre(data_path,archive_path);
    tass.fill_ntuple(data_path,archive_path);
  }  
  if(baseline == false and threshold == false){
     std::cout<<"Please specify either --bl (for baselines) or --thr (for thresholds)"
              <<std::endl;
     exit(1);
  }
  tplt.ThrCalib_plot(data_path, 
                     archive_path, 
                     json_config,
                     name,
                     this_str,
                     baseline,
                     threshold,
                     scale,
                     notit);
}else if(l1==true){
  if(pulser==true){
     if(data_path.length()<1){
        std::cout<<"path to the Pulser DAC data was not provided!"<<std::endl;
        exit(1);
     }
     tass.fill_pulser_dac(data_path, archive_path);
  }    
  std::cout<<"User needs PDO/TDO plots, move Roach!"<<std::endl;
  std::string pdotype = "PDOCalib";  
  std::string tdotype = "TDOCalib";  
  std::size_t found_pdo = root_file.find(pdotype);
  std::size_t found_tdo = root_file.find(tdotype);
std::cout<<"C"<<std::endl;
  if(found_pdo!=std::string::npos){
    pdo=true;
    std::cout<<"Found PDO tag!"<<std::endl;
  }
  else if(found_tdo!=std::string::npos){
    tdo=true;
    std::cout<<"Found TDO tag!"<<std::endl;
  }
  else{
    std::cout<<"Chosen root file does not have PDO/TDO calibration tag!"
             <<std::endl;
  }
  pc.PulseCalib_plot(pdo, 
                     tdo, 
                this_str, 
                    name, 
               root_file,
            archive_path);

std::cout<<"D"<<std::endl;
}else if(BIG){
   
   nsw::BigThresholds BT;

   if(threshold){
      BT.runFullSector(root_file,name,this_str,scale); 
   }else if(baseline){
      BT.baselineFullSector(root_file,name); 
   }else{
      std::cout<<"declare: --thr or --bl"<<std::endl;
   }

}else{std::cout<<"ERROR: no suitable option was found"<<std::endl;}

/////////////// clock stops now////////////////////////////////
 auto finish = std::chrono::high_resolution_clock::now();
 std::chrono::duration<double> elapsed = finish - start;
 std::cout << "\n Elapsed time: " << elapsed.count() 
           << " s\n"<<"\t\t "<<(elapsed.count())/60<<"[min]"<<std::endl;

 return 0;

}
#endif
