//---------- root + std + boost libs-------------------
#include <fstream>
#include "Riostream.h"
#include <TF1.h>
#include <TH2D.h>
#include <TH1F.h>
#include <sstream>
#include <iostream>
#include <TDatime.h> //tstamp
#include "TROOT.h"
#include "TRint.h"
#include <TString.h>
#include <TCanvas.h>
#include <cmath>
#include <TMath.h>
#include <TFile.h>
#include <TMultiGraph.h>
#include <TGraph.h>
#include <TAxis.h>
#include <TGaxis.h>
#include <TText.h>
#include <string>
#include <vector>
#include <dirent.h>
#include <sys/types.h>
#include <algorithm>
#include <chrono>
#include <TLegend.h>
#include <stdio.h>
#include <stdlib.h>
#include <TTree.h>
#include <map>
#include <TStyle.h>
#include <ctime>
#include <iomanip>
#include <TSystem.h>
#include <tuple>

#include "TSpectrum.h"
#include "TVirtualFitter.h"
#include "TPolyMarker.h"
#include "TProfile.h"

#include "boost/property_tree/json_parser.hpp"
#include "boost/foreach.hpp"

using namespace std;

std::string add_timestamp(std::string in_string){
	
	time_t rawtime;
	struct tm * timeinfo;
	char buffer[80];
	time(&rawtime);
	timeinfo = localtime(&rawtime);
	strftime(buffer,sizeof(buffer),"%d-%m-%Y_%H-%M-%S",timeinfo);
	std::string str(buffer);
	
	std::cout<<"timestamp test:"<<str<<std::endl;
	std::string out_string = in_string+str;	
	
	return out_string;
}

//================================================================================
//					FILLING PULSER DAC TREE
//================================================================================

void fill_pulser_dac(std::string io_config_path){

	namespace pt = boost::property_tree;
	pt::ptree input_data;
	pt::read_json(io_config_path, input_data);

//============ reading threshold file directory ====================================
	string path = input_data.get<std::string>("tp_data_output2");
	string path_archive = input_data.get<std::string>("archive");
////------------ reading whole directory -----------------------------------	
	vector<std::string> pdac_files;
	
	DIR *t_dir=NULL;
	struct dirent *t_ent=NULL;
	t_dir = opendir(path.c_str());	
	printf("\nreading baseline file directory\n");
	std::string s = ".";	
	if(t_dir!=NULL)
	{
		while(t_ent=readdir(t_dir))
		{
			std::string file_n = t_ent->d_name;
			if(file_n.at(0)!=s){pdac_files.push_back(t_ent->d_name);}
		}
	}

//======= tonn of checks ========================
	for(int k=0;k<pdac_files.size();k++)
	{
		printf("\n %s",pdac_files[k].c_str());
	}	

	std::sort(pdac_files.begin(),pdac_files.end());
	cout<<"\n sorted file vector \n"<<endl;	
	for(int k=0;k<pdac_files.size();k++)
	{
		printf("\n %s",pdac_files[k].c_str());
	}	
	cout <<"\n"<<endl;

	for(int k=0;k<pdac_files.size();k++)
	{
		printf("\n %s",pdac_files[k].c_str());
	}	
//=================================================
	int n_boards = pdac_files.size();
      
	printf("\ndirectory contains %i board output thr_files\n",n_boards);

//============= writing common threshold file =================================
	
 	std::ifstream ith_thr;
	
	std::ofstream new_thr;//, arch_thr;
	
	std::string tline, nline;
	string pulser_file = "common_txt/PulserDac.txt";
//	string pulser_file_archive = path_archive+"TextFiles/"+add_timestamp("PulserDac_")+".txt";
//	std::cout<<"PULSER DAC TEXT FILE ARCHIVATION TO:{"<<pulser_file_archive<<"}!"<<std::endl;
	//new_txt.open("calib_tree_input.txt");
	new_thr.open(pulser_file);
//	arch_thr.open(pulser_file_archive);

	for(int i_board=0; i_board <n_boards; i_board++)
	{
		ith_thr.open(path+pdac_files[i_board]);

		std::cout<<"\nwritting boad bl_files to single file\n"<<std::endl;
		while(std::getline(ith_thr, tline,'\n'))
		{
	//		std::cout<<wline<<std::endl;
			new_thr<<tline<<std::endl;
//			arch_thr<<tline<<std::endl;
		}
		
		std::cout<<"\nWritten "<<i_board<<" file to common tree txt\n"<<std::endl;

		ith_thr.close();
  }
	new_thr.close();
//	arch_thr.close();


//========================================================================================

  FILE *in_chan = fopen(Form("%s",pulser_file.c_str()),"r");

  float sam_mv; //thrmin,thrmax;
	int dac, vmm, sam;

  string fname_str;
	char fname[10];

  TFile *f = new TFile("root_files/pulser_dac_tree.root","RECREATE");
  TTree *t = new TTree("pdac_tree","Tree of pulser dac samples");

	cout<<"\n declared variables trees and root file\n"<<endl;

	 //---------- channel parameters -----------------------------
	 t->Branch("board",&fname_str);
	 t->Branch("vmm", &vmm, "vmm/I");
	 t->Branch("DAC", &dac, "DAC/I");
	 t->Branch("sample", &sam, "sample/I");
	 t->Branch("sample_mV", &sam_mv, "sample_mV/F");

	 char line_c[200];
	 int line_nr=0;
	 
	 cout<<"\ndeclared branches\n"<<endl;

	 while(fgets(line_c,400,in_chan))
		{
		//	cout<<"scanning "<<line_nr<< "line\n"<<endl;
			sscanf(&line_c[0],"%s %i %i %i %f",
			&fname, &vmm, &dac, &sam, &sam_mv);
      fname_str=fname;
			t->Fill();
			line_nr++;
    //  cout<< fname_str;
		}
	t->Write();
	cout<<"\nwritten the branches\n"<<endl;

  f->Write();	
	
	printf("\nfile have = %i lines\n",line_nr);

  fclose(in_chan);

	string arch_puls_root = add_timestamp("pulser_dac_tree_");
	string root_arch_path = path_archive+"RootFiles/"+arch_puls_root+".root";

	std::cout<<"PULSER DAC ROOT FILE ARCHIVATION TO:{"<<root_arch_path<<"}!"<<std::endl;
	gSystem->CopyFile("root_files/pulser_dac_tree.root", root_arch_path.c_str());

  printf("closed in_chan\n"); 
	std::cout<<"=====================================================================\n"<<std::endl;
	std::cout<<"================== PULSER DAC TREE UPDATED ! ========================\n"<<std::endl;
	std::cout<<"=====================================================================\n"<<std::endl;

}

//================================================================================
//					FILLING BASELINE TREE
//================================================================================

void fill_bl(std::string io_config_path){

	namespace pt = boost::property_tree;
	pt::ptree input_data;
	pt::read_json(io_config_path, input_data);

//============ reading threshold file directory ====================================
	string path_bl = input_data.get<std::string>("bl_data_output");
	string path_bl_archive = input_data.get<std::string>("archive");
////------------ reading whole directory -----------------------------------	
	vector<std::string> bl_files;
	
	DIR *t_dir=NULL;
	struct dirent *t_ent=NULL;
	t_dir = opendir(path_bl.c_str());	
	printf("\nreading baseline file directory\n");
	std::string s = ".";	
	if(t_dir!=NULL)
	{
		while(t_ent=readdir(t_dir))
		{
			std::string file_n = t_ent->d_name;
			if(file_n.at(0)!=s){bl_files.push_back(t_ent->d_name);}
		}
	}

//======= tonn of checks ========================
	for(int k=0;k<bl_files.size();k++)
	{
		printf("\n %s",bl_files[k].c_str());
	}	

	std::sort(bl_files.begin(),bl_files.end());
	cout<<"\n sorted file vector \n"<<endl;	
	for(int k=0;k<bl_files.size();k++)
	{
		printf("\n %s",bl_files[k].c_str());
	}	
	cout <<"\n"<<endl;

	for(int k=0;k<bl_files.size();k++)
	{
		printf("\n %s",bl_files[k].c_str());
	}	
//=================================================
	int n_boards = bl_files.size();
      
	printf("\ndirectory contains %i board output thr_files\n",n_boards);

//============= writing common threshold file =================================
	
 	std::ifstream ith_thr;
	
	std::ofstream new_thr;//, arch_thr;
	
	std::string tline, nline;
	string bl_file = "common_txt/Baselines.txt";
//	string bl_file_archive = path_bl_archive+"TextFiles/"+add_timestamp("Baselines_")+".txt";
//	std::cout<<"BASELINE TEXT FILE ARCHIVATION TO:{"<<bl_file_archive<<"}!"<<std::endl;
	//new_txt.open("calib_tree_input.txt");
	new_thr.open(bl_file);
//	arch_thr.open(bl_file_archive);

	for(int i_board=0; i_board <n_boards; i_board++)
	{
		ith_thr.open(path_bl+bl_files[i_board]);

		std::cout<<"\nwritting boad bl_files to single file\n"<<std::endl;
		while(std::getline(ith_thr, tline,'\n'))
		{
	//		std::cout<<wline<<std::endl;
			new_thr<<tline<<std::endl;
//			arch_thr<<tline<<std::endl;
		}
		
		std::cout<<"\nWritten "<<i_board<<" file to common tree txt\n"<<std::endl;

		ith_thr.close();
  }
	new_thr.close();
//	arch_thr.close();


//========================================================================================

  FILE *in_chan = fopen(Form("%s",bl_file.c_str()),"r");

  float sam, rms; //thrmin,thrmax;
	int ch, cvmm;

  string fname_str;
	char fname[10];

  TFile *f = new TFile("root_files/baseline_tree.root","RECREATE");
  TTree *t = new TTree("bl_tree","Tree of baseline samples");

	cout<<"\n declared variables trees and root file\n"<<endl;

	 //---------- channel parameters -----------------------------
	 t->Branch("board",&fname_str);
	 t->Branch("cvmm", &cvmm, "cvmm/I");
	 t->Branch("channel_id", &ch, "channel_id/I");
	 t->Branch("sample", &sam, "sample/F");
	 t->Branch("rms", &rms, "rms/F");

	 char line_c[200];
	 int line_nr=0;
	 
	 cout<<"\ndeclared branches\n"<<endl;

	 while(fgets(line_c,400,in_chan))
		{
		//	cout<<"scanning "<<line_nr<< "line\n"<<endl;
			sscanf(&line_c[0],"%s %i %i %f %f",
			&fname, &cvmm, &ch, &sam, &rms);
      fname_str=fname;
			t->Fill();
			line_nr++;
    //  cout<< fname_str;
		}
	t->Write();
	cout<<"\nwritten the branches\n"<<endl;

  f->Write();	
	
	printf("\nfile have = %i lines\n",line_nr);

  fclose(in_chan);

	string arch_bl_root = add_timestamp("baseline_tree_");
	string root_arch_path = path_bl_archive+"RootFiles/"+arch_bl_root+".root";

	std::cout<<"BASELINE ROOT FILE ARCHIVATION TO:{"<<root_arch_path<<"}!"<<std::endl;
	gSystem->CopyFile("root_files/baseline_tree.root", root_arch_path.c_str());

  printf("closed in_chan\n"); 
	std::cout<<"=====================================================================\n"<<std::endl;
	std::cout<<"================== BASELINE TREE UPDATED ============================\n"<<std::endl;
	std::cout<<"=====================================================================\n"<<std::endl;

}

//================================================================================
//					FILLING THRESHOLD TREE
//================================================================================
void fill_thre(std::string io_config_path){

	namespace pt = boost::property_tree;
	pt::ptree input_data;
	pt::read_json(io_config_path, input_data);
	
//============ reading threshold file directory ====================================
	string path_thr = input_data.get<std::string>("thr_data_output");
	string path_arch = input_data.get<std::string>("archive");
////------------ reading whole directory -----------------------------------	
	vector<std::string> thr_files;
	
	DIR *t_dir=NULL;
	struct dirent *t_ent=NULL;
	t_dir = opendir(path_thr.c_str());	
	printf("\nreading calibration file directory\n");
	std::string s = ".";	
	if(t_dir!=NULL)
	{
		while(t_ent=readdir(t_dir))
		{
			std::string file_n = t_ent->d_name;
			if(file_n.at(0)!=s){thr_files.push_back(t_ent->d_name);}
		}
	}

//======= tonn of checks ========================
	for(int k=0;k<thr_files.size();k++)
	{
		printf("\n %s",thr_files[k].c_str());
	}	

	std::sort(thr_files.begin(),thr_files.end());
	cout<<"\n sorted file vector \n"<<endl;	
	for(int k=0;k<thr_files.size();k++)
	{
		printf("\n %s",thr_files[k].c_str());
	}	
	cout <<"\n"<<endl;

	for(int k=0;k<thr_files.size();k++)
	{
		printf("\n %s",thr_files[k].c_str());
	}	
//=================================================
	int n_boards = thr_files.size();
      
	printf("\ndirectory contains %i board output thr_files\n",n_boards);

//============= writing common threshold file =================================
	
 	std::ifstream ith_thr;
	
	std::ofstream new_thr;//, arch_thr;
	
	std::string tline, nline;
	string thr_file = "common_txt/Untrimmed_thresholds.txt";
//	string thr_file_arch = path_arch+"TextFiles/"+add_timestamp("Untrimmed_thresholds_")+".txt";
	//new_txt.open("calib_tree_input.txt");
//	std::cout<<"THRESHOLD TEXT FILE ARCHIVATION TO:{"<<thr_file_arch<<"}!"<<std::endl;
	new_thr.open(thr_file);
//	arch_thr.open(thr_file_arch);

	for(int i_board=0; i_board <n_boards; i_board++)
	{
		ith_thr.open(path_thr+thr_files[i_board]);

		std::cout<<"\nwritting boad thr_files to single file\n"<<std::endl;
		while(std::getline(ith_thr, tline,'\n'))
		{
	//		std::cout<<wline<<std::endl;
			new_thr<<tline<<std::endl;
	//		arch_thr<<tline<<std::endl;
		}
		
		std::cout<<"\nWritten "<<i_board<<" file to calib tree txt\n"<<std::endl;

		ith_thr.close();
  }
	new_thr.close();
//	arch_thr.close();

//========================================================================================

  FILE *in_chan = fopen(Form("%s",thr_file.c_str()),"r");
 
  float thr, thrmin,thrmax;
	int ch, cvmm;

  string fname_str;
	char fname[10];

  TFile *f = new TFile("root_files/threshold_tree.root","RECREATE");
  TTree *t = new TTree("thr_tree","Tree of untrimmed channel thresholds");

	cout<<"\n declared variables trees and root file\n"<<endl;

	 //---------- channel parameters -----------------------------
	 t->Branch("board",&fname_str);
	 t->Branch("cvmm", &cvmm, "cvmm/I");
	 t->Branch("channel_id", &ch, "channel_id/I");
	 t->Branch("channel_thr", &thr, "channel_thr/F");
	 t->Branch("channel_thrmin", &thrmin, "channel_thrmin/I");
	 t->Branch("channel_thrmax", &thrmax, "channel_thrmax/I");

	 char line_c[200];
	 int line_nr=0;
	 
	 cout<<"\ndeclared branches\n"<<endl;

	 while(fgets(line_c,400,in_chan))
		{
	//		cout<<"scanning "<<line_nr<< "line\n"<<endl;
			sscanf(&line_c[0],"%s %i %i %f %i %i",
			&fname, &cvmm, &ch, &thr, &thrmin, &thrmax);
      fname_str=fname;
			t->Fill();
			line_nr++;
    //  cout<< fname_str;
		}
	t->Write();
	cout<<"\nwritten the branches\n"<<endl;

  f->Write();	
	
	printf("\nfile have = %i lines\n",line_nr);

  fclose(in_chan);

	string arch_thr_root = add_timestamp("threshold_tree_");
	string root_arch_path = path_arch+"RootFiles/"+arch_thr_root+".root";

	std::cout<<"THRESHOLD ROOT FILE ARCHIVATION TO:{"<<root_arch_path<<"}!"<<std::endl;
	gSystem->CopyFile("root_files/threshold_tree.root", root_arch_path.c_str());

  printf("closed in_chan\n"); 
	std::cout<<"=====================================================================\n"<<std::endl;
	std::cout<<"================== THRESHOLD TREE UPDATED ===========================\n"<<std::endl;
	std::cout<<"=====================================================================\n"<<std::endl;


}
//===============================================================================================
//						FILLING MAIN CALIBRATION DATA TREE
//===============================================================================================

void fill_ntuple(std::string io_config_path){

	namespace pt = boost::property_tree;
	pt::ptree input_data;
	pt::read_json(io_config_path, input_data);

//============= reading calibration parameter file directory ================
	string path = input_data.get<std::string>("cal_data_output");
	string arch_path = input_data.get<std::string>("archive");
////------------ reading whole directory -----------------------------------	
	vector<std::string> files;
	
	DIR *d_dir=NULL;
	struct dirent *ent=NULL;
	d_dir = opendir(path.c_str());	
	printf("\nreading calibration file directory\n");
	std::string s = ".";
	if(d_dir!=NULL)
	{
		while(ent=readdir(d_dir))
		{
			std::string file_n = ent->d_name;
			if(file_n.at(0)!= s ){files.push_back(ent->d_name);}
		}
	}
//======= tonn of checks ========================
	for(int k=0;k<files.size();k++)
	{
		printf("\n %s",files[k].c_str());
	}	

	std::sort(files.begin(),files.end());
	cout<<"\n sorted file vector \n"<<endl;	
	for(int k=0;k<files.size();k++)
	{
		printf("\n %s",files[k].c_str());
	}	
	cout <<"\n"<<endl;
	
	for(int k=0;k<files.size();k++)
	{
		printf("\n %s",files[k].c_str());
	}	
//=================================================
	int n_boards = files.size();
      
	printf("\ndirectory contains %i board output files\n",n_boards);

//============== part that writes single txt for all boards ======================= 
	
 	std::ifstream ith_txt;
	
	std::ofstream new_txt;//, arch_txt;
	
	std::string cline, wline;
	std::string in_file="common_txt/Calib_data.txt";
//	string cal_file_archive = arch_path+"TextFiles/"+add_timestamp("Calib_data_")+".txt";
//	std::cout<<"CALIBRATION DATA TEXT FILE ARCHIVATION TO:\n{"<<cal_file_archive<<"}!"<<std::endl;
	//new_txt.open("calib_tree_input.txt");
	new_txt.open(in_file);
//	arch_txt.open(cal_file_archive);

	for(int i_board=0; i_board <n_boards; i_board++)
	{
		ith_txt.open(path+files[i_board]);

		std::cout<<"\nwritting boad files to single file\n"<<std::endl;
		while(std::getline(ith_txt, wline,'\n'))
		{
	//		std::cout<<wline<<std::endl;
			new_txt<<wline<<std::endl;
//			arch_txt<<wline<<std::endl;
		}
		
		std::cout<<"\nWritten "<<i_board<<" file to calib tree txt\n"<<std::endl;

		ith_txt.close();
  }
	new_txt.close();
//	arch_txt.close();
//========================================================================================
//===================== FILLING THE GODDAMN TREE =========================================
//========================================================================================
  FILE *in_chan = fopen(in_file.c_str(),"r");

  float med, rms, eff_th, slope, eff_w_trim, ch_trim_thr;
	int ch, cvmm, trim, ch_mask;

  string fname_str;
	char fname[10];
	float vmm_med, vmm_rms, vmm_trim_med, vmm_eff_th, vmm_thdac_slope, vmm_thdac_offset;
	int gvmm, vmm_thdac;	 
 //int nlines = 0;

  TFile *f = new TFile("root_files/data_tree.root","RECREATE");
  TTree *t = new TTree("par_tree","Tree of calibration run output parameters");

	cout<<"\n declared variables trees and root file\n"<<endl;

	 //---------- channel parameters -----------------------------
	 t->Branch("board",&fname_str);
	 t->Branch("cvmm", &cvmm, "cvmm/I");
	 t->Branch("channel_id", &ch, "channel_id/I");
	 t->Branch("ch_baseline_med", &med, "ch_baseline_med/F");
	 t->Branch("ch_baseline_rms", &rms, "ch_baseline_rms/F");
	 t->Branch("ch_eff_threshold", &eff_th, "ch_eff_threshol/F");
	 t->Branch("ch_eff_th_slope", &slope, "ch_eff_th_slope/F");
	 t->Branch("ch_best_trim", &trim, "ch_best_trim/I");
	 t->Branch("ch_eff_thr_w_best_trim", &eff_w_trim, "ch_eff_thr_w_best_trim/F");
	 t->Branch("ch_trimmed_thr", &ch_trim_thr, "ch_trimmed_thr/F");
	 t->Branch("ch_mask", &ch_mask, "ch_mask/I");
	 //---------- chip global parameters --------------------------
	 t->Branch("vmm_median", &vmm_med, "vmm_median/F");
	 t->Branch("vmm_median_rms", &vmm_rms, "vmm_median_rms/F");
	 t->Branch("vmm_median_trim_mid", &vmm_trim_med, "vmm_median_trim_mid/F");
	 t->Branch("vmm_eff_thr", &vmm_eff_th, "vmm_eff_thr/F");
	 t->Branch("vmm_thdac", &vmm_thdac, "vmm_thdac/I");
	 t->Branch("vmm_thdac_slope", &vmm_thdac_slope, "vmm_thdac_slope/F");
	 t->Branch("vmm_thdac_offset", &vmm_thdac_offset, "vmm_thdac_offset/F");
	 //------------ threshold branch --------------------
//	 t->Branch("ch_threshold_ut", &ch_thr_ut, "ch_threshold_ut/F")

	 char line_c[400];
	 int line_nr=0;
	 
	 cout<<"\ndeclared branches\n"<<endl;

	 while(fgets(line_c,400,in_chan))
		{
	//		cout<<"scanning "<<line_nr<< "line\n"<<endl;
			sscanf(&line_c[0],"%s %i %i %f %f %f %f %f %f %f %f %i %i %f %f %f %f %i",
			&fname, &cvmm, &ch, &med, &rms, &eff_th, &slope,  
			&vmm_med, &vmm_rms, &vmm_trim_med, &vmm_eff_th, &vmm_thdac, &trim, &ch_trim_thr, &eff_w_trim, &vmm_thdac_slope, &vmm_thdac_offset, &ch_mask);
      fname_str=fname;
			t->Fill();
			line_nr++;
    //  cout<< fname_str;
		}

	t->Write();
	cout<<"\nwritten the branches\n"<<endl;

  f->Write();	
	
	printf("\nfile have = %i lines\n",line_nr);
  fclose(in_chan);

	string arch_cal_root = add_timestamp("data_tree_");
	string root_arch_path = arch_path+"RootFiles/"+arch_cal_root+".root";

	std::cout<<"CALIBRATION DATA ROOT FILE ARCHIVATION TO:\n{"<<root_arch_path<<"}!"<<std::endl;
	gSystem->CopyFile("root_files/data_tree.root", root_arch_path.c_str());


  printf("closed in_chan\n"); 
	std::cout<<"=====================================================================\n"<<std::endl;
	std::cout<<"================== CALIBRATION DATA TREE UPDATED ====================\n"<<std::endl;
	std::cout<<"=====================================================================\n"<<std::endl;


}

//Place holder for peak searching and fitting  
void find_pdo_peaks(map<std::string,TH1D*> &hist, char* name, std::vector<int> &means){

		Int_t i;
		const Int_t nbins = 1030;
		Double_t xmin     = 0;
	  Double_t xmax     = nbins;
		Double_t source[nbins], dest[nbins];

	  TH1F *bg  = new TH1F("bg","bg",nbins,xmin,xmax);

 	  hist[name]->Draw();

		TSpectrum *s = new TSpectrum();
   			
		double threshold = 0.1;
		double width = 6;	

		Int_t nfound = s->Search(hist[name],width,"",threshold);

    //if(cd > 0){cout<<"\n determining peak positions"<<endl;}
    cout<<"\n determining peak positions"<<endl;
    Int_t bin;
    Double_t a;
    Double_t fPositionX[100];
    Double_t fPositionY[100];
    Double_t *xpeaks = s->GetPositionX();
    for (i = 0; i < nfound; i++) {
       a=xpeaks[i];
       bin = 1 + Int_t(a + 0.5);
       fPositionX[i] = hist[name]->GetBinCenter(bin);
       fPositionY[i] = hist[name]->GetBinContent(bin);
    }
  
    TPolyMarker * pm = (TPolyMarker*)hist[name]->GetListOfFunctions()->FindObject("TPolyMarker");
    if (pm) {
       hist[name]->GetListOfFunctions()->Remove(pm);
       delete pm;
    }
  
    pm = new TPolyMarker(nfound, fPositionX, fPositionY);
    hist[name]->GetListOfFunctions()->Add(pm);
    pm->SetMarkerStyle(23);
    pm->SetMarkerColor(kRed);
    pm->SetMarkerSize(1.3);
    for (i = 0; i < nbins; i++) {
      bg->SetBinContent(i + 1,dest[i]);
    }
    bg->SetLineColor(kRed);
    bg->Draw("SAME");
    printf("Found %d candidate peaks\n",nfound);
    vector<std::pair<int,int>> ranges;
    for( i=0;i<nfound;i++){
      printf("posx= %f, posy= %f\n",fPositionX[i], fPositionY[i]);
      float xpos = fPositionX[i]-0.5;
      float ypos = fPositionY[i];
      int bin_count = 0;
      float prev_bin_val_r = ypos;
      float prev_bin_val_l = ypos;
      int next_bin_val_r = 0;
      int next_bin_val_l = 0;
      int next_bin_l, next_bin_r;
      //for(int b=1; b<50; b++)
			int b=20;
			next_bin_r = xpos + b;
      next_bin_l = xpos - b;
      ranges.push_back(std::make_pair(next_bin_l, next_bin_r));
    }

		  float mean;//, sig;

			std::cout<<"\n>>>Ranges = "<<ranges.size()<<" <<<"<<std::endl;
		  for( auto & r:ranges)
		  {
		    TF1 *ga = new TF1("ga","gaus",r.first,r.second);
//		    Na->Fit("ga","R");
		    hist[name]->Fit("ga","R","Q");
		    means.push_back(ga->GetParameter(1));
		//    sigmas.push_back(ga->GetParameter(2));
				std::cout<<" [mean = "<<ga->GetParameter(1)<<"][sigma = "<<ga->GetParameter(2)<<"]"<<std::endl;
		  }

		delete bg;

}
//

//==================================================================================================================
//						Plot initialization
//==================================================================================================================
//void initH2(std::map<std::string,TH2D*> &h2, char* name, int xbins, int xlow, int xhigh, int ybins, int ylow, int yhigh, bool scale,){
//   //h2[name] = new TH2D(name,name,xbins,xlow,xhigh,ybins,yhigh,ylow); 
//   if(scale==true){h2[name] = new TH2D(name,name,512,0,512,1200,0,1200);}
//   else{h2[name] = new TH2D(name,name,512,0,512,600,0,600);} 
//}
void initH1_pdo(std::map<std::string,TH1D*> &h1, char* name, bool scale, string feb){
 	 	 h1[name] = new TH1D(name,name,1030,0,1030);
}

void initH1_tdo(std::map<std::string,TH1D*> &h1, char* name, bool scale, string feb){
 	 	 h1[name] = new TH1D(name,name,250,0,250);
}

TH1D* getH1_pdo(char* name, std::map<std::string,TH1D*> &h1 ){
  if (h1.count(name)==0){
    std::cout << "cannot find this histogram: "<< name << std::endl;
    return 0;
  }
  else{
    return h1[name];
  }
}

TH1D* getH1_tdo(char* name, std::map<std::string,TH1D*> &h1 ){
  if (h1.count(name)==0){
    std::cout << "cannot find this histogram: "<< name << std::endl;
    return 0;
  }
  else{
    return h1[name];
  }
}

void initH2_DetOut_trgCalibKey(std::map<std::string,TH2D*> &h2, char* name, string feb, double n_events){
 	   h2[name] = new TH2D(name,name,1200,0,1200,1250,0,1250); 
}

TH2D* getH2_DetOut_trgCalibKey(char* name, std::map<std::string,TH2D*> &h2 ){
  if (h2.count(name)==0){
    std::cout << "cannot find histogram with name: "<< name << std::endl;
    return 0;
  }
  else{
    return h2[name];
  }
}

double fit_custom_pdo(double* x_val ,double* par){
///////// spizzheno s githaba, iz skripta analiza dannyh testbima //////////////////
//	double x = x_val[0];
//	
//	double c0 = par[0];
//	double A2 = par[1];
//	double t02 = par[2];
//	double d21 = par[3];
//
//	if(x > t02){
//		return c0;
//	}
//	if(x > t02+d21){
//		return c0 + A2*(x-t02)*(x-t02);
//	}
//	return c0 + A2*d21*(2.*x - d21 - 2*t02);	
///////////////////////////////////////////////////////////////////////////////////////////
///////// test Patricks` suggestion later ////////////////////////////////////////////////

	if(x_val[0]<par[0]){
		return par[1]*x_val[0]*par[2];
	}
	else{
		return par[1]*par[0]+par[2];
	}
/////////////////////////////////////////////////////////////////////////////////////////////
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void initH2(std::map<std::string,TH2D*> &h2, char* name, bool scale, string feb){
//	std::string feb = name;
	 bool mmfe = feb.find("MMFE")!=std::string::npos;
	 bool pfeb = feb.find("PFEB")!=std::string::npos;
	 bool sfeb = feb.find("SFEB")!=std::string::npos;
	 if(mmfe==true){
 	 	 if(scale==true){h2[name] = new TH2D(name,name,512,0,512,1200,0,1200);}
 	   else{h2[name] = new TH2D(name,name,512,0,512,600,0,600);} 
		 std::cout<<"Found MMFE"<<std::endl;
	 }
	 if(pfeb==true){
 	 	 if(scale==true){h2[name] = new TH2D(name,name,192,0,192,1200,0,1200);}
 	   else{h2[name] = new TH2D(name,name,192,0,192,600,0,600);} 
	 }
	 if(sfeb==true){
 	 	 if(scale==true){h2[name] = new TH2D(name,name,512,0,512,1200,0,1200);}
 	   else{h2[name] = new TH2D(name,name,512,0,512,600,0,600);} 
	 }
//   else{
//		 std::cout<<"Do we actually have this kind of boards << "<<feb<<" >> ?"<<std::endl;
//	 }
}

TH2D* getH2(char* name, std::map<std::string,TH2D*> &h2 ){
  if (h2.count(name)==0){
    std::cout << "cannot find h2: "<< name << std::endl;
    return 0;
  }
  else{
    return h2[name];
  }
}
void initH2_noise(std::map<std::string,TH2D*> &h2, char* name, bool scale, string feb){

 	 bool mmfe = feb.find("MMFE")!=std::string::npos;
	 bool pfeb = feb.find("PFEB")!=std::string::npos;
	 bool sfeb = feb.find("SFEB")!=std::string::npos;
	 if(mmfe==true){
 	 	 if(scale==false){h2[name] = new TH2D(name,name,512,0,512,8000,0,8000);}
 	   else{h2[name] = new TH2D(name,name,512,0,512,30000,0,30000);} 
	 }
	 if(pfeb==true){
 	 	 if(scale==false){h2[name] = new TH2D(name,name,192,0,192,8000,0,8000);}
 	   else{h2[name] = new TH2D(name,name,192,0,192,30000,0,30000);} 
	 }
	 if(sfeb==true){
 	 	 if(scale==false){h2[name] = new TH2D(name,name,512,0,512,8000,0,8000);}
 	   else{h2[name] = new TH2D(name,name,512,0,512,30000,0,30000);} 
	 }
//	 else{
//		 std::cout<<"Do we actually have this kind of boards << "<<feb<<" >> ?"<<std::endl;
//	 }
}

void initH2_noise_over(std::map<std::string,TH2D*> &h2, char* name, bool scale, string feb){

 	 bool mmfe = feb.find("MMFE")!=std::string::npos;
	 bool pfeb = feb.find("PFEB")!=std::string::npos;
	 bool sfeb = feb.find("SFEB")!=std::string::npos;
	 if(mmfe==true){
 	 	 if(scale==false){h2[name] = new TH2D(name,name,512,0,512,8100,0,8100);}
 	   else{h2[name] = new TH2D(name,name,512,0,512,30100,0,30100);} 
	 }
	 if(pfeb==true){
 	 	 if(scale==false){h2[name] = new TH2D(name,name,192,0,192,8100,0,8100);}
 	   else{h2[name] = new TH2D(name,name,192,0,192,30100,0,30100);} 
	 }
	 if(sfeb==true){
 	 	 if(scale==false){h2[name] = new TH2D(name,name,512,0,512,8100,0,8100);}
 	   else{h2[name] = new TH2D(name,name,512,0,512,30100,0,30100);} 
	 }
//	 else{
//		 std::cout<<"Do we actually have this kind of boards << "<<feb<<" >> ?"<<std::endl;
//	 }
}
//////////////// for layer histogram init ////////////////////////////////////////////
void initH2_layer(std::map<std::string,TH2D*> &h2, char* name, bool scale){
   h2[name] = new TH2D(name,name,8192,0,8192,10000,0,10000);
}
////////////////////////////////////////////////////////////////////////////////////
//===============for sampled baselines======================
void initH2F(std::map<std::string,TH2F*> &h2f, char* name, bool scale, string feb){

	 bool mmfe = feb.find("MMFE")!=std::string::npos;
	 bool pfeb = feb.find("PFEB")!=std::string::npos;
	 bool sfeb = feb.find("SFEB")!=std::string::npos;
	 if(mmfe==true){
 	 	 if(scale==false){h2f[name] = new TH2F(name,name,512,0,512,300,50,350);}
 	   else{h2f[name] = new TH2F(name,name,512,0,512,1200,0,1200);} 
	 }
	 if(pfeb==true){
 	 	 if(scale==false){h2f[name] = new TH2F(name,name,192,0,192,300,50,350);}
 	   else{h2f[name] = new TH2F(name,name,192,0,192,1200,0,1200);} 
	 }
	 if(sfeb==true){
 	 	 if(scale==false){h2f[name] = new TH2F(name,name,512,0,512,300,50,350);}
 	   else{h2f[name] = new TH2F(name,name,512,0,512,1200,0,1200);} 
	 }
	 else{
		 std::cout<<"Do we actually have this kind of boards << "<<feb<<" >> ?"<<std::endl;
	 }
}

void initH1F_dac(std::map<std::string,TH1F*> &h1f, char* name){
 	 	 h1f[name] = new TH1F(name,name,2300,0,2300);
}

TH1F* getH1F(char* name, std::map<std::string,TH1F*> &h1f ){
  if (h1f.count(name)==0){
    std::cout << "cannot find h1f: "<< name << std::endl;
    return 0;
  }
  else{
    return h1f[name];
  }
}

TH2F* getH2F(char* name, std::map<std::string,TH2F*> &h2f ){
  if (h2f.count(name)==0){
    std::cout << "cannot find h2f: "<< name << std::endl;
    return 0;
  }
  else{
    return h2f[name];
  }
}


//=======================================
//#######################################################################################################################################
//############################### MAIN PLOTTING OPERATIONS ##############################################################################
//#######################################################################################################################################
void plot_pdo(bool pdo, bool tdo, bool scale, string pic_name, string this_root_file)
{

	TFile *f3 = new TFile(this_root_file.c_str());//<--this hsould be input argument!
	//TFile *f3 = new TFile("/afs/cern.ch/user/v/vplesano/Documents/data_test.1586616254._.daq.RAW._lb0000._NSW_SWROD._0001.simple.root");//<--this hsould be input argument!
	TTree *data_tree = (TTree*)f3->Get("nsw");
	std::cout<<data_tree<<std::endl;
//	//-----------------------------------------------
	TFile *f_dac = new TFile("root_files/pulser_dac_tree.root");
	TTree *pdac_tree=(TTree* )f_dac->Get("pdac_tree");
	std::cout<< pdac_tree<<std::endl;
//--------------------------------------------------------------------------------

std::map<std::string,TH1D*> hpdo;
std::map<std::string,TH1D*> htdo;
std::map<std::string,TH1F*> hpdac;

std::map<std::string,TCanvas*> c;
std::map<std::string,TCanvas*> c_dac;
std::map<std::string,TCanvas*> c_tdo;
std::map<std::string,TCanvas*> c_pdac;
////============ patom dodelaju .... =================
//std::vector<int> fnames = {};
//vector<unsigned int> encoded_name;
//	std::cout<< "NUKA" <<std::endl;
//pdac_tree->SetBranchAddress("linkId",&encoded_name);
//	std::cout<< "HOBA" <<std::endl;
//int nrevents = pdac_tree->GetEntries();
//	std::cout<< "JOBA" <<std::endl;
//for(int i=0; i<nrevents; i++){
//	std::cout<< "NANAH!" <<std::endl;
//	pdac_tree->GetEntry(i);
//	for(auto &n : encoded_name){
//		std::cout<< n <<std::endl;
//	}	
//	std::cout<< "HAVAI "<<i<<" SUKA!" <<std::endl;
//}
////==================================
//std::string f_name;
//std::vector<unsigned int> FEBs = {568320,568336,568352};
std::vector<unsigned int> FEBs = {568320};
//std::vector<unsigned int> FEBs = {568320,568336,568352,568368,568384,568400,568416,568417};
//std::vector<unsigned int> FEBs = {568336,568352,568368,568384,568416,568417};
//std::vector<unsigned int> FEBs = {568352,568384,568416,568417};
//std::vector<unsigned int> FEBs = {568384,568417};
//std::vector<std::string> strFEBs = {/*"MMFE8-0000",*/"MMFE8-0001","MMFE8-0002","MMFE8-0003","MMFE8-0004","MMFE8-0005"};
//std::vector<std::string> strFEBs = {"MMFE8-0002","MMFE8-0003","MMFE8-0004"};
//std::vector<std::string> strFEBs = {"MMFE8-0000","MMFE8-0001"};// have only two good mmfe pulser dac readings 
std::vector<std::string> strFEBs = {"MMFE8-0002"};
//std::vector<std::string> strFEBs = {"MMFE8-0000","MMFE8-0001","MMFE8-0002","MMFE8-0003","MMFE8-0004","MMFE8-0005","MMFE8-0006","MMFE8-0007"};
//std::vector<unsigned int> FEBs = {568384, 568416};
//std::vector<unsigned int> FEBs = {568352};
//=========================================================================================
long int n_mmfe = FEBs.size();

//std::map<int,std::vector<int>> PDO_peaks;

std::vector<int> vmm_to_include = {0,0,1,0,0,0,0,0};

std::vector<unsigned int> dacs = {200,300,400,500,600,700,800,900,1000};
//std::vector<std::tuple<int,int,int>> dac_to_mV;
std::cout<<"we have: "<<FEBs.size()<<" FEBs to describe"<<std::endl;
//std::sort(FEBs.begin(),FEBs.end());
//cout<<"\n\t!!!!!!!POSHHITAL!!!!!!!!\n"<<endl;
//=========================  define histos for each MMFE8 ====================================

std::vector<unsigned int> v_trgCK = {};
Int_t trgCK;

data_tree->SetBranchAddress("triggerCalibrationKey",&trgCK);

int nrevents = data_tree->GetEntries();

for(int i=0; i<nrevents; i++){
	data_tree->GetEntry(i);
//	std::cout<< trgCK <<std::endl;
	v_trgCK.push_back(trgCK);
}

std::sort(v_trgCK.begin(),v_trgCK.end());

std::cout<<"sorted vector size:"<<v_trgCK.size()<<std::endl;

vector<int> dac;
vector<std::string> s_dac;
vector<int> del;
vector<std::string> s_del;

unsigned int current_trgCK = 0;
unsigned int trgCK_toCheck = 0;

unsigned int trgCK_step = 0;

for(int t=2; t<v_trgCK.size(); t++){
	trgCK_toCheck = v_trgCK[t-1];
	current_trgCK = v_trgCK[t];
	if(current_trgCK == -1){continue;}
	if(t==2){dac.push_back(current_trgCK);}
	//else if(current_trgCK > v_trgCK[t-1]){
	else if(current_trgCK > trgCK_toCheck){
		if(pdo){
			dac.push_back(v_trgCK.at(t));
			s_dac.push_back(std::to_string(v_trgCK.at(t)));
		}
		if(tdo){
			del.push_back(v_trgCK.at(t));
			s_del.push_back(std::to_string(v_trgCK.at(t)));
		}
		trgCK_step = current_trgCK - trgCK_toCheck;
	}
	else{continue;}
}
std::cout<<"\nStep between dac values is : "<<trgCK_step<<std::endl;
//std::cout<<"dac values {"<<std::endl;
//for(auto &d : dac){std::cout<<"n"<<d<<std::endl;}
//std::cout<<"}"<<endl;
//
//exit(0); // DO NOT FORGET ABOUT THIS SWITCH!!!!!111

std::cout<<"initializing histograms"<<endl;
for(long int i_mmfe=0; i_mmfe<n_mmfe; i_mmfe++){
	
	for(int ivmm=0; ivmm<8; ivmm++){

		//std::map<int,int> pdofit_coeff;
		//std::map<double,double> pdofit_coeff;

		std::vector<double> pdofit_acoef;
		std::vector<double> pdofit_bcoef;
		
		std::vector<std::tuple<int,int,int>> dac_to_mV;

		if(vmm_to_include.at(ivmm)==0){continue;} //this skips vmms that have at position ivmm 0 in vector vmm_to_include

//===== drawing vmm pulser dac hist=============
		for(int idac= 0; idac<dacs.size(); idac++){//starting vmm pulser dac loop
				initH1F_dac(hpdac,Form("%i_ADC_vs_vmm%i_pulser_dac_%i",FEBs[i_mmfe],ivmm, dacs[idac]));
				std::cout<<"initiated ["<<Form("%i_ADC_vs_vmm%i_pulser_dac_%i",FEBs[i_mmfe],ivmm,dacs[idac])<<"]"<<std::endl;
		
			 	getH1F(Form("%i_ADC_vs_vmm%i_pulser_dac_%i",FEBs[i_mmfe],ivmm,dacs[idac]),hpdac)->SetStats(0);
				getH1F(Form("%i_ADC_vs_vmm%i_pulser_dac_%i",FEBs[i_mmfe],ivmm,dacs[idac]),hpdac)->SetTitle(Form("%i VMM-%i Pulser DAC = %i",FEBs[i_mmfe],ivmm,dacs[idac]));
			  getH1F(Form("%i_ADC_vs_vmm%i_pulser_dac_%i",FEBs[i_mmfe],ivmm,dacs[idac]),hpdac)->GetYaxis()->SetTitle("Samples");
			  getH1F(Form("%i_ADC_vs_vmm%i_pulser_dac_%i",FEBs[i_mmfe],ivmm,dacs[idac]),hpdac)->GetXaxis()->SetTitle("mV");
		
				cout<<"Assembling PULSER DAC "<<dacs[idac]<<" plot for - "<<strFEBs[i_mmfe].c_str()<<endl;
		
				cout << Form("sample>>%i_ADC_vs_vmm%i_pulser_dac_%i",FEBs[i_mmfe],ivmm,dacs[idac])<<Form("board==\"%s\" && vmm==%i && DAC==%i", strFEBs[i_mmfe].c_str(),ivmm,dacs[idac]) <<endl;
		
				cout<<"Assembly done"<<endl;
		
				std::cout<<"Drawing PDAC histo for - "<<strFEBs[i_mmfe].c_str()<<endl;
		
				std::cout <<pdac_tree->Draw(Form("sample>>%i_ADC_vs_vmm%i_pulser_dac_%i",FEBs[i_mmfe],ivmm,dacs[idac]),Form("board==\"%s\" && vmm==%i && DAC==%i",strFEBs[i_mmfe].c_str(),ivmm,dacs[idac]),"goff")<<std::endl;
		
				cout<<"Drawing Pulser DAC "<<dacs[idac]<<" for ["<<strFEBs[i_mmfe].c_str()<<"] done"<<endl;
		
//		 	  getH1F(Form("%i_ADC_vs_vmm%i_pulser_dac_%i",FEBs[i_mmfe],ivmm,dacs[idac]),hpdac)->Draw();
//==========================looking for mV peak for DAC hists=========================================
			cout<<"\n>>>>>> FITUEM DAC "<<dacs[idac]<<" k MV <<<<<<<<\n"<<endl;
			Int_t i;
  		const Int_t nbins = 2300;
  		Double_t xmin     = 0;
 		  Double_t xmax     = nbins;
  		Double_t source[nbins], dest[nbins];

		  TH1F *bg  = new TH1F("bg","bg",nbins,xmin,xmax);

	 	  getH1F(Form("%i_ADC_vs_vmm%i_pulser_dac_%i",FEBs[i_mmfe],ivmm,dacs[idac]),hpdac)->Draw();

			TSpectrum *s = new TSpectrum();
     			
			double threshold = 0.3;
			double width = 4;	

			Int_t nfound = s->Search(getH1F(Form("%i_ADC_vs_vmm%i_pulser_dac_%i",FEBs[i_mmfe],ivmm,dacs[idac]),hpdac),width,"",threshold);

	    cout<<"\n determining peak positions"<<endl;
	    Int_t bin;
	    Double_t a;
	    Double_t fPositionX[100];
	    Double_t fPositionY[100];
	    Double_t *xpeaks = s->GetPositionX();
	    for (i = 0; i < nfound; i++) {
	       a=xpeaks[i];
	       bin = 1 + Int_t(a + 0.5);
	       fPositionX[i] = getH1F(Form("%i_ADC_vs_vmm%i_pulser_dac_%i",FEBs[i_mmfe],ivmm,dacs[idac]),hpdac)->GetBinCenter(bin);
	       fPositionY[i] = getH1F(Form("%i_ADC_vs_vmm%i_pulser_dac_%i",FEBs[i_mmfe],ivmm,dacs[idac]),hpdac)->GetBinContent(bin);
	    }
	  
	    TPolyMarker * pm = (TPolyMarker*) getH1F(Form("%i_ADC_vs_vmm%i_pulser_dac_%i",FEBs[i_mmfe],ivmm,dacs[idac]),hpdac)->GetListOfFunctions()->FindObject("TPolyMarker");
	    if (pm) {
	       getH1F(Form("%i_ADC_vs_vmm%i_pulser_dac_%i",FEBs[i_mmfe],ivmm,dacs[idac]),hpdac)->GetListOfFunctions()->Remove(pm);
	       delete pm;
	    }
	  
	    pm = new TPolyMarker(nfound, fPositionX, fPositionY);
	    getH1F(Form("%i_ADC_vs_vmm%i_pulser_dac_%i",FEBs[i_mmfe],ivmm,dacs[idac]),hpdac)->GetListOfFunctions()->Add(pm);
	    pm->SetMarkerStyle(23);
	    pm->SetMarkerColor(kBlue);
	    pm->SetMarkerSize(1.3);
	    for (i = 0; i < nbins; i++) {
	      bg->SetBinContent(i + 1,dest[i]);
	    }
	    bg->SetLineColor(kRed);
	    bg->Draw("SAME");
	    printf("Found %d candidate peak\n",nfound);

				dac_to_mV.push_back(make_tuple(ivmm,dacs[idac],fPositionX[0]));
	
				cout<<"Saving DAC histo "<<endl;
		 	  c_dac[Form("c_%s_%i_%i",strFEBs[i_mmfe].c_str(),ivmm,dacs[idac])] = new TCanvas(Form("c_%s_%i_%i",strFEBs[i_mmfe].c_str(),ivmm,dacs[idac]),Form("c_%s_%i_%i",strFEBs[i_mmfe].c_str(),ivmm,dacs[idac]),700,500);
		 	  c_dac[Form("c_%s_%i_%i",strFEBs[i_mmfe].c_str(),ivmm,dacs[idac])]->cd();
		 	  getH1F(Form("%i_ADC_vs_vmm%i_pulser_dac_%i",FEBs[i_mmfe],ivmm,dacs[idac]),hpdac)->Draw();
//		    c_dac[Form("c_%s_%i_%i",strFEBs[i_mmfe].c_str(),ivmm,dacs[idac])]->SaveAs(Form("plotter_output/pdacs/%s_VMM-%i_pulserDAC_%i_%s.png",strFEBs[i_mmfe].c_str(),ivmm,dacs[idac],pic_name.c_str()));
	
				delete bg;		
		}//ending vmm dac loop

			std::cout<<"\nJOBA!\n------------------------------------------------------------------------"<<std::endl;
			std::cout<<" VMM "<<ivmm<<" Pulser DAC values in mV are:"	<<std::endl;
			for(auto &t: dac_to_mV){
			std::cout<<"VMM "<<get<0>(t)<<"-> DAC "<<get<1>(t)<<"-> mV "<<get<2>(t)<<""<<std::endl;
			}
			std::cout<<"------------------------------------------------------------------------"<<std::endl;
 ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////doing global vmm pulser dac fit//////////////////////////////////////////////////////////////

			TGraph *glodac = new TGraph();
			int count = 0;
			int fitrange_max_dac = 0;
			int fitrange_min_dac = 0;
			int dactup_size = dac_to_mV.size();
			for(auto &dac_tup: dac_to_mV){
				glodac->SetPoint(count, get<1>(dac_tup), get<2>(dac_tup));
				if(count==0){fitrange_min_dac = get<1>(dac_tup);}
				if(count==dactup_size-1){fitrange_min_dac = get<1>(dac_tup);}
				count++;
			}
	
	    TCanvas *cdac = new TCanvas();
	    glodac->GetXaxis()->SetRangeUser(0.,1200.);
	    glodac->GetYaxis()->SetRangeUser(0.,2000.);
	    glodac->GetXaxis()->SetTitle("DAC");
	    glodac->GetYaxis()->SetTitle("mV");
		
			TString glodac_plotname = Form("%i_vmm%i_DacVsmV_%s.png",FEBs[i_mmfe],ivmm,pic_name.c_str());
			glodac->SetTitle(glodac_plotname);
			
			glodac->SetMarkerStyle(3);
			glodac->SetMarkerSize(3);
			glodac->SetMarkerColor(kBlue);
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			
			TF1 *joba = new TF1("joba","[0]*x + [1]", fitrange_min_dac, fitrange_max_dac);
			
			glodac->Fit("joba","r");
			double vmm_dac_slope = joba->GetParameter(0);
			double vmm_dac_offset = joba->GetParameter(1);

			glodac->Draw("AP");

			cdac->SaveAs("plotter_output/pdacs/"+glodac_plotname);

 /////////////////////////////////// fitting done //////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

		int missing_chan = 0;
		std::vector<int> missing_chan_list;

		for(int chan=0; chan<64; chan++){
		
			//if(chan>10){continue;}  //Skipping channels to check faster

			int chan_entries = data_tree->GetEntries(Form("pdo && linkId==%i && vmmid==%i && channel==%i", FEBs[i_mmfe],ivmm,chan));
			std::cout<<"===============CH{"<<chan<<"}==ENT{"<<chan_entries<<"}============"<<std::endl;	
			if(chan_entries<=100){
				std::cout<<"KANAL {"<<chan<<"} has less than 100 entries in total, skipping"<<std::endl;	
				continue;
			}

			std::vector<int> tdo_means;
			std::map<int,int> pdo_means;
			
			for(int n=0; n<dac.size(); n++ ){
	
				initH1_pdo(hpdo,Form("channel_pdo_vs_hits_%i_vmm%i_chan_%i_dac_%i",FEBs[i_mmfe],ivmm,chan,dac[n]), scale, std::to_string(FEBs[i_mmfe]));
	
			  getH1_pdo(Form("channel_pdo_vs_hits_%i_vmm%i_chan_%i_dac_%i",FEBs[i_mmfe],ivmm,chan,dac[n]),hpdo)->SetTitle(Form("%i PDO VMM%i chan %i hits",FEBs[i_mmfe],ivmm,chan));
			  getH1_pdo(Form("channel_pdo_vs_hits_%i_vmm%i_chan_%i_dac_%i",FEBs[i_mmfe],ivmm,chan,dac[n]),hpdo)->GetYaxis()->SetTitle("Hits");
			  getH1_pdo(Form("channel_pdo_vs_hits_%i_vmm%i_chan_%i_dac_%i",FEBs[i_mmfe],ivmm,chan,dac[n]),hpdo)->GetXaxis()->SetTitle("PeakDetectorOutput");
	
				cout << Form("pdo>>channel_pdo_vs_hits_%i_vmm%i_chan_%i_dac_%i",FEBs[i_mmfe],ivmm,chan,dac[n])<<Form("linkId==%i && vmmid==%i && channel==%i && triggerCalibrationKey==%i", FEBs[i_mmfe],ivmm,chan,dac[n]) <<endl;
	
				std::cout<<"Drawing threshold histos for - "<<FEBs[i_mmfe]<<endl;
	
				std::cout <<data_tree->Draw(Form("pdo>>channel_pdo_vs_hits_%i_vmm%i_chan_%i_dac_%i",FEBs[i_mmfe],ivmm,chan,dac[n]),Form("linkId==%i && vmmid==%i && channel==%i && triggerCalibrationKey==%i",FEBs[i_mmfe],ivmm,chan,dac[n]),"goff")<<std::endl;
	
				cout<<"Drawing channel ["<<chan<<"] done"<<endl;
		
	 	  int pdo_ent = getH1_pdo(Form("channel_pdo_vs_hits_%i_vmm%i_chan_%i_dac_%i",FEBs[i_mmfe],ivmm,chan,dac[n]),hpdo)->GetEntries();
			
	 		cout<<"PDO entries - ("<<pdo_ent<<")"<<endl;
	
			if(pdo_ent>0){//jopta
	//==============================================================

				Int_t i;
	  		const Int_t nbins = 1030;
	  		Double_t xmin     = 0;
	 		  Double_t xmax     = nbins;
	  		Double_t source[nbins], dest[nbins];
	
			  TH1F *bg  = new TH1F("bg","bg",nbins,xmin,xmax);
	
		 	  getH1_pdo(Form("channel_pdo_vs_hits_%i_vmm%i_chan_%i_dac_%i",FEBs[i_mmfe],ivmm,chan,dac[n]),hpdo)->Draw();
	
				TSpectrum *s = new TSpectrum();
		    			
				double threshold = 0.4;
				double width = 0.5;	
	
				Int_t nfound = s->Search(getH1_pdo(Form("channel_pdo_vs_hits_%i_vmm%i_chan_%i_dac_%i",FEBs[i_mmfe],ivmm,chan,dac[n]),hpdo),width,"",threshold);
	
		    cout<<"\n determining peak positions"<<endl;
		    Int_t bin;
		    Double_t a;
		    Double_t fPositionX[100];
		    Double_t fPositionY[100];
		    Double_t *xpeaks = s->GetPositionX();
		    for (i = 0; i < nfound; i++) { //na
		       a=xpeaks[i];
		       //bin = 1 + Int_t(a + 0.5);
		       bin = Int_t(a + 0.5);
		       fPositionX[i] = getH1_pdo(Form("channel_pdo_vs_hits_%i_vmm%i_chan_%i_dac_%i",FEBs[i_mmfe],ivmm,chan,dac[n]),hpdo)->GetBinCenter(bin);
		       fPositionY[i] = getH1_pdo(Form("channel_pdo_vs_hits_%i_vmm%i_chan_%i_dac_%i",FEBs[i_mmfe],ivmm,chan,dac[n]),hpdo)->GetBinContent(bin);
		    }//nah
		  
		    TPolyMarker * pm = (TPolyMarker*)getH1_pdo(Form("channel_pdo_vs_hits_%i_vmm%i_chan_%i_dac_%i",FEBs[i_mmfe],ivmm,chan,dac[n]),hpdo)->GetListOfFunctions()->FindObject("TPolyMarker");
		    if (pm) { //na
		       getH1_pdo(Form("channel_pdo_vs_hits_%i_vmm%i_chan_%i_dac_%i",FEBs[i_mmfe],ivmm,chan,dac[n]),hpdo)->GetListOfFunctions()->Remove(pm);
		       delete pm;
		    }//nah
		  
		    pm = new TPolyMarker(nfound, fPositionX, fPositionY);
		    getH1_pdo(Form("channel_pdo_vs_hits_%i_vmm%i_chan_%i_dac_%i",FEBs[i_mmfe],ivmm,chan,dac[n]),hpdo)->GetListOfFunctions()->Add(pm);
		    pm->SetMarkerStyle(23);
		    pm->SetMarkerColor(kRed);
		    pm->SetMarkerSize(1.3);
		    for (i = 0; i < nbins; i++) {//na
		      bg->SetBinContent(i + 1,dest[i]);
		    }//nah
		    bg->SetLineColor(kRed);
		    bg->Draw("SAME");
		    printf("Found %d candidate peaks\n",nfound);
		    vector<std::pair<int,int>> ranges;
		    //vector<std::pair<int,int>> ranges = {fPositionX[0]-10, fPositionX[0]+10};
		    for( i=0;i<nfound;i++){//na
//		      printf("posx= %f, posy= %f\n",fPositionX[i], fPositionY[i]);
		      //float xpos = fPositionX[i]-0.5;
		      float xpos = fPositionX[i];
		      float ypos = fPositionY[i];
//		      int bin_count = 0;
//		      float prev_bin_val_r = ypos;
//		      float prev_bin_val_l = ypos;
//		      int next_bin_val_r = 0;
//		      int next_bin_val_l = 0;
//		      int next_bin_l, next_bin_r;
//		      //for(int b=1; b<50; b++)
//					int b=20;
//					next_bin_r = xpos + b;
//		      next_bin_l = xpos - b;
//		      ranges.push_back(std::make_pair(next_bin_l, next_bin_r));
		      ranges.push_back(std::make_pair(xpos-10, xpos+10));
					if(i>0){continue;}
				}//nah
	
	//		  vector<float> means;//, sigmas;
			  float mean;//, sig;
	
				std::cout<<"\n>>>Ranges = "<<ranges.size()<<" <<<"<<std::endl;
			  for( auto & r:ranges)
			  {//na
			    TF1 *ga = new TF1("ga","gaus",r.first,r.second);
	//		    Na->Fit("ga","R");
			    getH1_pdo(Form("channel_pdo_vs_hits_%i_vmm%i_chan_%i_dac_%i",FEBs[i_mmfe],ivmm,chan,dac[n]),hpdo)->Fit("ga","R","Q");
			    //means.push_back(ga->GetParameter(1));
			    //pdo_means.push_back(ga->GetParameter(1));
			    pdo_means[dac.at(n)] = ga->GetParameter(1);
			//    sigmas.push_back(ga->GetParameter(2));
					std::cout<<" [mean = "<<ga->GetParameter(1)<<"][sigma = "<<ga->GetParameter(2)<<"]"<<std::endl;
			  }//nah
//			  std::sort(pdo_means.begin(), pdo_means.end());
			  if(pdo_means.size()==0){//na
			    missing_chan++;
				  missing_chan_list.push_back(chan);
				}//nah 
	//================================================================
				cout<<"Saving channel "<<chan<<" PDO dist"<<endl;
	 		  c[Form("c_%i_%i_%i_%i",FEBs[i_mmfe],ivmm,chan,dac[n])] = new TCanvas(Form("c_%i_%i_%i_%i",FEBs[i_mmfe],ivmm,chan,dac[n]),Form("c_%i_%i_%i_%i",FEBs[i_mmfe],ivmm,chan,dac[n]),700,500);
			  cout<<"TY gavno"<<endl;
	 		  c[Form("c_%i_%i_%i_%i",FEBs[i_mmfe],ivmm,chan,dac[n])]->cd();
			  cout<<"Ty kuda polez?"<<endl;
		 	  getH1_pdo(Form("channel_pdo_vs_hits_%i_vmm%i_chan_%i_dac_%i",FEBs[i_mmfe],ivmm,chan,dac[n]),hpdo)->Draw();
			  cout<<"te vjebat?"<<endl;
			  c[Form("c_%i_%i_%i_%i",FEBs[i_mmfe],ivmm,chan,dac[n])]->SaveAs(Form("plotter_output/pdo/%i_%iVMM_%ichan_%iDAC_%s.png",FEBs[i_mmfe],ivmm,chan,dac[n],pic_name.c_str()));
			  cout<<"ty cho ne ponjal?"<<endl;
	
				delete bg;
	//			delete pm;
	//			delete s;        		
				ranges.clear();
//===================================================================================
			 }//maslo
			}//dac loop for pdo ends here

		 std::cout<<"END OF triggerCalibKey LOOP"<<std::endl;

			if(pdo_means.size()==0){
				std::cout<<"Warning! Did not find any clear PDO peaks"<<std::endl;
				continue;
			}//match
			std::cout<<"Found following PDO peaks for channel [-"<<chan<<"-]:\n--------------------------"<<std::endl;
			

			for(auto &p: pdo_means){
				std::cout<<p.first<<"|"<<p.second<<std::endl;	
			}
			std::cout<<"\n-----------------------"<<std::endl;
	//		else if(ivmm==0 && chan%2!=0){		
//			std::vector<int> pdo_milivolts;
//
//			for(auto &tup : dac_to_mV)
//			{
//				pdo_milivolts.push_back(get<2>(tup));
//			}//mathc 

			TGraph *gr1 = new TGraph();
			std::cout<<"\nPlotting pdo vs dac"<<std::endl;	
//			for(int p=0; p<pdo_means.size(); p++)
///			{
///				gr1->SetPoint(p, pdo_milivolts.at(p),pdo_means.at(p));
///					std::cout<<"Point ["<<p.first<<"] = |"<<pdo_milivolts.at(p)<<"|"<<pdo_means.at(p)<<"|"<<std::endl;		
///			}//match	

//===================================================================================
//===========  recalculating constants ==============================================
//===================================================================================
//			y[mV] = a[mV/dac]*x[dac] - b[dac]); DAC value from global vmm dac fit
//			
//			y_[pdo] = a_1[pdo/dac]* x[dac] + b_1[pdo];

			int counter = 0;
			int pdofitrange_max = 0;
			int pdofitrange_min = 0;
			int pdo_range_size = pdo_means.size();
			int trgCKstep_mV = trgCK_step*vmm_dac_slope + vmm_dac_offset;// !!!converting DAC to mV here!!!

			for(auto &p: pdo_means)
			{
				int dacpoint_to_mV = 0;
				dacpoint_to_mV = p.first*vmm_dac_slope + vmm_dac_offset;// !!!converting DAC to mV here!!!
				if(counter==0){pdofitrange_min = dacpoint_to_mV;}
				if(p.second>=1022){
					if(pdofitrange_max==0){
					 pdofitrange_max = dacpoint_to_mV - trgCKstep_mV;
					}
				}
				if(counter==pdo_range_size-1 and pdofitrange_max == 0){pdofitrange_max = dacpoint_to_mV;}
				gr1->SetPoint(counter, dacpoint_to_mV, p.second);
				//gr1->SetPoint(counter, p.first,p.second);
				//std::cout<<"Point ["<<counter<<"] = |"<<p.first<<"|"<<p.second<<"|"<<std::endl;		
				std::cout<<"Point ["<<counter<<"] = |"<<p.first<<"[dac]|"<<p.second<<"|"<<std::endl;		
				counter++;
			}//match	
		
	    TCanvas *c7 = new TCanvas();

	    gr1->GetXaxis()->SetRangeUser(0.,2300.);
	    gr1->GetYaxis()->SetRangeUser(0.,1200.);
	    gr1->GetXaxis()->SetTitle("mV");
	    gr1->GetYaxis()->SetTitle("PDO");
		
	    TString plotname;
	    plotname = Form("%i_vmm%i_chan%i_PdoVsDac_%s.png",FEBs[i_mmfe],ivmm,chan,pic_name.c_str());
	    gr1->SetTitle(plotname);
		
		    // Draw graph 
		    // A = make axis
		    // p = make points
		    // L = draw a line in between
	    //gr1->Draw("P");
	    gr1->SetMarkerStyle(2);
	    gr1->SetMarkerSize(2);
	    gr1->SetMarkerColor(4);
	// tryna` fit
			//int maxFitRange = pdo_milivolts.size();
			//TF1 *line = new TF1("line","[0]*x + [1]",pdo_milivolts[0],maxFitRange);
			TF1 *line = new TF1("line","[0]*x + [1]",pdofitrange_min,pdofitrange_max);
			gr1->Fit("line","r");

			double pdoslope = line->GetParameter(0);
			double pdooffset = line->GetParameter(1);
//			pdofit_coeff[pdoslope] = pdooffset;
		  pdofit_acoef.push_back(pdoslope);		
		  pdofit_bcoef.push_back(pdooffset);		
		
	    gr1->Draw("AP");
			
				std::cout<<"\nchannel "<<chan<<": b=["<<pdoslope<<"] a=["<<pdooffset<<"]\n"<<std::endl;

			c7->SaveAs("plotter_output/fitpdo/"+plotname);


//  		pdo_milivolts.clear();
  		delete gr1;
  		delete line;
//		}		
			
//===================================================================================
		std::cout<<"END OF PDO LOOP"<<std::endl;
		cout<<"\nOLO, drawing TDOs\n"<<endl;

//  	initH1_tdo(htdo,Form("channel_tdo_vs_hits_%i_vmm%i_chan_%i_del_%i",FEBs[i_mmfe],ivmm,chan,del[n]), scale, std::to_string(FEBs[i_mmfe]));
//
//		getH1_tdo(Form("channel_tdo_vs_hits_%i_vmm%i_chan_%i_del_%i",FEBs[i_mmfe],ivmm,chan,del[n]),htdo)->SetTitle(Form("%i TDO VMM%i chan %i hits",FEBs[i_mmfe],ivmm,chan));
//		getH1_tdo(Form("channel_tdo_vs_hits_%i_vmm%i_chan_%i_del_%i",FEBs[i_mmfe],ivmm,chan,del[n]),htdo)->GetYaxis()->SetTitle("Hits");
//		getH1_tdo(Form("channel_tdo_vs_hits_%i_vmm%i_chan_%i_del_%i",FEBs[i_mmfe],ivmm,chan,del[n]),htdo)->GetXaxis()->SetTitle("TimeDetectorOutput");
//
//			cout << Form("tdo>>channel_tdo_vs_hits_%i_vmm%i_chan_%i_del_%i",FEBs[i_mmfe],ivmm,chan)<<Form("linkId==%i && vmmid==%i && channel==%i", FEBs[i_mmfe],ivmm,chan) <<endl;
//		std::cout <<data_tree->Draw(Form("tdo>>channel_tdo_vs_hits_%i_vmm%i_chan_%i_del_%i",FEBs[i_mmfe],ivmm,chan),Form("linkId==%i && vmmid==%i && channel==%i",FEBs[i_mmfe],ivmm,chan),"goff")<<std::endl;
//
// 	  int tdo_ent = getH1_tdo(Form("channel_tdo_vs_hits_%i_vmm%i_chan_%i",FEBs[i_mmfe],ivmm,chan),htdo)->GetEntries();
//
// 		cout<<"TDO entries - ("<<tdo_ent<<")"<<endl;
//		if(tdo_ent>0){
////====================searching & fitting tdo peaks================================================================
//		cout<<"\n>>>>>> ALO, fituem TDO <<<<<<<<\n"<<endl;
//			Int_t i;
//  		const Int_t nbins = 250;
//  		Double_t xmin     = 0;
// 		  Double_t xmax     = nbins;
//  		Double_t source[nbins], dest[nbins];
//
//		  TH1F *bg  = new TH1F("bg","bg",nbins,xmin,xmax);
//
//	 	  getH1_tdo(Form("channel_tdo_vs_hits_%i_vmm%i_chan_%i",FEBs[i_mmfe],ivmm,chan),htdo)->Draw();
//
//			TSpectrum *s = new TSpectrum();
//     			
//			double threshold = 0.1;
//			double width = 6;	
//
//			Int_t nfound = s->Search(getH1_tdo(Form("channel_tdo_vs_hits_%i_vmm%i_chan_%i",FEBs[i_mmfe],ivmm,chan),htdo),width,"",threshold);
//
//	    cout<<"\n determining peak positions"<<endl;
//	    Int_t bin;
//	    Double_t a;
//	    Double_t fPositionX[100];
//	    Double_t fPositionY[100];
//	    Double_t *xpeaks = s->GetPositionX();
//	    for (i = 0; i < nfound; i++) {
//	       a=xpeaks[i];
//	       bin = 1 + Int_t(a + 0.5);
//	       fPositionX[i] = getH1_tdo(Form("channel_tdo_vs_hits_%i_vmm%i_chan_%i",FEBs[i_mmfe],ivmm,chan),htdo)->GetBinCenter(bin);
//	       fPositionY[i] = getH1_tdo(Form("channel_tdo_vs_hits_%i_vmm%i_chan_%i",FEBs[i_mmfe],ivmm,chan),htdo)->GetBinContent(bin);
//	    }
//	  
//	    TPolyMarker * pm = (TPolyMarker*)getH1_tdo(Form("channel_tdo_vs_hits_%i_vmm%i_chan_%i",FEBs[i_mmfe],ivmm,chan),htdo)->GetListOfFunctions()->FindObject("TPolyMarker");
//	    if (pm) {
//	       getH1_tdo(Form("channel_tdo_vs_hits_%i_vmm%i_chan_%i",FEBs[i_mmfe],ivmm,chan),htdo)->GetListOfFunctions()->Remove(pm);
//	       delete pm;
//	    }
//	  
//	    pm = new TPolyMarker(nfound, fPositionX, fPositionY);
//	    getH1_tdo(Form("channel_tdo_vs_hits_%i_vmm%i_chan_%i",FEBs[i_mmfe],ivmm,chan),htdo)->GetListOfFunctions()->Add(pm);
//	    pm->SetMarkerStyle(23);
//	    pm->SetMarkerColor(kRed);
//	    pm->SetMarkerSize(1.3);
//	    for (i = 0; i < nbins; i++) {
//	      bg->SetBinContent(i + 1,dest[i]);
//	    }
//	    bg->SetLineColor(kRed);
//	    bg->Draw("SAME");
//	    printf("Found %d candidate peaks\n",nfound);
//	    vector<std::pair<int,int>> tdo_ranges;
//	    for( i=0;i<nfound;i++){
//	      printf("posx= %f, posy= %f\n",fPositionX[i], fPositionY[i]);
//	      float xpos = fPositionX[i]-0.5;
//	      float ypos = fPositionY[i];
//	      int bin_count = 0;
//	      float prev_bin_val_r = ypos;
//	      float prev_bin_val_l = ypos;
//	      int next_bin_val_r = 0;
//	      int next_bin_val_l = 0;
//	      int next_bin_l, next_bin_r;
//	      //for(int b=1; b<50; b++)
//				int b=20;
//				next_bin_r = xpos + b;
//	      next_bin_l = xpos - b;
//	      tdo_ranges.push_back(std::make_pair(next_bin_l, next_bin_r));
//			}
//
////		  vector<float> means;//, sigmas;
//		  float t_mean;//, sig;
//
//			std::cout<<"\n>>>Ranges = "<<tdo_ranges.size()<<" <<<"<<std::endl;
//		  for( auto & r:tdo_ranges)
//		  {
//		    //if(cd >0){cout<<"\nfit ranges are "<<r.second<<" from left & "<<r.first<<" from right"<<endl;}
//		    TF1 *ga = new TF1("ga","gaus",r.first,r.second);
////		    Na->Fit("ga","R");
//		    getH1_tdo(Form("channel_tdo_vs_hits_%i_vmm%i_chan_%i",FEBs[i_mmfe],ivmm,chan),htdo)->Fit("ga","R","Q");
//		    //means.push_back(ga->GetParameter(1));
//		    tdo_means.push_back(ga->GetParameter(1));
//		//    sigmas.push_back(ga->GetParameter(2));
//				std::cout<<" [mean = "<<ga->GetParameter(1)<<"][sigma = "<<ga->GetParameter(2)<<"]"<<std::endl;
//		  }
//		
////================================================================
//
////			cout<<"Saving channel "<<chan<<" TDO dist"<<endl;
//// 		  c_tdo[Form("ct_%i_%i_%i",FEBs[i_mmfe],ivmm,chan)] = new TCanvas(Form("ct_%i_%i_%i",FEBs[i_mmfe],ivmm,chan),Form("ct_%i_%I_%i",FEBs[i_mmfe],ivmm,chan),700,500);
//// 		  c_tdo[Form("ct_%i_%i_%i",FEBs[i_mmfe],ivmm,chan)]->cd();
//// 		  getH1_tdo(Form("channel_tdo_vs_hits_%i_vmm%i_chan_%i",FEBs[i_mmfe],ivmm,chan),htdo)->Draw();
//
////		  c_tdo[Form("ct_%i_%i_%i",FEBs[i_mmfe],ivmm,chan)]->SaveAs(Form("plotter_output/tdo/%i_VMM%i_chan_%i_%s.png",FEBs[i_mmfe],ivmm,chan,pic_name.c_str()));
//
//			delete bg;
////			delete pm;
////			delete s;
//			tdo_ranges.clear();
//
//		}

		cout<<"Saving done"<<endl;
		pdo_means.clear();
		tdo_means.clear();

	}//end of channel plotting loop
		std::cout<<"END OF CHANNEL LOOP"<<std::endl;

		std::cout<<"############## VMM "<<ivmm<<" fitting stats ###############"<<std::endl;
		std::cout<<"Missing {"<<missing_chan<<"} channels:"<<std::endl;
		for(int misch = 0; misch < missing_chan_list.size(); misch++ ){
				std::cout<<missing_chan_list.at(misch)<<::std::endl;
		}
		std::cout<<"###########################################################"<<std::endl;
		for(int chan=0; chan<64; chan++){
		 	delete	c[Form("c_%i_%i_%i",FEBs[i_mmfe],ivmm,chan)];

//			for(int n=0; n<dac.size(); n++){
//		  	delete getH1_pdo(Form("channel_pdo_vs_hits_%i_vmm%i_chan_%i_dac%i",FEBs[i_mmfe],ivmm,chan,dac[n]),hpdo);
//			}
// 			delete getH1_tdo(Form("channel_tdo_vs_hits_%i_vmm%i_chan_%i",FEBs[i_mmfe],ivmm,chan),htdo);
		}
		dac_to_mV.clear();

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  		TGraph *coeff_a = new TGraph();
			TGraph *coeff_b = new TGraph();
			int count_a = 0;
//			int fitrange_max_dac = 0;
//			int fitrange_min_dac = 0;
			//int coeff_size = pdofit_coeff.size();
			int coeffa_size = pdofit_acoef.size();
			for(auto &coef_a: pdofit_acoef){
				std::cout<<"|CH["<<count_a<<"] | a["<<coef_a<<"] | b["<<pdofit_bcoef[count_a]<<"] |"<<std::endl;
//			  int slope = pcoeff;
				coeff_a->SetPoint(count_a,count_a, coef_a);
				//coeff_a->SetPoint(count_c,count_c, stoi(pcoeff.first));
				coeff_b->SetPoint(count_a,count_a, pdofit_bcoef[count_a]);
//				if(count==0){fitrange_min_dac = get<1>(dac_tup);}
//				if(count==dactup_size-1){fitrange_min_dac = get<1>(dac_tup);}
				count_a++;
			}
	
//	    TCanvas *ca = new TCanvas();
//	    TCanvas *cb = new TCanvas();
//	    glodac->GetXaxis()->SetRangeUser(0.,1200.);
//	    glodac->GetYaxis()->SetRangeUser(0.,2000.);
	    coeff_a->GetXaxis()->SetTitle("channel");
	    coeff_a->GetXaxis()->SetTitle("channel");
	    coeff_b->GetYaxis()->SetTitle("mV/PDO");
	    coeff_b->GetYaxis()->SetTitle("mV");
		
			TString pcoeff_a_plotname = Form("%i_vmm%i_channel_slopes_%s.png",FEBs[i_mmfe],ivmm,pic_name.c_str());
			TString pcoeff_b_plotname = Form("%i_vmm%i_channel_offsets_%s.png",FEBs[i_mmfe],ivmm,pic_name.c_str());
			coeff_a->SetTitle(pcoeff_a_plotname);
			coeff_b->SetTitle(pcoeff_b_plotname);
			
			coeff_a->SetMarkerStyle(2);
			coeff_b->SetMarkerStyle(31);
			coeff_a->SetMarkerSize(3);
			coeff_b->SetMarkerSize(3);
			coeff_a->SetMarkerColor(2);
			coeff_b->SetMarkerColor(8);
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
			TCanvas *ca = new TCanvas();
			coeff_a->Draw("AP");
			ca->SaveAs("plotter_output/"+pcoeff_a_plotname);

			TCanvas *cb = new TCanvas();
			coeff_b->Draw("AP");
			cb->SaveAs("plotter_output/"+pcoeff_b_plotname);

	/////////////////////////////////// plotting obtained coefficients for vmms //////////////////////////////////////////////////////////////
//
//			TGraph *coeff_a = new TGraph();
//			TGraph *coeff_b = new TGraph();
//			int count_b = 0;
////			int fitrange_max_dac = 0;
////			int fitrange_min_dac = 0;
//			//int coeff_size = pdofit_coeff.size();
//			int coeffb_size = pdofit_bcoef.size();
//			for(auto &coef_b: pdofit_bcoef){
//				std::cout<<"CH["<<count_b<<"] | b["<<coef_b<<"]"<<std::endl;
////			  int slope = pcoeff;
//				coeff_b->SetPoint(count_b,count_b, coef_b);
//				coeff_a->SetPoint(count_c,count_c, stoi(pcoeff.first));
////				coeff_b->SetPoint(count_c,count_c, pdofit_bcoef[count_c]);
////				if(count==0){fitrange_min_dac = get<1>(dac_tup);}
////				if(count==dactup_size-1){fitrange_min_dac = get<1>(dac_tup);}
//				count_b++;
//			}
//	
////	    TCanvas *ca = new TCanvas();
//	    TCanvas *cb = new TCanvas();
////	    glodac->GetXaxis()->SetRangeUser(0.,1200.);
////	    glodac->GetYaxis()->SetRangeUser(0.,2000.);
////	    coeff_a->GetXaxis()->SetTitle("channel");
////	    coeff_a->GetXaxis()->SetTitle("channel");
//	    coeff_b->GetYaxis()->SetTitle("mV/PDO");
//	    coeff_b->GetYaxis()->SetTitle("mV");
//		
////			TString pcoeff_a_plotname = Form("%i_vmm%i_channel_slopes_%s.png",FEBs[i_mmfe],ivmm,pic_name.c_str());
//			TString pcoeff_b_plotname = Form("%i_vmm%i_channel_offsets_%s.png",FEBs[i_mmfe],ivmm,pic_name.c_str());
////			coeff_a->SetTitle(pcoeff_a_plotname);
//			coeff_b->SetTitle(pcoeff_b_plotname);
//			
////			coeff_a->SetMarkerStyle(5);
//			coeff_b->SetMarkerStyle(5);
////			coeff_a->SetMarkerSize(3);
//			coeff_b->SetMarkerSize(3);
////			coeff_a->SetMarkerColor(kRed);
//			coeff_b->SetMarkerColor(kYellow+3);
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//		
////			coeff_a->Draw("AP");
//			coeff_b->Draw("AP");
//
////			ca->SaveAs("plotter_output/"+pcoeff_a_plotname);
//			cb->SaveAs("plotter_output/"+pcoeff_b_plotname);
//
 /////////////////////////////////// done //////////////////////////////////////////////////////////////
//
		pdofit_acoef.clear();
		pdofit_bcoef.clear();
	
	}//end of vmm plotting loopo
}//endof feb loop	

cout<<"////////////////////////////////////////////////////////////////////////////////////////////////////////////"<<endl;
cout<<"//////   /////  //         //      //////        ////         //////    ////////////////////////////////////"<<endl;
cout<<"/////  /  ///  //  /////  //  ////  ////  ////  ///  //////////////    /////////////////////////////////////"<<endl;
cout<<"////  //  //  //  /////  //  /////  ///  ////  /////      ////////   ///////////////////////////////////////"<<endl;
cout<<"///  ///  /  //         //        ////        ///////////  /////////////////////////////////////////////////"<<endl;
cout<<"//  ////    //  /////  //  //////////  ////  ///         ///////  //////////////////////////////////////////"<<endl;
cout<<"////////////////////////////////////////////////////////////////////////////////////////////////////////////"<<endl;
}

//=====================================================================================================================================
//=====================================================================================================================================
//=====================================================================================================================================

void other_plot(std::string io_config_path, std::string pic_name, std::string this_str, bool bl_on, bool thr_on, bool scale){

namespace pt = boost::property_tree;
pt::ptree input_data;
pt::read_json(io_config_path, input_data);

////============= reading calibration parameter file directory ================
string json_file = input_data.get<std::string>("configuration_json");
string json_file_dir = input_data.get<std::string>("config_dir");
std::string c_path = json_file_dir+json_file;
pt::ptree config_tree;
pt::read_json(c_path, config_tree);
std::vector<std::string> FrontEnds;
std::string feb_name;
BOOST_FOREACH(pt::ptree::value_type& val, config_tree)
{
		feb_name = val.first;
		FrontEnds.push_back(feb_name);
}

//--------- open file -----------------------

	TFile *f = new TFile("root_files/data_tree.root");
	//TFile *f = new TFile("test_tree.root");
	std::cout<< f <<std::endl;
	TTree *par_tree=(TTree* )f->Get("par_tree");
	//TTree *cal_tree=(TTree* )f->Get("cal_tree");
	std::cout<< par_tree<<std::endl;
//-----------------------------------------------
	TFile *f1 = new TFile("root_files/threshold_tree.root");
	TTree *thr_tree=(TTree* )f1->Get("thr_tree");
	std::cout<< thr_tree<<std::endl;
//-------------------------------------------------

	TFile *f2 = new TFile("root_files/baseline_tree.root");
	TTree *bl_tree = (TTree*)f2->Get("bl_tree");
	std::cout<<bl_tree<<std::endl;

//------------------------------------------------------
//define maps to hold the pointers to the histos
std::map<std::string,TH2D*> h1; //baseline hists
std::map<std::string,TH2D*> h3; // channel trimmed threshold hists
std::map<std::string,TH2D*> h4; //untrimed thresholds from dif root file
std::map<std::string,TH2D*> h5; //channel noise rms histograms
std::map<std::string,TH2D*> htno; //channel noise rms histograms
std::map<std::string,TH2D*> hn; //channel noise rms histograms from direct bl measurements
std::map<std::string,TH2D*> hno; //channel noise rms overflow histograms from direct bl measurements
std::map<std::string,TH2D*> h6;
std::map<std::string,TH2D*> h7;
std::map<std::string,TH2F*> bl;
std::map<std::string,TH2D*> hmask;

//define map to hold canvas per mmfe8
std::map<std::string,TCanvas*> c;
std::map<std::string,TCanvas*> n;
//std::map<std::string,TCanvas*> c_blo;
std::map<std::string,TCanvas*> c_bl;
std::map<std::string,TCanvas*> c_no;

//======================== reading the names of FEBs -=========================================

std::string f_name;
std::vector<string> FEBs;
std::string roc = "roc_common_config";
for(int f=0; f<FrontEnds.size(); f++)
{
	f_name = FrontEnds[f];
	if(f_name==roc){continue;}
	else if(f_name=="vmm_common_config"){continue;}
	else if(f_name=="tds_common_config"){continue;}
	else {
		FEBs.push_back(f_name);
	}
}
//=========================================================================================
long int n_mmfe = FEBs.size();

std::cout<<"we have: "<<FEBs.size()<<" FEBs to describe"<<std::endl;
std::sort(FEBs.begin(),FEBs.end());

//=========================  define histos for each MMFE8 ====================================
std::cout<<"initializing histograms"<<endl;
for(long int i_mmfe=0; i_mmfe<n_mmfe; i_mmfe++){
	if((this_str.length()>0) and (FEBs.at(i_mmfe).find(this_str)!=std::string::npos)){
//	std::cout<<"initializing histos for - "<<FEBs[i_mmfe]<<endl;
  if(thr_on==true){
		initH2(h1,Form("ch_baseline_med_vs_channel_%s",FEBs[i_mmfe].c_str()), scale, FEBs[i_mmfe]);
		initH2(h3,Form("ch_trimmed_thr_vs_channel_%s",FEBs[i_mmfe].c_str()),scale, FEBs[i_mmfe]);
		initH2(h4,Form("ch_threshold_ut_vs_channel_%s",FEBs[i_mmfe].c_str()),scale, FEBs[i_mmfe]);
//---------effective threshold -----------------------------------------------------------------
		initH2(h6,Form("ch_eff_thr_w_best_trim_vs_channel_%s",FEBs[i_mmfe].c_str()),scale, FEBs[i_mmfe]);
		initH2(hmask,Form("mask_vs_channel_%s",FEBs[i_mmfe].c_str()),scale, FEBs[i_mmfe]);
//------------noise histo -------------------------------------------------------------------------------
		initH2_noise(h5,Form("ch_baseline_rms_vs_channel_%s",FEBs[i_mmfe].c_str()),scale, FEBs[i_mmfe]);
		initH2_noise(h7,Form("vmm_median_rms_vs_channel_%s",FEBs[i_mmfe].c_str()),scale, FEBs[i_mmfe]);
		initH2_noise(htno,Form("ch_baseline_rms_overflow_vs_channel_%s",FEBs[i_mmfe].c_str()),scale, FEBs[i_mmfe]);
	}
  if(bl_on==true){
		initH2_noise(hn,Form("baseline_rms_vs_channel_%s",FEBs[i_mmfe].c_str()),scale, FEBs[i_mmfe]);
  	initH2_noise_over(hno,Form("noise_overflow_vs_channel_%s",FEBs[i_mmfe].c_str()),scale, FEBs[i_mmfe]);
//------------baselines-------------------------------------------------------------------------------
 		initH2F(bl,Form("samples_vs_channel_%s",FEBs[i_mmfe].c_str()),scale, FEBs[i_mmfe]);
		}
	else{continue;}
	}else{continue;}
}

//======================== set style =========================================================
	std::cout<<"Formating histograms"<<std::endl;
for(int i_mmfe=0; i_mmfe<n_mmfe; i_mmfe++){
//------------------ baseline and thresholds --------------------------------------------------------------------------
	if((this_str.length()>0) and (FEBs.at(i_mmfe).find(this_str)!=std::string::npos)){
		if(thr_on==true){
			//----------------------- channel baseline median ---------------------------------------------------	
			  getH2(Form("ch_baseline_med_vs_channel_%s",FEBs[i_mmfe].c_str()),h1)->SetStats(0);
			  getH2(Form("ch_baseline_med_vs_channel_%s",FEBs[i_mmfe].c_str()),h1)->SetTitle(Form("%s Baseline and Thresholds",FEBs[i_mmfe].c_str()));
			  getH2(Form("ch_baseline_med_vs_channel_%s",FEBs[i_mmfe].c_str()),h1)->SetMarkerStyle(7);
			  getH2(Form("ch_baseline_med_vs_channel_%s",FEBs[i_mmfe].c_str()),h1)->SetMarkerColor(kBlack);
			  getH2(Form("ch_baseline_med_vs_channel_%s",FEBs[i_mmfe].c_str()),h1)->GetYaxis()->SetTitle("mV");
			  getH2(Form("ch_baseline_med_vs_channel_%s",FEBs[i_mmfe].c_str()),h1)->GetXaxis()->SetTitle("Channel");
			//----------------------- channel trimmed threshold ---------------------------------------------------	
				getH2(Form("ch_trimmed_thr_vs_channel_%s",FEBs[i_mmfe].c_str()),h3)->SetMarkerStyle(7);
			  getH2(Form("ch_trimmed_thr_vs_channel_%s",FEBs[i_mmfe].c_str()),h3)->SetMarkerColor(kGreen);
			  getH2(Form("ch_trimmed_thr_vs_channel_%s",FEBs[i_mmfe].c_str()),h3)->GetYaxis()->SetTitle("mV");
			  getH2(Form("ch_trimmed_thr_vs_channel_%s",FEBs[i_mmfe].c_str()),h3)->GetXaxis()->SetTitle("Channel");
			//----------------------- channel untrimmed threshold ---------------------------------------------------	
				getH2(Form("ch_threshold_ut_vs_channel_%s",FEBs[i_mmfe].c_str()),h4)->SetMarkerStyle(7);
			  getH2(Form("ch_threshold_ut_vs_channel_%s",FEBs[i_mmfe].c_str()),h4)->SetMarkerColor(kBlue);
			  getH2(Form("ch_threshold_ut_vs_channel_%s",FEBs[i_mmfe].c_str()),h4)->GetYaxis()->SetTitle("mV");
			  getH2(Form("ch_threshold_ut_vs_channel_%s",FEBs[i_mmfe].c_str()),h4)->GetXaxis()->SetTitle("Channel");
			//----------------------- effective threshold ---------------------------------------------------	
				getH2(Form("ch_eff_thr_w_best_trim_vs_channel_%s",FEBs[i_mmfe].c_str()),h6)->SetMarkerStyle(7);
			  getH2(Form("ch_eff_thr_w_best_trim_vs_channel_%s",FEBs[i_mmfe].c_str()),h6)->SetMarkerColor(kYellow+3);
			  getH2(Form("ch_eff_thr_w_best_trim_vs_channel_%s",FEBs[i_mmfe].c_str()),h6)->GetYaxis()->SetTitle("mV");
			  getH2(Form("ch_eff_thr_w_best_trim_vs_channel_%s",FEBs[i_mmfe].c_str()),h6)->GetXaxis()->SetTitle("Channel");		
			//---------------------- noise plots ---------------------------------------------------------------------------
			 	getH2(Form("ch_baseline_rms_vs_channel_%s",FEBs[i_mmfe].c_str()),h5)->SetStats(0);
				getH2(Form("ch_baseline_rms_vs_channel_%s",FEBs[i_mmfe].c_str()),h5)->SetTitle(Form("%s Channel Noise", FEBs[i_mmfe].c_str()));
				getH2(Form("ch_baseline_rms_vs_channel_%s",FEBs[i_mmfe].c_str()),h5)->SetMarkerStyle(7);
			  getH2(Form("ch_baseline_rms_vs_channel_%s",FEBs[i_mmfe].c_str()),h5)->SetMarkerColor(kMagenta+3);
			  getH2(Form("ch_baseline_rms_vs_channel_%s",FEBs[i_mmfe].c_str()),h5)->GetYaxis()->SetTitle("ENC");
			  getH2(Form("ch_baseline_rms_vs_channel_%s",FEBs[i_mmfe].c_str()),h5)->GetXaxis()->SetTitle("channel");
			// -----------for overflow to see readings out of scope of the histogram -------------
			 	getH2(Form("ch_baseline_rms_overflow_vs_channel_%s",FEBs[i_mmfe].c_str()),htno)->SetStats(0);
			//	getH2(Form("ch_baseline_rms_overflow_vs_channel_%s",FEBs[i_mmfe].c_str()),h5)->SetTitle(Form("%s channel noise", FEBs[i_mmfe].c_str()));
				getH2(Form("ch_baseline_rms_overflow_vs_channel_%s",FEBs[i_mmfe].c_str()),htno)->SetMarkerStyle(22);
			  getH2(Form("ch_baseline_rms_overflow_vs_channel_%s",FEBs[i_mmfe].c_str()),htno)->SetMarkerColor(kBlack);
			  getH2(Form("ch_baseline_rms_overflow_vs_channel_%s",FEBs[i_mmfe].c_str()),htno)->GetYaxis()->SetTitle("ENC");
			  getH2(Form("ch_baseline_rms_overflow_vs_channel_%s",FEBs[i_mmfe].c_str()),htno)->GetXaxis()->SetTitle("Channel");
			//------------------------------------------------------------------------------
			 	getH2(Form("vmm_median_rms_vs_channel_%s",FEBs[i_mmfe].c_str()),h7)->SetStats(0);
				getH2(Form("vmm_median_rms_vs_channel_%s",FEBs[i_mmfe].c_str()),h7)->SetMarkerStyle(7);
			  getH2(Form("vmm_median_rms_vs_channel_%s",FEBs[i_mmfe].c_str()),h7)->SetMarkerColor(kGreen+2);
			  getH2(Form("vmm_median_rms_vs_channel_%s",FEBs[i_mmfe].c_str()),h7)->GetYaxis()->SetTitle("ENC");
			  getH2(Form("vmm_median_rms_vs_channel_%s",FEBs[i_mmfe].c_str()),h7)->GetXaxis()->SetTitle("Channel");
			//-------------- masked channels --------------------------------
			 	getH2(Form("mask_vs_channel_%s",FEBs[i_mmfe].c_str()),hmask)->SetStats(0);
				getH2(Form("mask_vs_channel_%s",FEBs[i_mmfe].c_str()),hmask)->SetMarkerStyle(21);
			  getH2(Form("mask_vs_channel_%s",FEBs[i_mmfe].c_str()),hmask)->SetMarkerColor(kBlue);
	}
	if(bl_on==true){
		//--------------------- noise plot from direct bl measurement ----------------------------------------------------
			getH2(Form("baseline_rms_vs_channel_%s",FEBs[i_mmfe].c_str()),hn)->SetStats(0);
			getH2(Form("baseline_rms_vs_channel_%s",FEBs[i_mmfe].c_str()),hn)->SetTitle(Form("%s Channel Noise", FEBs[i_mmfe].c_str()));
			getH2(Form("baseline_rms_vs_channel_%s",FEBs[i_mmfe].c_str()),hn)->SetMarkerStyle(7);
		  getH2(Form("baseline_rms_vs_channel_%s",FEBs[i_mmfe].c_str()),hn)->SetMarkerColor(kRed+2);
		  getH2(Form("baseline_rms_vs_channel_%s",FEBs[i_mmfe].c_str()),hn)->GetYaxis()->SetTitle("ENC");
		  getH2(Form("baseline_rms_vs_channel_%s",FEBs[i_mmfe].c_str()),hn)->GetXaxis()->SetTitle("Channel");
//			getH2(Form("noise_overflow_vs_channel_%s",FEBs[i_mmfe].c_str()),hno)->SetStats(0);
		//	getH2(Form("noise_overflow_vs_channel_%s",FEBs[i_mmfe].c_str()),hno)->SetTitle(Form("%s channel noise", FEBs[i_mmfe].c_str()));
			getH2(Form("noise_overflow_vs_channel_%s",FEBs[i_mmfe].c_str()),hno)->SetMarkerStyle(22);
			getH2(Form("noise_overflow_vs_channel_%s",FEBs[i_mmfe].c_str()),hno)->SetMarkerSize(1);
		  getH2(Form("noise_overflow_vs_channel_%s",FEBs[i_mmfe].c_str()),hno)->SetMarkerColor(kBlack);
		  getH2(Form("noise_overflow_vs_channel_%s",FEBs[i_mmfe].c_str()),hno)->GetYaxis()->SetTitle("ENC");
		  getH2(Form("noise_overflow_vs_channel_%s",FEBs[i_mmfe].c_str()),hno)->GetXaxis()->SetTitle("Channel");	
		//----------------------- baseline samples -------------------------------------------------------------------------
		 	getH2F(Form("samples_vs_channel_%s",FEBs[i_mmfe].c_str()),bl)->SetStats(0);
		  getH2F(Form("samples_vs_channel_%s",FEBs[i_mmfe].c_str()),bl)->SetTitle(Form("%s Baseline",FEBs[i_mmfe].c_str()));
		  getH2F(Form("samples_vs_channel_%s",FEBs[i_mmfe].c_str()),bl)->GetYaxis()->SetTitle("mV");
		  getH2F(Form("samples_vs_channel_%s",FEBs[i_mmfe].c_str()),bl)->GetXaxis()->SetTitle("Channel");
		}
	}else{continue;}	
}
//========================== fill histos =============================================================

int e_to_mV = 8000*9/6242;
std::map<std::string, int> nr_masked_chan;

if(thr_on){
	//-------- filling the threshold vs baseline plots -----------------
	for(int i_mmfe=0; i_mmfe<n_mmfe; i_mmfe++){
		if((this_str.length()>0) and (FEBs.at(i_mmfe).find(this_str)!=std::string::npos)){
		cout<<"Assembling plots for FEB - "<<FEBs[i_mmfe]<<endl;
		cout << Form("ch_baseline_med:channel_id+cvmm*64>>ch_baseline_med_vs_channel_%s",FEBs[i_mmfe].c_str()) <<Form("board==\"%s\"",FEBs[i_mmfe].c_str()) <<endl;
		cout << Form("ch_trimmed_thr*1000*1.5/4095.0:channel_id+cvmm*64>>ch_trimmed_thr_vs_channel_%s",FEBs[i_mmfe].c_str())<<Form("board==\"%s\"", FEBs[i_mmfe].c_str()) <<endl;
		cout << Form("channel_thr:channel_id+cvmm*64>>ch_threshold_ut_vs_channel_%s",FEBs[i_mmfe].c_str())<<Form("board==\"%s\"", FEBs[i_mmfe].c_str()) <<endl;
		cout << Form("ch_eff_thr_w_best_trim:channel_id+cvmm*64>>ch_eff_thr_w_best_trim_vs_channel_%s",FEBs[i_mmfe].c_str())<<Form("board==\"%s\"", FEBs[i_mmfe].c_str()) <<endl;
	
		std::cout<<"Drawing threshold histos for - "<<FEBs[i_mmfe]<<endl;
		std::cout <<par_tree->Draw(Form("ch_baseline_med:channel_id+cvmm*64>>ch_baseline_med_vs_channel_%s",FEBs[i_mmfe].c_str()),Form("board==\"%s\"",FEBs[i_mmfe].c_str()),"goff")<<std::endl;
		std::cout <<par_tree->Draw(Form("ch_trimmed_thr*1000*1.5/4095.0:channel_id+cvmm*64>>ch_trimmed_thr_vs_channel_%s",FEBs[i_mmfe].c_str()),Form("board==\"%s\"",FEBs[i_mmfe].c_str()),"goff")<<std::endl;
		std::cout <<par_tree->Draw(Form("ch_eff_thr_w_best_trim*1000*1.5/4095.0:channel_id+cvmm*64>>ch_eff_thr_w_best_trim_vs_channel_%s",FEBs[i_mmfe].c_str()),Form("board==\"%s\"",FEBs[i_mmfe].c_str()),"goff")<<std::endl;
		std::cout <<par_tree->Draw(Form("ch_mask:channel_id+cvmm*64>>mask_vs_channel_%s",FEBs[i_mmfe].c_str()),Form("board==\"%s\" && ch_mask!=0",FEBs[i_mmfe].c_str()),"goff")<<std::endl;
	  int mask = getH2(Form("mask_vs_channel_%s",FEBs[i_mmfe].c_str()),hmask)->GetEntries();
		cout<<mask<<endl;

		std::cout <<thr_tree->Draw(Form("channel_thr:channel_id+cvmm*64>>ch_threshold_ut_vs_channel_%s",FEBs[i_mmfe].c_str()),Form("board==\"%s\"",FEBs[i_mmfe].c_str()),"goff")<<std::endl;
	 
		nr_masked_chan[FEBs[i_mmfe]] = getH2(Form("mask_vs_channel_%s",FEBs[i_mmfe].c_str()),hmask)->GetEntries();

		}else{continue;}// if name input string does not match anything in the json file - continue
	}

	for(int i_mmfe=0; i_mmfe<n_mmfe; i_mmfe++)
	{
		if((this_str.length()>0) and (FEBs.at(i_mmfe).find(this_str)!=std::string::npos)){
		cout<<"Drawing noise histogramm for "<<FEBs[i_mmfe]<<endl;
		cout << Form("ch_baseline_rms/9*6242:channel_id+cvmm*64>>ch_baseline_rms_vs_channel_mmfe8-%04i",i_mmfe)<<Form("board==\"%s\"",FEBs[i_mmfe].c_str()) <<endl;
		cout << Form("vmm_median_rms/9*6242:channel_id+cvmm*64>>vmm_median_rms_vs_channel_mmfe8-%04i",i_mmfe)<<Form("board==\"%s\"",FEBs[i_mmfe].c_str()) <<endl;
		std::cout <<par_tree->Draw(Form("ch_baseline_rms/9*6242:channel_id+cvmm*64>>ch_baseline_rms_vs_channel_%s",FEBs[i_mmfe].c_str()),Form("board==\"%s\"",FEBs[i_mmfe].c_str()),"goff")<<std::endl;
		std::cout <<par_tree->Draw(Form("7900:channel_id+cvmm*64>>ch_baseline_rms_overflow_vs_channel_%s",FEBs[i_mmfe].c_str()),Form("board==\"%s\" && ch_baseline_rms>%i",FEBs[i_mmfe].c_str(), e_to_mV),"goff")<<std::endl;
		std::cout <<par_tree->Draw(Form("vmm_median_rms/9*6242:channel_id+cvmm*64>>vmm_median_rms_vs_channel_%s",FEBs[i_mmfe].c_str()),Form("board==\"%s\"",FEBs[i_mmfe].c_str()),"goff")<<std::endl;
		}else{continue;}
	}
}//thr_on bool if expresion ends
//-------- filling pure baseline plots ---------------------------------------------------------------------------------------------------

if(bl_on){

	for(int i_mmfe=0; i_mmfe<n_mmfe; i_mmfe++){
	if((this_str.length()>0)and (FEBs.at(i_mmfe).find(this_str)!=std::string::npos)){
	cout<<"Drawing baseline histogramm for "<<FEBs[i_mmfe]<<endl;
	cout << Form("sample:channel_id+cvmm*64>>samples_vs_channel_%s",FEBs[i_mmfe].c_str()) <<Form("board==\"%s\"",FEBs[i_mmfe].c_str(),"goff") <<endl;
	cout<<"Noise plot for"<<FEBs[i_mmfe]<<endl;
	cout << Form("rms/9*6242:channel_id+cvmm*64>>baseline_rms_vs_channel_%s",FEBs[i_mmfe].c_str()) <<Form("board==\"%s\"",FEBs[i_mmfe].c_str(),"goff") <<endl;
	std::cout <<bl_tree->Draw(Form("sample:channel_id+cvmm*64>>samples_vs_channel_%s",FEBs[i_mmfe].c_str()),Form("board==\"%s\"",FEBs[i_mmfe].c_str()),"goff")<<std::endl;
	std::cout <<bl_tree->Draw(Form("rms/9*6242:channel_id+cvmm*64>>baseline_rms_vs_channel_%s",FEBs[i_mmfe].c_str()),Form("board==\"%s\"",FEBs[i_mmfe].c_str()),"goff")<<std::endl;
	std::cout <<bl_tree->Draw(Form("7900:channel_id+cvmm*64>>noise_overflow_vs_channel_%s",FEBs[i_mmfe].c_str()),Form("board==\"%s\" && rms>%i",FEBs[i_mmfe].c_str(),e_to_mV),"goff")<<std::endl;

	}else{continue;}
	}
}
//
// later here a loop should be added to count overflow entries...
//
//Draw one canvas per MMMFE8
std::string ymd, hms;
TDatime *dt = new TDatime;
const char* datetime = dt->AsString();
//std::cout<<"date: "<<datetime<<" !"<<std::endl;
std::string datetime_str = datetime;
std::replace(datetime_str.begin(),datetime_str.end(),' ','_'); 
std::replace(datetime_str.begin(),datetime_str.end(),':','-'); 
//std::cout<<"formated date: "<<datetime_str<<" !"<<std::endl;

string archive = input_data.get<std::string>("archive"); 
std::string thr_folder_name="";
std::string bl_folder_name="";
if(thr_on==true){
	thr_folder_name = add_timestamp("threshold_calibration_plots_");
	std::string make_thr_folder = "mkdir -p "+archive+thr_folder_name; 
	system(make_thr_folder.c_str());
}
if(bl_on==true){
	bl_folder_name = add_timestamp("baseline_plots_");
	std::string make_bl_folder = "mkdir -p "+archive+bl_folder_name; 
	system(make_bl_folder.c_str());
}

//char new_dir[200];
//#################################################################
//std::string make_thr_folder = "mkdir -p "+archive+thr_folder_name; 
//std::string make_bl_folder = "mkdir -p "+archive+bl_folder_name; 
//system(make_thr_folder.c_str());
//system(make_bl_folder.c_str());
//##################################################################
if(thr_on){
	for(int i_mmfe=0; i_mmfe<n_mmfe; i_mmfe++){
	 if((this_str.length()>0) and (FEBs.at(i_mmfe).find(this_str)!=std::string::npos)){
   c[Form("c_%s",FEBs[i_mmfe].c_str())] = new TCanvas(Form("c_%s",FEBs[i_mmfe].c_str()),Form("c_%s",FEBs[i_mmfe].c_str()),700,500);
   c[Form("c_%s",FEBs[i_mmfe].c_str())]->cd();
   getH2(Form("ch_baseline_med_vs_channel_%s",FEBs[i_mmfe].c_str()),h1)->Draw();
	 getH2(Form("ch_trimmed_thr_vs_channel_%s",FEBs[i_mmfe].c_str()),h3)->Draw("SAME");
	 getH2(Form("ch_threshold_ut_vs_channel_%s",FEBs[i_mmfe].c_str()),h4)->Draw("SAME");
	 getH2(Form("ch_eff_thr_w_best_trim_vs_channel_%s",FEBs[i_mmfe].c_str()),h6)->Draw("SAME");
	
   TLegend *leg=new TLegend(0.1, 0.9, 0.35, 0.75);
   leg->AddEntry(getH2(Form("ch_baseline_med_vs_channel_%s",FEBs[i_mmfe].c_str()),h1),"Baseline","P");
   leg->AddEntry(getH2(Form("ch_trimmed_thr_vs_channel_%s",FEBs[i_mmfe].c_str()),h3),"Trimmed threshold","P");
   leg->AddEntry(getH2(Form("ch_threshold_ut_vs_channel_%s",FEBs[i_mmfe].c_str()),h4),"Untrimmed threshold","P");
   leg->AddEntry(getH2(Form("ch_eff_thr_w_best_trim_vs_channel_%s",FEBs[i_mmfe].c_str()),h6),"Effective threshold","P");
	 std::string mask_comment = "Tot CH masked: "+std::to_string(nr_masked_chan[FEBs[i_mmfe]]);
	 leg->AddEntry((TObject*)0,mask_comment.c_str(),"");
	
   leg->Draw();
//-----saving pic ---------------
	 c[Form("c_%s",FEBs[i_mmfe].c_str())]->SaveAs(Form("plotter_output/thresholds/%s_%s_%s.png",FEBs[i_mmfe].c_str(),pic_name.c_str(), datetime_str.c_str()));
   c[Form("c_%s",FEBs[i_mmfe].c_str())]->SaveAs(Form("%s%s/%s_%s_thr_%s.png",archive.c_str(), thr_folder_name.c_str(), FEBs[i_mmfe].c_str(),pic_name.c_str(), datetime_str.c_str())); 

   delete c[Form("c_%s",FEBs[i_mmfe].c_str())];
	 delete getH2(Form("ch_baseline_med_vs_channel_%s",FEBs[i_mmfe].c_str()),h1);	  
	 delete getH2(Form("ch_trimmed_thr_vs_channel_%s",FEBs[i_mmfe].c_str()),h3);
	 delete getH2(Form("ch_threshold_ut_vs_channel_%s",FEBs[i_mmfe].c_str()),h4);
	 delete getH2(Form("ch_eff_thr_w_best_trim_vs_channel_%s",FEBs[i_mmfe].c_str()),h6);
	 delete getH2(Form("mask_vs_channel_%s",FEBs[i_mmfe].c_str()),hmask);
	
	 }else{continue;}
	}
//############################### threshold noise plots##########################################
	for(int i=0; i<n_mmfe; i++)
	{
		if((this_str.length()>0)and(FEBs.at(i).find(this_str)!=std::string::npos)){
		n[Form("n_%s",FEBs[i].c_str())] = new TCanvas(Form("n_%s",FEBs[i].c_str()),Form("n_%s",FEBs[i].c_str()),700,500);
	  n[Form("n_%s",FEBs[i].c_str())]->cd();
	
	  getH2(Form("ch_baseline_rms_vs_channel_%s",FEBs[i].c_str()),h5)->Draw();
	  getH2(Form("ch_baseline_rms_overflow_vs_channel_%s",FEBs[i].c_str()),htno)->Draw("SAME");
	  getH2(Form("vmm_median_rms_vs_channel_%s",FEBs[i].c_str()),h7)->Draw("SAME");

	  TLegend *leg=new TLegend(0.1, 0.9, 0.3, 0.8);
	  leg->AddEntry(getH2(Form("ch_baseline_rms_vs_channel_%s",FEBs[i].c_str()),h5),"Channel noise RMS","P");
	  leg->AddEntry(getH2(Form("vmm_median_rms_vs_channel_%s",FEBs[i].c_str()),h7),"VMM meadian noise RMS","P");
	  leg->Draw();
//-------- saving --------------
		n[Form("n_%s",FEBs[i].c_str())]->SaveAs(Form("plotter_output/noise/%s_%s_%s.png",FEBs[i].c_str(),pic_name.c_str(), datetime_str.c_str()));
	  n[Form("n_%s",FEBs[i].c_str())]->SaveAs(Form("%s%s/%s_%s_noise_%s.png",archive.c_str(), thr_folder_name.c_str(), FEBs[i].c_str(),pic_name.c_str(), datetime_str.c_str()));

		delete n[Form("n_%s",FEBs[i].c_str())];
	 
		delete getH2(Form("ch_baseline_rms_vs_channel_%s",FEBs[i].c_str()),h5);
	  delete getH2(Form("ch_baseline_rms_overflow_vs_channel_%s",FEBs[i].c_str()),htno);
	  delete getH2(Form("vmm_median_rms_vs_channel_%s",FEBs[i].c_str()),h7);

		}else{continue;}
	}
//	sleep(1);
}
//#################################### baseline plots + noise ######################################################
if(bl_on){
	for(int i_mmfe=0; i_mmfe<n_mmfe; i_mmfe++){
		if((this_str.length()>0)and (FEBs.at(i_mmfe).find(this_str)!=std::string::npos)){

////////////////// plotting baselines ////////////////////////
	  c_bl[Form("b_%s",FEBs[i_mmfe].c_str())] = new TCanvas(Form("b_%s",FEBs[i_mmfe].c_str()),Form("c_%s",FEBs[i_mmfe].c_str()),700,500);
 		c_bl[Form("b_%s",FEBs[i_mmfe].c_str())]->cd();
  	getH2F(Form("samples_vs_channel_%s",FEBs[i_mmfe].c_str()),bl)->Draw("COLZ");
		c_bl[Form("b_%s",FEBs[i_mmfe].c_str())]->SaveAs(Form("plotter_output/baselines/%s_baseline_%s_%s.svg",FEBs[i_mmfe].c_str(),pic_name.c_str(),datetime_str.c_str()));
//		c_blo[Form("bo_%s",FEBs[i_mmfe].c_str())]->SaveAs(Form("plotter_output/baselines/%s_baseline_overflow_%s_%s.svg",FEBs[i_mmfe].c_str(),pic_name.c_str(),datetime_str.c_str()));
	  c_bl[Form("b_%s",FEBs[i_mmfe].c_str())]->SaveAs(Form("%s%s/%s_%s_%s.svg",archive.c_str(), bl_folder_name.c_str(), FEBs[i_mmfe].c_str(),pic_name.c_str(), datetime_str.c_str()));

/////////////// plotting noise //////////////////////////////
	  c_no[Form("no_%s",FEBs[i_mmfe].c_str())] = new TCanvas(Form("no_%s",FEBs[i_mmfe].c_str()),Form("no_%s",FEBs[i_mmfe].c_str()),700,500);	
	  c_no[Form("no_%s",FEBs[i_mmfe].c_str())] ->cd();
  	getH2(Form("baseline_rms_vs_channel_%s",FEBs[i_mmfe].c_str()),hn)->Draw();
  	getH2(Form("noise_overflow_vs_channel_%s",FEBs[i_mmfe].c_str()),hno)->Draw("SAME");
		c_no[Form("no_%s",FEBs[i_mmfe].c_str())]->SaveAs(Form("plotter_output/noise/%s_noise_%s_%s.png",FEBs[i_mmfe].c_str(),pic_name.c_str(),datetime_str.c_str()));
	  c_no[Form("no_%s",FEBs[i_mmfe].c_str())]->SaveAs(Form("%s%s/%s_noise_%s_%s.png",archive.c_str(), bl_folder_name.c_str(), FEBs[i_mmfe].c_str(),pic_name.c_str(), datetime_str.c_str()));
		
    delete c_no[Form("no_%s",FEBs[i_mmfe].c_str())];
  	delete getH2(Form("baseline_rms_vs_channel_%s",FEBs[i_mmfe].c_str()),hn);
  	delete getH2(Form("noise_overflow_vs_channel_%s",FEBs[i_mmfe].c_str()),hno);

		}else{continue;}
	 }
  }

}//end of other_plot function

//==================================================================================================================
//#############################	M A I N ############################################################################
//==================================================================================================================

#ifndef __CINT__
int main(int ac, const char* av[]){

	auto start = std::chrono::high_resolution_clock::now();
//-------- terminal value input comparison ------------------------------

	bool scale = false;
	bool vs = false;
	bool bb5 = false;
	bool baseline = false;
	bool threshold = false;	
	bool upd_tree = false;
	bool pulser = false;
	bool sca = false;
	bool l1 = false;
	bool pdo = false;
	bool tdo = false;

	std::string name;
	std::string this_str;
	std::string input_file;
	std::string root_file;

	printf("WELCOME COMRADE, LETS PLOT THINGS:\n");
	for(int i_arg=1;i_arg<ac;i_arg++){
				if(strcmp(av[i_arg], "-n")==0){name = std::string(av[i_arg+1]);i_arg++;printf("Plot names will have suffix- [%s]\n",name.c_str());}
        if(strcmp(av[i_arg], "-f")==0){this_str = std::string(av[i_arg+1]);i_arg++;printf("Boards with following character in names will be described -> - [%s]\n",this_str.c_str());}
        if(strcmp(av[i_arg], "-j")==0){input_file = std::string(av[i_arg+1]);i_arg++;printf("Selected input .json file -> - [%s]\n",input_file.c_str());}
        if(strcmp(av[i_arg], "-rf")==0){root_file = std::string(av[i_arg+1]);i_arg++;printf("Selected data root file -> - [%s]\n",root_file.c_str());}
//---------------------------------- pdo plotting falg ------------------------------------------------
        if(strcmp(av[i_arg], "--l1")==0){l1 = true;printf("Drawing SCA pathc calibration results -> [DA]\n");}
        if(strcmp(av[i_arg], "--pdo")==0){pdo = true;printf("Calibrating PDO -> [DA]\n");}
        if(strcmp(av[i_arg], "--tdo")==0){tdo = true;printf("Calibrating TDO -> [DA]\n");}
        if(strcmp(av[i_arg], "--sca")==0){sca = true;printf("Drawing L1 calibration results -> [DA]\n");}
//--------------------------------------------------------------------------------------------------
//        if(strcmp(av[i_arg], "--pdo")==0){pdo = true;printf("Drawing PDO hits -> [DA]\n");}
//-----------------------------------------------------------------------------------------------------
        if(strcmp(av[i_arg], "--bl")==0){baseline = true;printf("Drawing baselines -> [DA]\n");}
        if(strcmp(av[i_arg], "--thr")==0){threshold = true;printf("Drawing thresholds? -> [DA]\n");}
	      if(strcmp(av[i_arg], "--pulser")==0){pulser = true;printf("Drawing Pulser DAC -> [DA]\n");}
 //       if(strcmp(av[i_arg], "--vs")==0){vs = true;printf("Using VS data -> [DA]\n");}
        if(strcmp(av[i_arg], "--scale")==0){scale = true;printf("Drawing on the full mV scale -> [DA]\n");}
      //  if(strcmp(av[i_arg], "-f")==0){this_str = std::string(av[i_arg+1]);i_arg++;printf("Particular boards to consider? -> - (%s)\n",this_str.c_str());}
        if(strcmp(av[i_arg], "--upd_trees")==0){upd_tree = true;printf("Root trees will be updated -> [DA]\n");}
        }
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------
	std::string io_config_path;
	io_config_path = input_file;
//	if(bb5){io_config_path = "/afs/cern.ch/user/v/vplesano/public/NSWConfig_build/work/NSWConfiguration/bb5_input_data.json";} // json for freiburg setup data !!!!!
//	else if(vs){io_config_path = "/afs/cern.ch/user/v/vplesano/public/NSWConfig_build/work/NSWConfiguration/vs_input_data.json";} // json for the VS setup data !!!!!
//	else{io_config_path = "/afs/cern.ch/user/v/vplesano/public/NSWConfig_build/work/NSWConfiguration/lxplus_input_data.json";} // json for BB5 DW data !!!!!
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------
if(sca==true){
	if(upd_tree == true)
	{
		if(baseline == true){fill_bl(io_config_path);}		
		//if(pulser == true){fill_pulser_dac(io_config_path);}		
		//fill_bl(io_config_path);		
		fill_thre(io_config_path);
		fill_ntuple(io_config_path);
	}	
//	other_plot(name, this_str, n_mmfe, baseline, threshold, bb5, vs, scale);
	//other_plot(io_config_path, name, this_str, baseline, threshold, bb5, vs, scale);
	if(baseline == false and threshold == false){exit(0);}
//------------ drawing pdo -------------------------
	//else if(pdo){plot_pdo(scale, name);}

//--------------------------------------------------
	//else{other_plot(io_config_path, name, this_str, baseline, threshold, scale);}
	other_plot(io_config_path, name, this_str, baseline, threshold, scale);
}
else if(l1==true){
	if(pulser==true){fill_pulser_dac(io_config_path);}		
//	fill_pulser_dac(io_config_path);		
	std::cout<<"User needs PDO/TDO plots, move Roach!"<<std::endl;
	plot_pdo(pdo, tdo, scale, name, root_file);
}
else{std::cout<<"nothing to do"<<std::endl;}
	return 0;
/////////////// clock stops now////////////////////////////////
 auto finish = std::chrono::high_resolution_clock::now();
 std::chrono::duration<double> elapsed = finish - start;
 std::cout << "\n Elapsed time: " << elapsed.count() << " s\n";

}
#endif
