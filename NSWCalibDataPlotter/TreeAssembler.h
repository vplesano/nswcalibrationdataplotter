#ifndef TREEASSEMBLER_H_
#define TREEASSEMBLER_H_

#include <fstream>
#include "Riostream.h"
#include <sstream>
#include <iostream>
#include "TROOT.h"
#include "TRint.h"
#include <TString.h>
#include <cmath>
#include <TFile.h>
#include <TMultiGraph.h>
#include <string>
#include <vector>
#include <sys/types.h>
#include <algorithm>
#include <chrono>
#include <stdio.h>
#include <stdlib.h>
#include <TTree.h>
#include <map>
#include <TStyle.h>
#include <ctime>
#include <iomanip>
#include <TSystem.h>
#include <TTreeReader.h>
#include <TTreeReaderArray.h>
#include <TTreeReaderValue.h>
#include <math.h>
#include "boost/property_tree/json_parser.hpp"
#include "boost/foreach.hpp"


namespace nsw {
  
  class TreeAssembler {

    public:  
    TreeAssembler();    
    ~TreeAssembler() {};

    void fill_pulser_dac(std::string data_path, std::string archive_path);

    void fill_bl(std::string data_path, std::string archive_path);
        
    void fill_thre(std::string data_path, std::string archive_path);

    void fill_ntuple(std::string data_path, std::string archive_path);
  };

}

#endif
