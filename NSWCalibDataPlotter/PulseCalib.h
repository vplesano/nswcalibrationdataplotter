#ifndef PULSECALIB_H_
#define PULSECALIB_H_

#include <fstream>
#include "Riostream.h"
#include <TF1.h>
#include <TH2D.h>
#include <TH1F.h>
#include <sstream>
#include <iostream>
#include "TROOT.h"
#include "TRint.h"
#include <TString.h>
#include <TCanvas.h>
#include <cmath>
#include <TMath.h>
#include <TFile.h>
#include <TMultiGraph.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TAxis.h>
#include <TGaxis.h>
#include <TText.h>
#include <string>
#include <vector>
#include <sys/types.h>
#include <algorithm>
#include <TLegend.h>
#include <stdio.h>
#include <stdlib.h>
#include <TTree.h>
#include <map>
#include <TStyle.h>
#include <ctime>
#include <iomanip>
#include <TSystem.h>
#include <tuple>
#include <TTreeReader.h>
#include <TTreeReaderArray.h>
#include <TTreeReaderValue.h>
#include <math.h>
#include <TLatex.h>

namespace nsw{
  
  class PulseCalib{
    public:  
    PulseCalib();    
    ~PulseCalib() {};

    void calibrate_pulser_DAC(
         std::map<std::string,std::tuple<float,float,float,float>> &vmm_constants,
         std::string pic_name, 
         std::string feb_to_check);    

    std::map<std::string,TH1F*> read_root_file(std::string this_root_file,
                                                std::string pic_name,
                                                std::map<std::string,TH1F*> &t_hpdo,
                                                std::vector<int> &dac_del,
                                                std::string feb_to_check,
                                                int &totent,
                                                bool pdo,
                                                bool tdo);
    
    void find_peaks(std::map<std::string,TH1F*> &hist,
                    std::map<std::string,TGraphErrors*> &graph,
                    std::string feb_to_check,
                    std::map<std::string, std::tuple<float,float,float,float>> &vmm_constants,
                    std::string pic_name,
                    float &mean_hits_per_key,
                    bool tdo,
                    bool pdo);

    void fit_calibKey_graphs(std::map<std::string,TGraphErrors*> &t_gr,
                              std::string feb_to_check,
                              std::string rootoutfilename,
                              std::string pic_name,
                              bool pdo,
                              bool tdo);

    void PulseCalib_plot(bool pdo,
                 bool tdo,
                 std::string feb_to_check,
                 std::string pic_name,
                 std::string this_root_file,
                 std::string archive_path);

  };

}

#endif
