#ifndef BIGTHRESHOLDS_H_
#define BIGTHRESHOLDS_H_

#include <fstream>
#include "Riostream.h"
#include <TF1.h>
#include <TH3D.h>
#include <TH2D.h>
#include <TH1F.h>
#include <TGraph2D.h>
#include <TStyle.h>
#include <sstream>
#include <iostream>
#include "TROOT.h"
#include "TRint.h"
#include <TString.h>
#include <TCanvas.h>
#include <cmath>
#include <TMath.h>
#include <TLine.h>
#include <TFile.h>
#include <TMultiGraph.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TAxis.h>
#include <TGaxis.h>
#include <TText.h>
#include <TMarker.h>
#include <string>
#include <vector>
#include <sys/types.h>
#include <algorithm>
#include <TLegend.h>
#include <stdio.h>
#include <stdlib.h>
#include <TTree.h>
#include <map>
#include <TStyle.h>
#include <ctime>
#include <iomanip>
#include <TSystem.h>
#include <tuple>
#include <TTreeReader.h>
#include <TTreeReaderArray.h>
#include <TTreeReaderValue.h>
#include <TMultiGraph.h>
#include <math.h>
#include <TLatex.h>
#include <TGaxis.h>

namespace nsw{
 
  class BigThresholds{
      public:
      BigThresholds();    
      ~BigThresholds() {};
 
      void runFullSector(std::string rootfile, std::string picname, std::string FEB, bool enlarge);
      void baselineFullSector(std::string rootfile, std::string picname);

      int getRadius(std::string febname, int &layer);
      int getSectorChan(std::string fename, int chan, int vmm);

      float getMean(std::vector<float> &vals);
      float getRMS(std::vector<float> &vals,float &mean);

  };

}

#endif



