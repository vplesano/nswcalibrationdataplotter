#ifndef THRESHOLDPLOTTER_H_
#define THRESHOLDPLOTTER_H_

#include <fstream>
#include "Riostream.h"
#include <TF1.h>
#include <TH2D.h>
#include <TH1F.h>
#include <sstream>
#include <iostream>

#include "TROOT.h"
#include "TRint.h"
#include <TString.h>
#include <TCanvas.h>
#include <cmath>
#include <TMath.h>
#include <TFile.h>
#include <TMultiGraph.h>
#include <TGraph.h>
#include <TAxis.h>
#include <TGaxis.h>
#include <TText.h>
#include <string>
#include <vector>

#include <sys/types.h>
#include <algorithm>
#include <chrono>
#include <TLegend.h>
#include <stdio.h>
#include <stdlib.h>
#include <TTree.h>
#include <map>
#include <TStyle.h>
#include <ctime>
#include <iomanip>
#include <TSystem.h>

#include <TTreeReader.h>
#include <TTreeReaderArray.h>
#include <TTreeReaderValue.h>
#include <math.h>
#include <TLatex.h>

#include "TSpectrum.h"
#include "TVirtualFitter.h"
#include "TPolyMarker.h"
#include "TProfile.h"

#include "boost/property_tree/json_parser.hpp"
#include "boost/foreach.hpp"


namespace nsw {
  
  class ThresholdPlotter {

    public:
    ThresholdPlotter();
    ~ThresholdPlotter() {};

    //// plot inits ////////////////// 
    std::string add_timestamp(std::string in_string);

    void initH1_pdo(std::map<std::string,TH1D*> &h1, char* name, bool scale, std::string feb);

    void initH1_tdo(std::map<std::string,TH1D*> &h1, char* name, bool scale, std::string feb);

    void initH2_DetOut_trgCalibKey(std::map<std::string,TH2D*> &h2, 
                                   char* name, 
                                   std::string feb, 
                                   double n_events);

    TH1D* getH1_pdo(char* name, std::map<std::string,TH1D*> &h1 );

    TH1D* getH1_tdo(char* name, std::map<std::string,TH1D*> &h1 );

    TH2D* getH2_DetOut_trgCalibKey(char* name, std::map<std::string,TH2D*> &h2 );

    void initH2(std::map<std::string,TH2D*> &h2, char* name, bool scale, std::string feb);

    TH2D* getH2(char* name, std::map<std::string,TH2D*> &h2 );

    void initH2_noise(std::map<std::string,TH2D*> &h2, char* name, bool scale,std::string feb);
    
    void initH2_noise_over(std::map<std::string,TH2D*> &h2, char* name, bool scale, std::string feb);

    void initH2_layer(std::map<std::string,TH2D*> &h2, char* name, bool scale);
    
    void initH1F_dac(std::map<std::string,TH1F*> &h1f, char* name);

    void initH2F(std::map<std::string,TH2F*> &h2f, char* name, bool scale, std::string feb);

    TH1F* getH1F(char* name, std::map<std::string,TH1F*> &h1f );

    TH2F* getH2F(char* name, std::map<std::string,TH2F*> &h2f );

                ///// main threshold plotting func ////////  
    void ThrCalib_plot(std::string data_path,
                        std::string archive_path,
                        std::string json_config,
                        std::string pic_name,
                        std::string this_str,
                        bool bl_on,
                        bool thr_on,
                        bool scale,
                        bool notitle);

    std::vector<std::string> getAvailableFebs(std::string rootfile, bool baseline);

    int getRadius(std::string febname);

    void PlotOldBaseline(std::string file,std::string pname,int exp_samples);
    
    std::string changeFeName(std::string inputstr);

  };//class brack

}//namespace brack

#endif
